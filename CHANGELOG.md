# CHANGELOG

## [Unreleased]
### Added

- เพิ่ม API ให้ 3rd-party สามารถทำการส่งเยี่ยมบ้านคนไข้ได้
- 


## [2.1.0] - 2019-10-01
### Changed

- Redesign application to be hosted on cloud
- Remove EHHC Gateway
- Call NHSO Service for user authentication
- Encrypt sensitive data

## [2.0.2] - 2019-03-14
### Changed

- Fixes some bugs

## [2.0.1] - 2019-03-12
### Changed

- Create dummy visit if it's not available
- Change wording in init db

## [2.0.0] - 2019-03-05

- Refactoring code & improve performance

## [1.0.9] - 2019-01-10
### Added

- Handle redis connection timeout

### Changed

- Revert referrals status if cannot send to MQ

## [1.0.8] - 2019-01-07
### Added

- Install wkhtmltopdf on docker

## [1.0.7] - 2018-12-26
### Fixed

- Fix missing error code

## [1.0.6] - 2018-12-11
### Added

- Load configuration from dotenv

### Changed

- Update UI container

## [1.0.5] - 2018-11-28
### Changed

- Allow user to have multiple roles

## [1.0.4] - 2018-11-07
### Fixed

- Search service by id instead of pick the first one
