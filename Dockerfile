FROM ruby:2.5
RUN apt-get update -q && apt-get install -y libpq-dev nodejs
RUN curl -L -o wkhtmltopdf.tar.xz https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
RUN tar -xf wkhtmltopdf.tar.xz
RUN mv wkhtmltox/bin/wkhtmltopdf /usr/bin/wkhtmltopdf
RUN chmod +x /usr/bin/wkhtmltopdf
RUN mkdir /app
WORKDIR /app
COPY . /app
RUN bundle install
EXPOSE 5000
