# EHHC-Backend
Server สำหรับจัดการข้อมูลเยี่ยมบ้าน และแสดงรายงาน

## การประมวลผลข้อมูล
สถานะของการส่งไปประมวลผลมีอยู่ด้วยกัน 4 สถานะ
- 1: ข้อมูลใหม่ยังไม่ประมวลผล
- 2: ประมวลผลผ่าน
- 3: ประมวลผลไม่ผ่าน
- 4: ผ่านแต่ไม่ตรงปีงบประมาณปัจจุบัน

## User Roles
- EHHC_VSTAFFTYPE_ONSITE
- EHHC_VSTAFFTYPE_NURSE
- EHHC_VSTAFFTYPE_CENTER
- EHHC_VSTAFFTYPE_ADMIN
- EHHC_VSTAFFTYPE_BMA

## ประเภทของสถานพยาบาล
- 1: โรงพยาบาลของรัฐ
- 2: โรงพยาบาลเอกชน
- 3: ศูนย์บริการสาธารณสุข
- 4: คลินิกชุมชนอบอุ่น
- 5: สถานพยาบาล
- 6: อื่นๆ
- 7: หน่วยเฉพาะทาง(รัฐ)
- 8: หน่วยเฉพาะทาง(เอกชน)
- 9: ร้านยาคุณภาพ
- 10: คลินิกร่วมทันตกรรม

## รายงาน
- ผู้ใช้งานสามารถกรองข้อมูลตาม htype ของสถานพยาบาล
  - role: admin และ bma จะเห็นเฉพาะ htype = 1, 3 และ 4
  - role: bma จะเห็นเฉพาะ htype = 3


## Authentication

POST https://bkkapp.nhso.go.th/bkkapp/api/auth/login
Body:
username: string
password: string
appid: number

Example:


## สร้าง Referral ใหม่
POST /api/referrals
Body:

## New APIs
### Paralleled Referral

POST /api/referrals/:id/paralleled
body:


POST /api/referrals/:id/continuous
body: