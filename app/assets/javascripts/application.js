// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require turbolinks
//= require semantic-ui
//= require_tree .





function dateFormatter (date, settings) {
  if (!date) return '';
  const day = ("0" + date.getDate()).slice(-2);
  const month = ("0" + (date.getMonth() + 1)).slice(-2);
  const year = date.getFullYear() + 543;

  let represent;
  if (settings.type == 'date') {
    represent = day + '/' + month + '/' + year;
  } else if (settings.type == 'month') {
    represent = month + '/' + year;
  }

  return represent;
}

function thaiText () {
  return {
    days: ['อา.', 'จ.', 'อ.', 'พ', 'พฤ.', 'ศ.', 'ส.'],
    months: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
    monthsShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
    today: 'วันนี้',
    now: 'ขณะนี้',
    am: 'AM',
    pm: 'PM'
  };
}

function enableCalendar(component, options) {
  // console.log('enable calendar');
  const defaultOptions = {
    type: 'date',
    maxDate: new Date(),
    text: thaiText(),
    formatter: {
      date: dateFormatter,
      dayHeader: function (date, settings) {
        const month = date.getMonth();
        const year = date.getFullYear() + 543;
        return thaiText().monthsShort[month] + ' ' + year;
      },
      monthHeader: function (date, settings) {
        const year = date.getFullYear() + 543;
        return year;
      },
      yearHeader: function (date, settings) {
        const year = date.getFullYear() + 543;
        return year;
      }
    }
  }

  $(component).calendar(Object.assign({}, defaultOptions, options));
}

function demoFunc() {
  console.log('calling demo function');
}
