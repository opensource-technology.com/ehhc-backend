
const setDataCaseManager = (caseManagers) => {
  $('#case_manager_dropdown option').remove();
  $('#case_manager_dropdown').append(new Option('ทั้งหมด', 'all'))
  caseManagers.forEach(isCaseManager => {
    const text = `${isCaseManager.first_name} ${isCaseManager.last_name}`
    $('#case_manager_dropdown').append(new Option(text, isCaseManager.set_id || null))
  });
  
  $('#case_manager_dropdown').dropdown('set selected', 'all');
}
const setOption = (dataArray, componentName) => {
  $(`${componentName} option`).remove();
  $(componentName).append(new Option('ทั้งหมด', 'all'))
  dataArray.forEach(data => {
    $(componentName).append(new Option(data[0], data[1] || null))
  });
  
  $(componentName).dropdown('set selected', 'all');
}

const onChangeCaseManager = (sites, token, type, reportType) => {
  const destination_checked = $('[name="destination"]:checked').val();
  if(reportType === 'assessment'){ 
    // ((destination_checked === 'site' && type === "hospital_destination") ||
    // (destination_checked === 'accept' && type === "hospital")) &&
    getCaseManager($('#hospital').val(), $('#hospital_destination').val(), token); 
  }else if(reportType === 'refer'){
    type === "hospital" && getCaseManager(sites, null, token);
  }
}
const getCaseManager = (site1, site2, token) => {
  $('#case_manager_dropdown_field').addClass("disabled")
  $.ajax({
    url: `/providers/filter?site1=${site1}&site2=${site2}&token=${token}` 
  }).done(function(data) { 
      setDataCaseManager(data)
    $('#case_manager_dropdown_field').removeClass("disabled");
  });
}

const setDate = (fromDate, toDate) => {
  $('#from_date_component').calendar('set date', fromDate);
  $('#to_date_component').calendar('set date', toDate);
}

$(document).on('turbolinks:load', function () {
  $("#toggle_navbar").click(function () {
    $(".headVisit-contentLeft").toggleClass("--active");
  });
});
