class AddressesController < ApplicationController
  include Api

  before_action :set_patient, except: %i[changwats amphurs tumbons]
  before_action :set_address, only: %i[show edit update destroy], except: %i[changwats amphurs tumbons]

  # GET /addresses
  # GET /addresses.json
  def index
    render json: @patient.addresses.as_json(ATTRS), status: :ok
  end

  # GET /addresses/1
  # GET /addresses/1.json
  def show
    render json: @address.as_json(ATTRS), status: :ok
  end

  # GET /addresses/new
  def new
    @address = Address.new
  end

  # GET /addresses/1/edit
  def edit; end

  # POST /addresses
  # POST /addresses.json
  def create
    # puts(address_params[:address_type])
    @address = Address.where patient_id: @patient.id, address_type: address_params[:address_type]

    if @address.exists?
      if @address.update(address_params)
        render json: @address, status: :ok
      else
        render json: @address.errors, status: :bad_request
      end
    else
      @address = Address.new(address_params)
      if @address.save
        render json: @address, status: :ok
      else
        render json: @address.errors, status: :bad_request
      end
    end
  end

  # PATCH/PUT /addresses/1
  # PATCH/PUT /addresses/1.json
  def update
    if @address.update(address_params)
      render json: @address.as_json(), status: :ok
    else
      render json: @address.errors, status: :unprocessable_entity
    end
  end

  # DELETE /addresses/1
  # DELETE /addresses/1.json
  def destroy
    @address.destroy
    respond_to do |format|
      format.html { redirect_to addresses_url, notice: 'Address was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def changwats
    @changwats = MAddressChangwat.all.order(:label)
    render json: @changwats.as_json, status: :ok
  end

  def amphurs
    @amphurs = if params.key?(:query)
                 MAddressAmphur.where("code like ? and label not like '%*%'", "#{params[:query]}%").order(:label)
               else
                 MAddressAmphur.all.order(:label)
               end
    render json: @amphurs.as_json, status: :ok
  end

  def tambons
    @tambons = if params.key?(:query)
                 MAddressTambon.where("code like ? and label not like '%*%'", "#{params[:query]}%").order('label ASC')
               else
                 MAddressTambon.all.order(:label)
               end
    render json: @tambons.as_json, status: :ok
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_address
    @address = Address.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def address_params
    params.fetch(:address, {}).merge(patient_id: @patient.id).permit!
  end

  def set_patient
    @patient = Patient.find(params[:patient_id]) if params.key?(:patient_id)
  end
end
