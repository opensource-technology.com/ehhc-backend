class ApiController < ApplicationController
  before_action :require_login, except: %w[test index_next]
end
