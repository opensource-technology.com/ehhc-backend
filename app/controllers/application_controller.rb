class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  # skip_before_action :verify_authenticity_token
  include ApplicationHelper
  include SessionsHelper
  include AuthenticationHelper
  include ReportsHelper
  include ParameterReportsHelper
  include PdfReportsHelper
  include AssessmentsHelper
  def health; end

  def about; end
end
