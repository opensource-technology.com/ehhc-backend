class AssessmentsController < ApplicationController
  include Api

  before_action :require_admin_nhso, only: [:by_id, :list]
  before_action :set_referral, except: [:list]
  before_action :set_assessment, only: [:show, :edit, :update, :destroy, :by_id]

  # GET /assessments
  # GET /assessments.json
  def index
    if @referral.present?
      @assessments = @referral.assessments.order(assessment_at: :desc)
      render json: @assessments.as_json(list: true), status: :ok
    else
      render json: { code: '31', message: 'Referral ID is missing.' }, status: :bad_request
    end
  end

  def list
    page_param = params.fetch(:page, 1)
    start_date_param = params.fetch(:start_date, nil)
    end_date_param = params.fetch(:end_date, nil)

    query = ''
    query_params = {}

    if !start_date_param.nil? && !end_date_param.nil?
      query = "updated_at at time zone 'utc' at time zone 'ict' between :start_date AND :end_date"
      query_params = { start_date: start_date_param, end_date: end_date_param }
    elsif !start_date_param.nil?
      query = "updated_at at time zone 'utc' at time zone 'ict' >= :start_date"
      query_params = { start_date: start_date_param }
    elsif !end_date_param.nil?
      query = "udpated_at at time zone 'utc' at time zone 'ict' <= :end_date"
      query_params = { end_date: end_date_param }
    end

    @assessments = if query.present?
                     Assessment.where(query, query_params).order(updated_at: :asc, assessment_at: :asc).page(page_param)
                   else
                     Assessment.order(updated_at: :asc, assessment_at: :asc).page(page_param)
                   end
     
    render json: @assessments.as_json(list: true), status: :ok
  end

  def by_id
    render json: @assessment.as_json, status: :ok
  end

  # GET /assessments/1
  # GET /assessments/1.json
  def show
    render json: @assessment.as_json, status: :ok
  end

  # GET /assessments/1/edit
  def edit; end

  # POST /assessments
  # POST /assessments.json
  def create
    # p params
    # Extract relevent data from assessments
    # 1. Health condition
    # 2. Medication
    # 3. Mood and behavior pattern
    # 4. Physical functioning
    # 5. Preventive health measure
    # 6. Skin condition
    # 7. Pharmacies
    # 8. Care activities
    # 9. Multidisciplinary teams
    # 10. Patient assessment equipments
    assessment_params = params.fetch(:assessment, {})
    assessment_params = assessment_params.merge(creator: current_authenticated_user)

    # if appointment == 2 --> terminated
    ActiveRecord::Base.transaction do
      update_referral(assessment_params)
      check_continuous_referral

      %w[health_condition medication mood_and_behavior_pattern physical_functioning preventive_health_measure skin_condition].each do |attr|
        assessment_params = create_or_update_relative(attr, assessment_params)
      end

      # begin
      if assessment_params.key?(:set_id) && assessment_params[:set_id].present?
        @assessment = Assessment.find_by(set_id: assessment_params[:set_id])
        @assessment.update(assessment_params.except(id: assessment_params[:id]).merge(referral_id: @referral.id).permit!)
      else
        # create new Assessment
        @assessment = Assessment.create assessment_params.merge(referral_id: @referral.id, hospital: @hospital).permit!
      end

      return render json: @assessment.errors, status: :unprocessable_entity if @assessment.errors.present?

      pharmacies_params = params.fetch(:pharmacies, [])
      pharmacies_params.each do |pharmacy_param|
        if pharmacy_param.key?(:set_id) & !pharmacy_param[:set_id].nil?
          pharmacy = Pharmacy.find_by(set_id: pharmacy_param[:set_id])
          pharmacy.update(pharmacy_param.merge(assessment_id: @assessment.id).permit!)
        else
          Pharmacy.create pharmacy_param.merge(assessment_id: @assessment.id, hospital: @hospital).permit!
        end
      end

      care_activities_params = params.fetch(:care_activities, [])
      care_activities_params.each do |care_activity_param|
        # p care_activity_param
        care_activity = CareActivity.find_by(assessment_id: @assessment.id, activity: care_activity_param[:activity])
        if care_activity.present?
          care_activity.update(care_activity_param.merge(assessment_id: @assessment.id).permit!)
        else
          CareActivity.create care_activity_param.merge(assessment_id: @assessment.id, hospital: @hospital).permit!
        end
      end

      multidisciplinary_teams_params = params.fetch(:multidisciplinary_teams, [])
      multidisciplinary_teams_params.each do |multidisciplinary_team_param|
        if multidisciplinary_team_param.key?(:set_id) & !multidisciplinary_team_param[:set_id].nil?
          multidisciplinary_team = MultidisciplinaryTeam.find_by(set_id: multidisciplinary_team_param[:set_id])
          multidisciplinary_team.update(multidisciplinary_team_param.merge(assessment_id: @assessment.id).permit!)
        else
          MultidisciplinaryTeam.create multidisciplinary_team_param.merge(assessment_id: @assessment.id, hospital: @hospital).permit!
        end
      end

      patient_assessment_equipments_params = params.fetch(:patient_assessment_equipments, [])
      patient_assessment_equipments_params.each do |patient_assessment_equipment_param|
        # p patient_assessment_equipment_param
        if patient_assessment_equipment_param.key?(:set_id) & !patient_assessment_equipment_param[:set_id].nil?
          patient_assessment_equipment = PatientAssessmentEquipment.find_by(set_id: patient_assessment_equipment_param[:set_id])
          patient_assessment_equipment.update(patient_assessment_equipment_param.merge(assessment_id: @assessment.id).permit!)
        else
          PatientAssessmentEquipment.create patient_assessment_equipment_param.merge(assessment_id: @assessment.id, hospital: @hospital).permit!
        end
      end

      render json: @assessment.as_json, status: :ok
    end
  end

  def create_or_update_relative(type, assessment_params)
    clzz_params = params.fetch(type.to_sym, {})
    return assessment_params if !clzz_params.present?

    clazz = type.classify.constantize
    if clzz_params.key?(:set_id)
      instance = clazz.find_by(set_id: clzz_params[:set_id])
      instance.update(clzz_params.permit!)
    else
      instance = clazz.create(clzz_params.merge(hospital: @hospital).permit!)
    end

    return render json: instance.errors, status: :unprocessable_entity if instance.errors.present?

    assessment_params.merge("#{type}_id": instance.id)
  end

  # TODO: retrun hash of params to update referral instead updating here
  def update_referral(assessment_params)
    update_referral = { adl_score_20: assessment_params[:adl_score_20], adl_score_100: assessment_params[:adl_score_100] }
    # Termination
    # if check_termination?(assessment_params)
    if check_termination?(assessment_params)
      update_referral = update_referral.merge(status: 'F')
    else
      update_referral = update_referral.merge(status: 'H')
    end

    # Patient type & ADL Score
    update_referral = update_referral.merge(
      patient_type: assessment_params[:patient_type]
    )

    @referral.update(update_referral)

    update_parent_and_child
  end

  def update_parent_and_child
    return if @referral.status != 'F'

    # if this referral is parent
    if @referral.chain_parent?
      child_chains = ReferralChain.where(parent_referral: @referral)

      has_continuous = false
      child_chains.each do |chain|
        has_continuous = chain.is_continuous if !has_continuous
        chain.referral.update(has_parent_opened: false)
      end
    end

    if @referral.chain_child?
      # get parent first
      parent_referral = @referral.chain_parent_referral
      return if parent_referral.nil?

      child_referrals = ReferralChain.where(parent_referral: parent_referral, is_continuous: false).map(&:referral)
      if child_referrals.all? { |r| r.status == 'F' || r.status == 'R' }
        parent_referral.update(has_children_opened: false)
      end
    end
  end

  def close_continuous(referral)
    # Find child continuous referrals
    chains = ReferralChain.where(parent_referral: referral, is_continuous: true)
    chains.each do |chain|
      referral = chain.referral
      referral.update(status: 'F')
    end
  end

  def check_termination?(assessment_params)
    if assessment_params.key?(:appointment)
      appointment = assessment_params.fetch(:appointment)
      return true if appointment == '2'
    end
    false
  end

  # PATCH/PUT /assessments/1
  # PATCH/PUT /assessments/1.json
  def update
    update_referral(assessment_params)
    check_continuous_referral

    if @assessment.update(assessment_params)
      render json: @assessment.as_json, status: :ok
    else
      render json: @assessment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /assessments/1
  # DELETE /assessments/1.json
  def destroy
    @assessment.destroy
    render json: { status: 'ok' }, status: :no_content
  end

  # GEt /assessments/history
  def history
    #  both of parallel and continuous referrals
    assessments = find_history(@referral.id, true)
    render json: assessments
  rescue StandardError
    render json: []
  end

  def find_associate_parent(list, last_id)
    list.prepend(last_id) unless list.include?(last_id)
    chains = ReferralChain.where("parent_referral_id = :id or referral_id = :id", id: last_id).map(&:parent_referral_id).uniq
    chains -= list
    chains.each do |c|
      find_associate_parent(list, c)
    end

    list
  end

  # GET /assessments/parallel
  def parallel
    assessments = assessment_from_chains(action_name, ignore_child: true)
    assessments.contcat(assessment_from_chains('parallel', ignore_child: true)) if action_name == 'continuous'
    # in case that this rerral has
    render json: assessments
  end

  # GET /assessments/continuous
  def continuous
    assessments = assessment_from_chains(action_name, ignore_child: true)
    render json: assessments
  end

  private

  def check_continuous_referral
    if assessment_params[:appointment] == "2" && assessment_params[:visit_terminated] == "9"
      hospital_destination = params[:other_hospital_destination]
      covisit_reason = params[:covisit_reason]

      new_referral = Referral.create(
        @referral.attributes.except('id', 'set_id', 'read', 'sent', 'created_at', 'updated_at', 'has_parent_opened', 'has_children_opened')
                            .merge(hospital: @hospital, hospital_source: @hospital, hospital_destination: hospital_destination, status: 'P', is_continuous: true, 
                                   has_parent_opened: false)
      )

      @referral.update(node: true)

      # Create with job
      CreateParalleledReferralJob.perform_later(
        referral_id: @referral.id,
        new_referral_id: new_referral.id,
        hospital: @hospital,
        hospital_destination: hospital_destination,
        covisit_reason: covisit_reason,
        is_continuous: true
      )
    end
  end

  def is_parent_referral_in_chain(referral)
    chain = referral.referral_chain
    raise StandardError, 'Chain not found' if chain.nil?

    return referral.id == chain.parent_referral.id ? true : false
  end

  def assessment_from_chains(name, ignore_child: false)
    assessments = []
    # is parent or child referrals
    chains = if @referral.chain_parent?
               ReferralChain.where(parent_referral: @referral, "is_#{name}": true)
             elsif !ignore_child && @referral.send("is_#{name}")
               parent_referral = @referral.referral_chain.parent_referral
               ReferralChain.where("parent_referral_id = :parent_referral_id and referral_id != :referral_id and is_#{name} = 'true'",
                                { parent_referral_id: parent_referral.id, referral_id: @referral.id })
             end
    assessments.concat(extract_assessments(chains))
  end

  def parent_assessment(referral)
    {
      referral: referral.set_id,
      hospital_source: Hospital.by_code(referral.hospital_source).as_json,
      hospital_destination: Hospital.by_code(referral.hospital_destination).as_json,
      assessments: referral.assessments.order(assessment_at: :desc).as_json(list: true)
    }
  end

  def extract_assessments(chains)
    assessments = []
    return assessments if chains.nil?
    chains.each do |chain|
      referral = chain.referral
      if referral.status != 'R'
        ams = {
          referral: referral.set_id,
          hospital_source: Hospital.by_code(referral.hospital_source).as_json,
          hospital_destination: Hospital.by_code(referral.hospital_destination).as_json,
          assessments: referral.assessments.order(assessment_at: :desc).as_json(list: true),
          is_parallel: referral.is_parallel,
          is_continuous: referral.is_continuous
        }
        assessments.push(ams)
      end
    end
    assessments
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_assessment
    @assessment = Assessment.find(params[:id])
  end

  def set_referral
    @referral = Referral.find(params[:referral_id]) if params.key?(:referral_id)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def assessment_params
    params.fetch(:assessment, {}).merge(referral_id: @referral.id).permit!
  end
end
