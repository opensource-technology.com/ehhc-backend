class AuthenticationController < ApplicationController
  skip_before_action :verify_authenticity_token

  def login
    # get auth provider from factory
    result = ProcessAuthenticateUser.call(user_params)
    if result.success?
      render json: result.authentication, status: :ok
    else
      render json: { code: result.error.code, message: t(result.error.message) }, status: :unauthorized
    end
  end

  def user_params
    params.require(:authentication).permit(:username, :password)
  end
end
