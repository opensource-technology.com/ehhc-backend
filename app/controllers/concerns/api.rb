require 'active_support/concern'

module Api
  extend ActiveSupport::Concern

  included do
    skip_before_action :verify_authenticity_token
    before_action :require_login, except: %w[test index_next]
  end
end
