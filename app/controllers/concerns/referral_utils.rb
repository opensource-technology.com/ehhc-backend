require 'active_support/concern'

module ReferralUtils
  extend ActiveSupport::Concern

  included do
    def build_patient_status_query(patient_status)
      if patient_status == '3'
        "((patient_type = '6' and CURRENT_DATE - effective_at > '30 day'::interval) or (patient_type != '6' and CURRENT_DATE - effective_at > '7 day'::interval))"
      elsif patient_status == '2'
        "((patient_type = '6' and CURRENT_DATE - effective_at > '28 day'::interval and CURRENT_DATE - effective_at < '30 day'::interval) or (patient_type != '6' and CURRENT_DATE - effective_at > '5 day'::interval and CURRENT_DATE - effective_at < '7 day'::interval))"
      elsif patient_status == '1'
        "((patient_type = '6' and CURRENT_DATE - effective_at > '0 day'::interval and CURRENT_DATE - effective_at < '28 day'::interval) or (patient_type != '6' and CURRENT_DATE - effective_at > '0 day'::interval and CURRENT_DATE - effective_at < '5 day'::interval))"
      else
        "effective_at is null"
      end
    end

    def calculate_patient_status
      today = Date.today
      @referrals.each do |referral|
        diff = referral.effective_at.present? ? (today - referral.effective_at.to_date).to_i : 0
        referral.patient_status = patient_status(referral.patient_type, diff)
      end
    end

    def render_referrals(referrals)
      pagination = paginate_by_items(referrals)
      render json: {
        referrals: referrals.as_json(api: true), pagination: pagination
      }, status: :ok
    end
  end
end
