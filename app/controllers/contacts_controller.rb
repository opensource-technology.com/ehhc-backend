class ContactsController < ApplicationController
  include Api

  before_action :set_patient
  before_action :set_contact, only: [:show, :edit, :update, :destroy]

  # GET /contacts
  # GET /contacts.json
  def index
    puts "Get all contacts"
    @contacts = @patient.contacts
    render json: @contacts.as_json(Contact::ALL_ATTRS), status: :ok
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
    if @contact.present?
      render json: @contact.as_json, status: :ok
    else
      render json: {}, status: :ok
    end
  end

  # GET /contacts/new
  def new
    @contact = Contact.new
  end

  # GET /contacts/1/edit
  def edit; end

  # POST /contacts
  # POST /contacts.json
  def create
    @contacts = @patient.contacts
    if @contacts[0].nil?
      @contact = Contact.new(contact_params.merge(hospital: @hospital))
      # @patient.contacts << @contact
      if @contact.save
        render json: @contact.as_json, status: :created
      else
        render json: @contact.errors, status: :unprocessable_entity
      end
    else
      @contact = @patient.contacts[0]
      if @contact.update(contact_params)
        render json: @contact.as_json, status: :ok
      else
        render json: @contact.errors, status: :unprocessable_entity
      end
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    if @contact.update(contact_params)
      render json: @contact.as_json, status: :ok
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact.destroy
    render json: { status: "OK" }, status: :no_content
  end

  private

  def set_patient
    @patient = Patient.find(params[:patient_id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_contact
    @contact = Contact.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def contact_params
    params.fetch(:contact, {}).merge(patient_id: @patient.id).permit!
  end
end
