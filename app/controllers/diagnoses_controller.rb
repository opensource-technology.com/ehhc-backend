class DiagnosesController < ApplicationController
  include Api

  before_action :set_referral
  before_action :set_visit
  before_action :set_diagnosis, only: [:show, :edit, :update, :destroy]

  # GET /diagnoses
  # GET /diagnoses.json
  def index
    if @visit.present?
      @diagnoses = @visit.diagnoses
      ApiCallLogger.info "Get diagnoses"
      render json: @diagnoses.as_json(Diagnosis::ATTRS), status: :ok
    else
      render json: { code: '31', message: 'Referral ID is missing.' }, status: :bad_request
    end
  end

  # GET /diagnoses/1
  # GET /diagnoses/1.json
  def show
    if @diagnosis.present?
      ApiCallLogger.info "Get diagnosis #{@diagnosis.id}"
      render json: @diagnosis.as_json(Diagnosis::ATTRS), status: :ok
    else
      render json: {}, status: :ok
    end
  end

  # GET /diagnoses/new
  def new
    @diagnosis = Diagnosis.new
  end

  # GET /diagnoses/1/edit
  def edit; end

  # POST /diagnoses
  # POST /diagnoses.json
  def create
    # receive array of diagnoses
    @diagnoses = []
    # p params
    if params.key?(:_json)
      diagnoses = params.fetch(:_json, [])
      diagnoses.each.with_index do |diagnose_param, index|
        @visit.update(diagnosis: diagnose_param[:disease_text]) if index.zero?

        if diagnose_param.key?(:set_id)
          # Update diagnose
          @diagnosis = Diagnosis.find_by set_id: diagnose_param[:set_id]
          @diagnosis.update diagnose_param.permit!
          @diagnoses << @diagnosis
        else
          # Create new one
          @diagnosis = Diagnosis.new diagnose_param.merge(visit_id: @visit.id, hospital: @hospital).permit!
          @diagnoses << @diagnosis if @diagnosis.save
        end
      end
      render json: @diagnoses.as_json, status: :ok
    end
  end

  # PATCH/PUT /diagnoses/1
  # PATCH/PUT /diagnoses/1.json
  def update
    if @diagnosis.update(diagnosis_params)
      ApiCallLogger.info "Update diagnosis #{@diagnosis.id}"
      render json: @diagnosis.as_json(Diagnosis::ATTRS), status: :ok
    else
      render json: @diagnosis.errors, status: :unprocessable_entity
    end
  end

  # DELETE /diagnoses/1
  # DELETE /diagnoses/1.json
  def destroy
    @diagnosis.destroy
    render json: { status: "OK" }, status: :no_content
  end

  # GET /diagnoses/search
  # Search list of diagnoses by key words
  def search
    @diagnoses = RDiagnosis.where("label ilike ? or code ilike ?", "%#{params[:query]}%", "#{params[:query]}%").limit(20)
    render json: { status: "OK", search: @diagnoses }
  end

  def update_disease_text
    @diagnoses = @visit.diagnoses
    @visit.update(diagnosis: diagnosis_params[:disease_text])
    @diagnoses.each do |diagnose|
      diagnose.update(diagnosis_params.permit(:disease_text))
    end
    render json: { status: 'OK' }, status: :ok
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_diagnosis
    @diagnosis = Diagnosis.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    # do nothing
    ApiCallLogger.error "Not found diagnosis"
  end

  def set_visit
    @visit = Visit.find(params[:visit_id]) if params.key?(:visit_id)
  end

  def set_referral
    @referral = Referral.find(params[:referral_id]) if params.key?(:referral_id)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def diagnosis_params
    params.fetch(:diagnosis, {}).merge(visit_id: @visit.id).permit!
  end
end
