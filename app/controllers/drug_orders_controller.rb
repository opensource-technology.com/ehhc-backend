class DrugOrdersController < ApplicationController
  include Api

  before_action :set_referral, only: [:index, :create, :edit, :update, :destroy]
  before_action :set_drug_order, only: [:show, :edit, :update, :destroy]

  # GET /drug_orders
  # GET /drug_orders.json
  def index
    @drug_orders = DrugOrder.all
    render json: @drug_orders.as_json(DrugOrder::ATTRS), status: :ok
  end

  # GET /drug_orders/1
  # GET /drug_orders/1.json
  def show
    if @drug_orders.present?
      render json: @drug_orders.as_json(DrugOrder::ATTRS), status: :ok
    else
      render json: {}, status: :ok
    end
  end

  # GET /drug_orders/new
  def new
    @drug_order = DrugOrder.new
  end

  # GET /drug_orders/1/edit
  def edit
  end

  # POST /drug_orders
  # POST /drug_orders.json
  def create
    @drug_order = DrugOrder.new(drug_order_params)
    if @drug_order.save
      render json: @drug_order.as_json(DrugOrder::ATTRS), status: :created
    else
      render json: @drug_order.errors, status: :unprocessable_entity 
    end
  end

  # PATCH/PUT /drug_orders/1
  # PATCH/PUT /drug_orders/1.json
  def update
    if @drug_order.update(drug_order_params)
      render json: @drug_order, status: :ok
    else
      render json: @drug_order.errors, status: :unprocessable_entity
    end
  end

  # DELETE /drug_orders/1
  # DELETE /drug_orders/1.json
  def destroy
    @drug_order.destroy
    render json: { status: 'ok' }, status: :no_content
  end

  private
  # Set referral if referral_id exists
  def set_referral
    if params.key?(:referral_id)
      @referral = Referral.find(params[:referral_id])
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_drug_order
    @drug_order = DrugOrder.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def drug_order_params
    params.fetch(:drug_order, {}).merge(referral_id: @referral.id).permit!
  end
end
