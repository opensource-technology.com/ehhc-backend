class EmployeesController < ApplicationController
  before_action :set_employee, only: %i[show edit update destroy]

  # GET /employees
  # GET /employees.json
  def index
    p session[:hospcode]
    puts "Current Hospcode: #{session[:hospcode]}"
    @employees = Provider.where(hospital: session[:hospcode])
  end

  # GET /employees/1
  # GET /employees/1.json
  def show
  end

  # GET /employees/new
  def new
    @employee = Provider.new
    @prenames = MPrename.all
    @position_types = ProviderPosition.all
  end

  # GET /employees/1/edit
  def edit
    @prenames = MPrename.all
    @position_types = ProviderPosition.all

    p @employee
  end

  # POST /employees
  # POST /employees.json
  def create
    @employee = Provider.new(employee_params)
    @employee.hospital = current_user.hospcode # Fix

    respond_to do |format|
      if @employee.save
        format.html { redirect_to employees_url, notice: 'Employee was successfully created.' }
        format.json { render :show, status: :created, location: @employee }
      else
        @prename_list = MPrename.all
        @position_types = ProviderPosition.all
        format.html { render :new }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to employees_url, notice: 'Employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to employees_url, notice: 'Employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_employee
    @employee = Provider.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def employee_params
    p current_user.hospcode
    puts "Current hospcode"
    params.fetch(:provider, {}).merge(hospital: current_user.hospcode).permit!
  end
end
