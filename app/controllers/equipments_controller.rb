class EquipmentsController < ApplicationController
  include Api

  before_action :set_referral, only: [:index, :create, :edit, :update, :destroy]
  before_action :set_equipment, only: [:show, :edit, :update, :destroy]

  # GET /equipment
  # GET /equipment.json
  def index
    # @equipment = Equipment.all
    attrs = {
      include: {
        referral: {
          only: [:id]
        },
        type: {
          only: [:id, :code, :label]
        }
      }, except: [:referral_id, :type_id]
    }

    render json: @referral.equipments.as_json
  end

  # GET /equipment/1
  # GET /equipment/1.json
  def show; end

  # GET /equipment/new
  def new
    @equipment = Equipment.new
  end

  # GET /equipment/1/edit
  def edit; end

  # POST /equipments
  # POST /equipments.json
  def create
    @equipment = Equipment.new(equipment_params.merge(hospital: @hospital))
    if @equipment.save
      render json: @equipment, status: :created
    else
      render json: @equipment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /equipment/1
  # PATCH/PUT /equipment/1.json
  def update
    if @equipment.update(equipment_params)
      render json: @equipment, status: :ok
    else
      render json: @equipment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /equipment/1
  # DELETE /equipment/1.json
  def destroy
    @equipment.destroy
    render json: { status: 'ok' }, status: :no_content
  end

  private

  # Set referral if referral_id exists
  def set_referral
    @referral = Referral.find(params[:referral_id]) if params.key?(:referral_id)
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_equipment
    @equipment = Equipment.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def equipment_params
    if @referral.present?
      params.fetch(:equipment, {}).merge(referral_id: @referral.id).permit!
    else
      params.fetch(:equipment, {}).permit!
    end
  end
end
