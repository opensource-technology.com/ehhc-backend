class HomeController < ApplicationController
  def health
    render json: { status: 'online' }
  end

  def status
    render plain: metric_message, status: :ok
  end

  private

  def metric_message
    <<~Q.freeze
      # HELP rails_status rails_status
      # TYPE rails_status gauge
      rails_status 1
      # HELP rails_mem rails_mem
      # TYPE rails_mem gauge
      rails_mem 1820
    Q
  end
end
