class MultidisciplinaryTeamsController < ApplicationController
  include Api

  before_action :set_multidisciplinary_team, only: [:show, :edit, :update, :destroy]

  # GET /multidisciplinary_teams
  # GET /multidisciplinary_teams.json
  def index
    @multidisciplinary_teams = MultidisciplinaryTeam.all
  end

  # GET /multidisciplinary_teams/1
  # GET /multidisciplinary_teams/1.json
  def show
  end

  # GET /multidisciplinary_teams/new
  def new
    @multidisciplinary_team = MultidisciplinaryTeam.new
  end

  # GET /multidisciplinary_teams/1/edit
  def edit
  end

  # POST /multidisciplinary_teams
  # POST /multidisciplinary_teams.json
  def create
    @multidisciplinary_team = MultidisciplinaryTeam.new(multidisciplinary_team_params)

    respond_to do |format|
      if @multidisciplinary_team.save
        format.html { redirect_to @multidisciplinary_team, notice: 'Multidisciplinary team was successfully created.' }
        format.json { render :show, status: :created, location: @multidisciplinary_team }
      else
        format.html { render :new }
        format.json { render json: @multidisciplinary_team.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /multidisciplinary_teams/1
  # PATCH/PUT /multidisciplinary_teams/1.json
  def update
    respond_to do |format|
      if @multidisciplinary_team.update(multidisciplinary_team_params)
        format.html { redirect_to @multidisciplinary_team, notice: 'Multidisciplinary team was successfully updated.' }
        format.json { render :show, status: :ok, location: @multidisciplinary_team }
      else
        format.html { render :edit }
        format.json { render json: @multidisciplinary_team.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /multidisciplinary_teams/1
  # DELETE /multidisciplinary_teams/1.json
  def destroy
    @multidisciplinary_team.destroy
    render json: { status: "OK" }, status: :no_content
  end

  private

  def set_multidisciplinary_team
    @multidisciplinary_team = MultidisciplinaryTeam.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def multidisciplinary_team_params
    params.fetch(:multidisciplinary_team, {})
  end
end
