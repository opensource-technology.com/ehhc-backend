class PatientAssessmentEquipmentsController < ApplicationController
  include Api

  before_action :set_patient_assessment_equipment, only: [:show, :edit, :update, :destroy]

  # GET /patient_assessment_equipments
  # GET /patient_assessment_equipments.json
  def index
    @patient_assessment_equipments = PatientAssessmentEquipment.all
  end

  # GET /patient_assessment_equipments/1
  # GET /patient_assessment_equipments/1.json
  def show
  end

  # GET /patient_assessment_equipments/new
  def new
    @patient_assessment_equipment = PatientAssessmentEquipment.new
  end

  # GET /patient_assessment_equipments/1/edit
  def edit
  end

  # POST /patient_assessment_equipments
  # POST /patient_assessment_equipments.json
  def create
    @patient_assessment_equipment = PatientAssessmentEquipment.new(patient_assessment_equipment_params)

    respond_to do |format|
      if @patient_assessment_equipment.save
        format.html { redirect_to @patient_assessment_equipment, notice: 'Patient assessment equipment was successfully created.' }
        format.json { render :show, status: :created, location: @patient_assessment_equipment }
      else
        format.html { render :new }
        format.json { render json: @patient_assessment_equipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /patient_assessment_equipments/1
  # PATCH/PUT /patient_assessment_equipments/1.json
  def update
    respond_to do |format|
      if @patient_assessment_equipment.update(patient_assessment_equipment_params)
        format.html { redirect_to @patient_assessment_equipment, notice: 'Patient assessment equipment was successfully updated.' }
        format.json { render :show, status: :ok, location: @patient_assessment_equipment }
      else
        format.html { render :edit }
        format.json { render json: @patient_assessment_equipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patient_assessment_equipments/1
  # DELETE /patient_assessment_equipments/1.json
  def destroy
    @patient_assessment_equipment.destroy
    render json: { status: "OK" }, status: :no_content
  end

  private

  def set_patient_assessment_equipment
    @patient_assessment_equipment = PatientAssessmentEquipment.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def patient_assessment_equipment_params
    params.fetch(:patient_assessment_equipment, {})
  end
end
