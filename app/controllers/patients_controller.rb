# Patient Controller
class PatientsController < ApplicationController
  # skip_before_action :verify_authenticity_token
  include Api

  before_action :set_patient, only: %i[show edit update destroy]

  # pagination
  before_action :current_page, only: [:index]

  # GET /patients
  # GET /patients.json
  def index
    @patients = Patient.where(hospital: @hospital).order(:id).page(current_page)
    pagination = paginate_by_items(@patients)
    ApiCallLogger.info "Get patients [page: @page]"

    render json: { patients: @patients.as_json(Patient::ATTRS), pagination: pagination }, status: :ok
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
    if @patient.present?
      ApiCallLogger.info "Get patient #{@patient.id}"
      render json: @patient.as_json(Patient::ALL_ATTRS), status: :ok
    else
      render json: {}, status: :no_content
    end
  end

  # GET /patients/new
  def new
    @patient = Patient.new
  end

  # GET /patients/1/edit
  def edit; end

  # POST /patients
  # POST /patients.json
  def create
    @patient = Patient.create(patient_params.merge(hospital: @hospital))
    ApiCallLogger.info "Create new patient #{@patient.id}"
    render json: @patient.to_json, status: :ok
  rescue ActiveRecord::RecordNotUnique
    ApiCallLogger.error "Can not create new patient, Patient already exist"
    render json: {
      code: Error::PATIENT_ALREADY_EXIST.code,
      message: t(Error::PATIENT_ALREADY_EXIST.message)
    }, status: :bad_request
  rescue StandardError
    ApiCallLogger.error "Can not create new patient, Missing parameters"
    render json: {
      code: Error::MISSING_PARAMETER.code,
      message: t(Error::MISSING_PARAMETER.message)
    }, status: :bad_request
  end

  # PATCH/PUT /patients/1
  # PATCH/PUT /patients/1.json
  def update
    if @patient.update(patient_params.except(:creator).permit!)
      ApiCallLogger.info "Update patient #{@patient.id}"
      render json: @patient.as_json, status: :ok
    else
      ApiCallLogger.error "Can not update patient"
      render json: @patient.errors, status: :unprocessable_entity
    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    @patient.destroy
    ApiCallLogger.info "Delete patient #{@patient.id}"
    render json: { status: 'OK' }, status: :no_content
  end

  def search
    query = params.fetch(:q, '')
    patients = Patient.search(query, @hospital)
    render json: {
      status: 'OK',
      patients: patients.as_json(search: true)
    }, status: :ok
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_patient
    @patient = Patient.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def patient_params
    params.fetch(:patient).merge(creator: current_authenticated_user).permit!
  end

  def invalid_login_attempt
    render json: {
      code: Error::USERNAME_PASSWORD_MISMATCH.code,
      message: t(Error::USERNAME_PASSWORD_MISMATCH.message)
    }, status: :unauthorized
  end
end
