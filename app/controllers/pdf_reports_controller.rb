require 'action_view'

class PdfReportsController < ApplicationController
  include ActionView::Helpers::DateHelper
  include PdfReportsHelper
  layout 'report'

  def assessments_history
    @id = params[:id]
    @parallel = params[:parallel]
    @continuous = params[:continuous]
    @assessments_history = find_history(@id, false).select{ |assessment| func_select_assessments(assessment,  @parallel, @continuous) }
    name = 'assessments_history'
    from_time = Time.now
    filename = "#{name}_#{@id}_#{from_time.strftime('%Y%m%d%H%M%S')}"
    respond_to do |format|
      format.html { render template: 'pdf_reports/assessments_history' }
      format.pdf { render_pdf(name, filename, from_time) }
    end
  end

  def hhc
    where_param = params.key?(:set_id) ? { set_id: params[:set_id] } : { id: params[:id] }
    @referral = Referral.includes(:assessments)
                        .order('assessments.assessment_at')
                        .find_by(where_param)

    name = 'hhc'
    from_time = Time.now
    filename = "#{name}_#{@referral[:set_id]}_#{from_time.strftime('%Y%m%d%H%M%S')}"
    respond_to do |format|
      format.html { render template: 'pdf_reports/hhc' }
      format.pdf { render_pdf(name, filename, from_time) }
    end
  end

  def refer
    where_param = params.key?(:set_id) ? { set_id: params[:set_id] } : { id: params[:id] }
    @referral = Referral.includes(:assessments)
                        .order('assessments.assessment_at')
                        .find_by(where_param)
    name = 'referral'
    from_time = Time.now
    filename = "#{name}_#{@referral[:set_id]}_#{from_time.strftime('%Y%m%d%H%M%S')}"
    respond_to do |format|
      format.html { render template: 'pdf_reports/referral' }
      format.pdf { render_pdf(name, filename, from_time) }
    end
  end

  def assessment
    send_file Rails.public_path.join('hhc_case_v2.pdf'), type: 'application/pdf', disposition: 'inline'
  end

  def render_pdf(name, filename, from_time)
    render(
      pdf: filename,
      template: "pdf_reports/#{name}", dpi: 300,
      page_size: 'A4', viewport_size: '1280x1024', zoom: 0.78,
      footer: {
        left: "#{t(:page_label)} [page] #{t(:from_label)} [topage]",
        right: "#{t(:print_at_label)} #{from_time.strftime('%d/%m/%Y %H:%M')}",
        font_name: 'sans-serif, serif', font_size: 8
      },
      encoding: 'UTF-8',
      layout: 'report'
    )
  end
end
