class ProvidersController < ApplicationController
  include Api

  before_action :set_provider, only: %i[show edit update destroy], expect: [:search]

  # GET /providers
  # GET /providers.json
  def index
    @providers = Provider.where(hospital: @hospital)
    render json: @providers.as_json(Provider::ATTRS), status: :ok
  end

  # GET /providers/1
  # GET /providers/1.json
  def show; end

  # GET /providers/new
  def new
    @provider = Provider.new
  end

  # GET /providers/1/edit
  def edit; end

  # POST /providers
  # POST /providers.json
  def create
    @provider = Provider.new(provider_params.merge(hospital: @hospital))

    respond_to do |format|
      if @provider.save
        format.html { redirect_to @provider, notice: 'Provider was successfully created.' }
        format.json { render :show, status: :created, location: @provider }
      else
        format.html { render :new }
        format.json { render json: @provider.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /providers/1
  # PATCH/PUT /providers/1.json
  def update
    respond_to do |format|
      if @provider.update(provider_params)
        format.html { redirect_to @provider, notice: 'Provider was successfully updated.' }
        format.json { render :show, status: :ok, location: @provider }
      else
        format.html { render :edit }
        format.json { render json: @provider.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /providers/1
  # DELETE /providers/1.json
  def destroy
    @provider.destroy
    respond_to do |format|
      format.html { redirect_to providers_url, notice: 'Provider was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Search for provider list
  def search
    @providers = if params.key?(:query)
                   puts params[:query]
                   Provider.where("(first_name ilike ? or provider_identifier ilike ?) and hospital = ?", "#{params[:query]}%", "#{params[:query]}%", @hospital)
                 else
                   @providers = Provider.where("hospital = ?", @hospital).order(:first_name)
                 end

    render json: { status: "OK", search: @providers }
  end

  # Find provider by hospcode
  def filter
    site = params.fetch('site').split(",")
    @providers = Provider.where(hospital: [site])
  end

  private

  def set_provider
    @provider = Provider.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def provider_params
    params.fetch(:provider, {})
  end
end
