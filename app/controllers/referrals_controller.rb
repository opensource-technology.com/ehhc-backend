# Referral Controller:
class ReferralsController < ApplicationController
  include Api
  include ReferralsHelper
  include ReferralUtils

  before_action :current_page, only: %i[index inbound outbound queue waiting onsite]
  before_action :set_referral, only: %i[show edit refer accept update flush update_status destroy new_paralleled_referral]

  LIMIT = 30

  # GET /referrals
  # GET /referrals.json
  def index
    query, query_params = referral_query_params(['I']) do |query, query_params|
      query.concat(" AND (hospital_source = :hospital OR hospital_destination = :hospital)")
      query_params[:hospital] = @hospital
    end

    @referrals = Referral.where(query, query_params).offset(@offset).limit(@limit).page(current_page)
    render_referrals(@referrals)
  end

  # GET /referrals/1
  # GET /referrals/1.json
  def show
    ApiCallLogger.info "Get referral #{@referral.id}"
    if params.key?(:ehhc)
      render json: @referral.as_json(ehhc: true)
    else
      render json: @referral.as_json(Referral::ALL_ATTRS)
    end
  end

  # GET /referrals/new
  def new
    @referral = Referral.new
  end

  # GET /referrals/1/edit
  def edit; end

  # POST /referrals
  # POST /referrals.json
  def create
    @referral = Referral.new(referral_params.except(REFERRAL_CHAIN_PARAMS).merge(hospital: @hospital))
    if @referral.save
      ApiCallLogger.info "Create new referral #{@referral.id}"
      render json: @referral.as_json, status: :created
    else
      ApiCallLogger.error "Cannot create new referral"
      render json: @referral.errors, status: :unprocessable_entity
    end
  end

  def create_first_visit
    return unless @referral.visits.length.zero?

    Visit.create(hospital_primary: @hospital, referral_id: @referral.id,
                 admit_at: Time.now, hospital: @hospital)
    @referral.reload
  end

  # PATCH/PUT /referrals/1
  # PATCH/PUT /referrals/1.json
  def update
    create_first_visit if @referral.status == 'I' || @referral.status == 'D' # only status 'I' or 'D' can update referral

    result = ResponseReferral.call referral: @referral, **referral_params.to_h.symbolize_keys
    referral = result.referral
    if result.success?
      ApiCallLogger.info "Update referral #{referral.id}"
      render json: referral.as_json(Referral::ALL_ATTRS), status: :ok
    else
      ApiCallLogger.error "Cannot update referral #{referral.id}"
      render json: referral.errors, status: :unprocessable_entity
    end
  end

  # DELETE /referrals/1
  # DELETE /referrals/1.json
  def destroy
    @referral.destroy
    ApiCallLogger.info "Delete referral #{@referral.id}"
    respond_to do |format|
      format.html { redirect_to referrals_url, notice: 'Referral was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Get referral info from inbound, outbound and queue
  # @return Object json object
  def info
    # get inbound
    total_inbound = inbound_referral(@hospital).except(:order, :offset, :limit).count
    total_outbound = outbound_referral(@hospital).except(:order, :offset, :limit).count
    total_queue = queue_referral(@hospital).except(:order, :offset, :limit).count

    # Parallel and Continuous
    query = "referral_chains.hospital_destination = '#{@hospital}' and referral_chains.is_parallel = true"
    total_parallel_inbound = find_parallel(query).count
    query = "referral_chains.hospital_source = '#{@hospital}' and referral_chains.is_parallel = true"
    total_parallel_outbound = find_parallel(query).count
    query = "referral_chains.hospital_destination = '#{@hospital}' and referral_chains.is_continuous = true"
    total_continuous_inbound = find_continuous(query).count
    query = "referral_chains.hospital_source = '#{@hospital}' and referral_chains.is_continuous = true"
    total_continuous_outbound = find_continuous(query).count

    ApiCallLogger.info "Get all referrals info"
    render json: {
      total_inbound: total_inbound,
      total_outbound: total_outbound,
      total_queue: total_queue,
      total_parallel_inbound: total_parallel_inbound,
      total_parallel_outbound: total_parallel_outbound,
      total_continuous_inbound: total_continuous_inbound,
      total_continuous_outbound: total_continuous_outbound
    }
  end

  def inbound_referral_query(hospital, more_query = nil) # :nodoc:
    referral_query_params do |query, query_params|
      query.concat(' AND referrals.hospital_destination = :hospital') unless user_info.roles.include?("admin_nhso")
      query.concat(" AND is_parallel = false AND is_continuous = false")
      query_params[:hospital] = hospital unless user_info.roles.include?("admin_nhso")
    end
  end

  def inbound_referral(hospital, more_query = nil)
    query_str, query_params = inbound_referral_query hospital, more_query
    find_referrals(OpenStruct.new(query_str: query_str, query_params: query_params, hospital: hospital))
  end

  def outbound_referral_query(hospital, more_query = nil)
    referral_query_params do |query, query_params|
      query.concat(' AND referrals.hospital_source = :hospital AND referrals.hospital_destination <> :hospital')
      # query.concat(" AND #{more_query}") unless more_query.nil?
      query.concat(" AND is_parallel = false AND is_continuous = false")
      query_params[:hospital] = hospital
    end
  end

  def outbound_referral(hospital, more_query = nil)
    query_str, query_params = outbound_referral_query hospital, more_query
    find_referrals(OpenStruct.new(query_str: query_str, query_params: query_params))
  end

  def queue_referral_query(hospital, more_query = nil)
    referral_query_params(['D']) do |query, query_params|
      query.concat(' AND referrals.hospital_source = :hospital')
      query.concat(" AND is_parallel = false AND is_continuous = false")
      query_params[:hospital] = hospital
    end
  end

  def queue_referral(hospital, more_query = nil)
    query_str, query_params = queue_referral_query hospital, more_query
    find_referrals(OpenStruct.new(query_str: query_str, query_params: query_params))
  end

  def waiting_referral_query(hospital, more_query = nil)
    referral_query_params(['A', 'H']) do |query, query_params|
      type = params.fetch(:type, nil)
      if type == '1'
        query.concat(" AND ((referrals.patient_type != '6' and current_date - referrals.effective_at between '1' and '4') or
                         (referrals.patient_type = '6' and current_date - referrals.effective_at between '1' and '27')) ")
      elsif type == '2'
        query.concat(" AND ((referrals.patient_type != '6' and current_date - referrals.effective_at between '5' and '6') or
          (referrals.patient_type = '6' and current_date - referrals.effective_at between '1' and '27')) ")
      elsif type == '3'
        query.concat(" AND ((referrals.patient_type != '6' and current_date - referrals.effective_at > '7') or
          (referrals.patient_type = '6' and current_date - referrals.effective_at > '30')) ")
      end

      query.concat(' AND referrals.hospital_destination = :hospital')
      query_params[:hospital] = hospital

      patient_status_params = params.fetch('patient_status', nil)
      if patient_status_params.present?
        if query.nil?
          query.concat(build_patient_status_query(patient_status_params)) 
        else
          query.concat(" AND #{build_patient_status_query(patient_status_params)}")
        end
      end
    end
  end

  def waiting_referral(hospital, more_query = nil)
    query_str, query_params = waiting_referral_query hospital, more_query
    find_referrals(OpenStruct.new(query_str: query_str, query_params: query_params))
  end

  # Find referrals by query
  # @param [String] query
  # @return [list] referral list
  def find_referrals(query)
    Referral.referrals(query).page(current_page)
  end

  def find_referrals_by_type(type)
    search_query = nil

    @referrals = if type == 'inbound'
                   inbound_referral(@hospital, search_query).page(current_page)
                 elsif type == 'outbound'
                   outbound_referral(@hospital, search_query).page(current_page)
                 elsif type == 'queue'
                   queue_referral(@hospital, search_query).page(current_page)
                 elsif type == 'waiting'
                   waiting_referral(@hospital, search_query).page(current_page)
                 end

   calculate_patient_status
    @referrals
  end

  # Get inbound referral
  # There are 5 statuses for referral
  # [P] Pending
  # [R] Rejected
  # [H] HHC
  # [F] Final
  # [A] Accept
  # @return [Object] referral json
  def inbound
    referrals = find_referrals_by_type('inbound')
    render_referrals(referrals)
  end

  # Center
  def outbound
    @referrals = find_referrals_by_type('outbound')
    render_referrals(@referrals)
  end

  # Center
  def queue
    referrals = find_referrals_by_type('queue')
    render_referrals(referrals)
  end

  # On site
  # Query Params:
  # - patient_status: 3, 2, 1, 0
  def waiting
    @referrals = find_referrals_by_type('waiting')
    # ApiCallLogger.info "Get waiting [page:#{@page}]"
    render_referrals(@referrals)
  end

  def refer
    unless params.key?(:destination)
      return render json: {
        code: Error::REFERRAL_DESTINATION_IS_REQUIRED.code,
        message: t(Error::REFERRAL_DESTINATION_IS_REQUIRED.message)
      }, status: :bad_request
    end
    # TODO: Refer patient to another hospital
    result = ReferPatient.call referral: @referral, destination: params[:destination]
    ApiCallLogger.info "Send refer from #{@referral.hospital_source} to #{@referral.hospital_destination}" if result.success?

    if @referral.is_continuous
      # Update referral chain
      @referral.referral_chain.update(hospital_destination: params[:destination])
    end
    # Expect to always success
    render json: { status: 'OK' }, status: :ok
  end

  # Update referral status
  # @param params [Parameter] request parameter object
  # @return json [Status]
  def update_status
    result = ResponseReferral.call referral: @referral, **status_params
    if result.success?
      # Call update_parent_status interactor
      UpdateParentReferralStatus.call referral: @referral
      ApiCallLogger.info "Update referral status"
      render json: { status: 'OK' }, status: :ok
    else
      ApiCallLogger.error "Cannot update referral status"
      render json: { status: 'failed' }, status: :bad_request
    end
  end

  def status_params
    {
      status: params[:status],
      response_reason: params[:response_reason],
      hospital_destination: params[:hospital_destination]
    }
  end

  # Paralled Referral
  def new_paralleled_referral
    # Create referral chain
    # params:
    # - hospital_destination
    # - speciality
    # - covisit_reason
    # TODO: Move to interactor later
    hospital_destination = params[:hospital_destination]
    speciality = params[:speciality]
    speciality_other = params[:speciality_other]
    covisit_reason = params[:covisit_reason]
    # Create referal chain current
    # Create new referral
    new_referral = Referral.create(
      @referral.attributes.except('id', 'set_id', 'read', 'sent', 'created_at', 'updated_at', 'has_parent_opened', 'has_children_opened')
                          .merge(hospital: @hospital, hospital_source: @hospital, hospital_destination: hospital_destination, status: 'P', 
                                 is_continuous: false, is_parallel: true, has_parent_opened: true, node: false)
    )

    @referral.update(has_children_opened: false, node: true)

    # Create with job
    CreateParalleledReferralJob.perform_later(
      referral_id: @referral.id,
      new_referral_id: new_referral.id,
      hospital: @hospital,
      hospital_destination: hospital_destination,
      speciality: speciality,
      speciality_other: speciality_other,
      covisit_reason: covisit_reason,
      is_parallel: true
    )

    render json: {
      status: 'ok',
      new_referral_id: new_referral.id
    }
  end

  # Get paralleled referral
  def find_parallel(query_str = nil, query_params = {})
    Referral.parallel(OpenStruct.new(query_str: query_str, query_params: query_params))
  end

  def find_continuous(query_str = nil, query_params = {})
    Referral.continuous(OpenStruct.new(query_str: query_str, query_params: query_params))
  end

  def parallel_inbound
    query, query_params = referral_query_params do |query, query_params|
      q = "referral_chains.hospital_destination = :hospital and referral_chains.is_parallel = true"
      q.prepend(' and ') if !query.empty?
      query.concat(q)
      query_params[:hospital] = @hospital
    end

    @referrals = find_parallel(query, query_params).page(current_page)
    calculate_patient_status
    render_referrals(@referrals)
  end

  def parallel_outbound
    query, query_params = referral_query_params do |query, query_params|
      q = "referral_chains.hospital_source = :hospital and referral_chains.is_parallel = true"
      q.prepend(' and ') if !query.empty?
      query.concat(q)
      query_params[:hospital] = @hospital
    end 

    @referrals = find_parallel(query, query_params).page(current_page)
    calculate_patient_status
    render_referrals(@referrals)
  end

  def continuous_inbound
    query, query_params = referral_query_params do |query, query_params|
      q = "referral_chains.hospital_destination = :hospital and referral_chains.is_continuous = true"
      q.prepend(' and ') if !query.empty?
      query.concat(q)
      query_params[:hospital] = @hospital
    end

    @referrals = find_continuous(query, query_params).page(current_page)
    calculate_patient_status
    render_referrals(@referrals)
  end

  def continuous_outbound
    query, query_params = referral_query_params do |query, query_params|
      q = "referral_chains.hospital_source = :hospital and referral_chains.is_continuous = true"
      q.prepend(' and ') if !query.empty?
      query.concat(q)
      query_params[:hospital] = @hospital
    end

    @referrals = find_continuous(query, query_params).page(current_page)
    calculate_patient_status
    render_referrals(@referrals)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_referral
    @referral = Referral.includes(:assessments, :patient).where(id: params[:id]).order('assessments.assessment_at DESC').first
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def referral_params
    referral_param = params.fetch(:referral, {})
    referral_param = referral_param.merge(hospital_source: @hospital) unless @referral && @referral.hospital_source.present?
    referral_param.permit!
  end

  def referral_chain_params
    referral_param = params.fetch(:referral, {})
    referral_param = referral_param.merge(hospital_source: @hospital) unless @referral && @referral.hospital_source.present?
    referral_param.permit(REFERRAL_CHAIN_PARAMS).permit!
  end

  REFERRAL_CHAIN_PARAMS = %i[
    status hospital_source hospital_destination response_reason response_refer_at
    covisit_reason speciality speciality_other host_status
  ].freeze
end
