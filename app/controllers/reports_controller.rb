require 'ostruct'
require 'ehhc/jsonwebtoken'
require 'object_result'

class ReportsController < ApplicationController
  layout 'reports'

  before_action :validate_token, :new_searchable

  def index; end

  def hospital_filter_by_type
    results = hospitals_select_by_htype params[:htype].to_i
    render json: results.as_json, status: :ok
  end

  def providers_filter
    site1 = params.fetch('site1')
    site2 = params.fetch('site2')
    @providers = providers_filter_by_type(site1, site2)
    render json: @providers.as_json, status: :ok
  end

  # reports --
  def site_referral_patients
    @roles = 'site'
    @searchable.report_type = 'refers'
    @searchable.template = 'reports/site/referral_patients'
    @searchable.hospital = @hospital
    @providers = get_providers_map(@hospital)
    @hospitals_select = hospitals_select_not(@hospital)
    return if @searchable.init

    find_patients_report_details('site', "referral_patients")
  end

  def site_assessment_patients
    @roles = 'site'
    @searchable.report_type = 'assessment'
    @searchable.hospital = @hospital
    @providers = get_providers_map(@hospital)
    @hospitals_select = hospitals_select_not(@hospital)
    return if @searchable.init

    patients_report("assessment_patients")
  end

  def admin_referral_patients
    admin_patients_report("referral_patients")
  end

  def admin_assessment_patients
    admin_patients_report("assessment_patients")
  end

  def site_referral_patients_details
    @searchable.hospital = @hospital
    find_patients_report_details('site', "referral_patients")
  end

  def site_assessment_patients_details
    @searchable.hospital = @hospital
    find_patients_report_details('site', "assessment_patients")
  end

  def admin_referral_patients_details
    find_patients_report_details('admin', "referral_patients")
  end

  def admin_assessment_patients_details
    find_patients_report_details('admin', "assessment_patients")
  end

  private

  def providers_filter_by_type(site1, site2)
    @providers1 = unless (site1.eql?('null') || site1.eql?('all'))
                    get_providers(site1)
                  else
                    []
                  end
    @providers2 = unless site1 == site2 || (site2.eql?('null') || site2.eql?('all'))
                    get_providers(site2)
                  else
                    []
                  end
    return @providers1 + @providers2
  end

  def admin_patients_report(report_type)
    @roles = 'admin'
    @providers = []
    @hospitals_select =  hospitals_select_by_htype(@searchable.htype)
    return if @searchable.init

    patients_report(report_type)
  end

  def find_patients_report_details(roles, report_type)
    @roles = roles
    patients_report_detail(report_type)
  end

  def hospitals_select_by_htype(htype)
    if htype.to_i == 0
      Hospital.where.not(htype: [1, 3, 4])
    else
      Hospital.where(htype: htype.to_i || 1)
    end.map { |h| [h.label, h.code] }
  end

  def hospitals_select_not(hospital)
    Hospital.where.not(code: hospital).map { |h| [h.label, h.code] }
  end

  def get_providers(hospital)
    is_select = %i[set_id first_name last_name prename position_type hospital]
    Provider.select(is_select).distinct.where(hospital: hospital)
  end

  def get_providers_map(hospital)
    providers_to_map(get_providers(hospital))
  end

  def providers_to_map(providers)
    providers.map { |p| ["#{p.first_name} #{p.last_name}", p.set_id] }
  end

  def execute_query_map_objresult(part_query, searchable)
    execute_query(part_query, searchable).map do |r|
      ObjResult.new(r.symbolize_keys)
    end
  end

  def query_results(total_count_part_query, results_part_query)
    @obj_total_count = execute_query_map_objresult(total_count_part_query, @searchable)
    @total_count = @obj_total_count[0].total_count
    @obj_results = execute_query_map_objresult(results_part_query, @searchable.use_paginate(true))
    @results = Kaminari.paginate_array(@obj_results, total_count: @total_count).page(@searchable.page)
  end

  def patients_report(type)
    respond_to do |format|
      format.html do
        @providers = if type == 'assessment_patients' &&  @searchable.destination == 'site'
                       providers_to_map(providers_filter_by_type(@searchable.hospital, @searchable.hospital_destination))
                     else
                       get_providers_map(@searchable.hospital)
                     end
        query_results("#{@searchable.template}/total_count", "#{@searchable.template}/count")
      end
      format.xlsx do
        @obj_results = execute_query_map_objresult("#{@searchable.template}/count", @searchable)
        template = "xlsx/patients_report/#{type}.xlsx.axlsx"
        render xlsx: "#{type}_#{@searchable.from_date}_#{@searchable.to_date}", template: template
      end
    end
  end

  def patients_report_detail(type)
    respond_to do |format|
      format.html do
        query_results("#{@searchable.template}/#{@searchable.report_type}/total_count", "#{@searchable.template}/#{@searchable.report_type}/results")
        render("reports/#{type}_details") if roles_is_admin?(@roles) || type == 'assessment_patients'
      end
      format.xlsx do
        @obj_results = execute_query_map_objresult("#{@searchable.template}/#{@searchable.report_type}/results", @searchable)
        template = "xlsx/patients_report_detail/#{type}.xlsx.axlsx"
        render xlsx: "#{type}_#{@searchable.from_date}_#{@searchable.to_date}_detail", template: template
      end
    end
  end

  def validate_token
    @token = params[:token]
    check_token(@token)
  end

  def report_params
    params.except(:controller, :action).permit!
  end

  def new_searchable
    @searchable = Searchable.new report_params, is_bma
  end
end
