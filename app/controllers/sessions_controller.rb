require 'user'
class SessionsController < ApplicationController
  def new
    @user ||= User.new
  end

  def create
    result = ProcessAuthenticateUser.call(user_params)
    if result.success?
      login(result.authentication)
      redirect_to setup_url(token: result.authentication['token'])
    else
      flash[:error] = { code: 1, message: t(:login_failed_message) }
      @user = User.new(**user_params.except(:authenticity_token, :commit).to_h.symbolize_keys)
      render :new
    end
  end

  def destroy
    logout
    redirect_to login_url
  end

  private

  def user_params
    params.permit(:username, :password, :authenticity_token, :commit)
  end
end
