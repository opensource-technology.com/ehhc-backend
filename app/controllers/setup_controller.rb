# Admin Page
class SetupController < ApplicationController
  layout 'setup'
  skip_before_action :verify_authenticity_token
  before_action :require_user_login
  before_action :session_expires

  def index
    current_user
    @hospital = MHospital.hospcode current_user.hospcode
  end

  # GET /list
  def list
    @referrals = Referral.page(1)
  end
end
