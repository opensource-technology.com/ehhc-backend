class TestController < ApplicationController
  include ReferralsHelper

  DEFAULT_REFERRAL_STATUSES = %w[P A R H F].freeze
  def index
    query, query_params = referral_query_params

    p query
    p query_params
    @referrals = Referral.referrals(OpenStruct.new(query_str: query, query_params: query_params))
    render json: { message: 'hello' }, status: :ok
  end
end
