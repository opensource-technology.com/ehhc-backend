class TreatmentOrdersController < ApplicationController
  include Api

  before_action :set_referral, only: [:index, :create, :edit, :update, :destroy]
  before_action :set_visit
  before_action :set_treatment_order, only: [:show, :edit, :update, :destroy]

  # GET /treatment_orders
  # GET /treatment_orders.json
  def index
    @treatment_orders = TreatmentOrder.all
  end

  # GET /treatment_orders/1
  # GET /treatment_orders/1.json
  def show
  end

  # GET /treatment_orders/new
  def new
    @treatment_order = TreatmentOrder.new
  end

  # GET /treatment_orders/1/edit
  def edit
  end

  # POST /treatment_orders
  # POST /treatment_orders.json
  def create
    @treatment_orders = []
    # p params
    if params.key?(:_json)
      treatment_orders_params = params.fetch(:_json, [])
      treatment_orders_params.each do |treatment_order_param|
        if treatment_order_param.key?(:set_id)
          # Update diagnose
          @treatment_order = TreatmentOrder.find_by set_id: treatment_order_param[:set_id]
          @treatment_order.update treatment_order_param.permit!
          @treatment_orders << @treatment_order
        else
          # Create new one
          @treatment_order = TreatmentOrder.new treatment_order_param.merge(visit_id: @visit.id, hospital: @hospital).permit!
          @treatment_orders << @treatment_order if @treatment_order.save
        end
      end
    end
    render json: @treatment_orders.as_json, status: :ok
  end

  # PATCH/PUT /treatment_orders/1
  # PATCH/PUT /treatment_orders/1.json
  def update
    if @treatment_order.update(treatment_order_params)
      render json: @treatment_order.as_json, status: :ok
    else
      render json: @treatment_order.errors, status: :unprocessable_entity
    end
  end

  # DELETE /treatment_orders/1
  # DELETE /treatment_orders/1.json
  def destroy
    @treatment_order.destroy
    render json: { status: "OK" }, status: :no_content
  end

  private
    def set_referral
      if params.key?(:referral_id)
        @referral = Referral.find(params[:referral_id])
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_treatment_order
      @treatment_order = TreatmentOrder.find(params[:id])
    end

    def set_visit
      if params.key?(:visit_id)
        @visit = Visit.find(params[:visit_id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def treatment_order_params
      if @visit.present?
        params.fetch(:treatment_order, {}).merge(visit_id: @visit.id).permit!
      else
        params.fetch(:treatment_order, {}).permit!
      end
    end
end
