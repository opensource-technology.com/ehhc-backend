class UsersController < ApplicationController
  before_action :set_user, only: %i[show edit update destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show; end

  # GET /users/new
  def new
    @user = User.new
    @roles = Role.all
  end

  # GET /users/1/edit
  def edit
    @roles = Role.all
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    role_param = params.fetch(:role, [])
    role = Role.find(role_param)
    @user.roles << role

    respond_to do |format|
      if @user.save
        @role = @user.roles[0]
        format.html { redirect_to users_url, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        @roles = Role.all
        @role = @user.roles[0]
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      role_param = params.fetch(:role, [])
      role = Role.find(role_param)
      @user.roles.clear
      @user.roles << role
      if @user.update(user_params)
        @role = @user.roles[0]
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        @roles = Role.all
        @role = @user.roles[0]
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.roles.clear
    @user.accesses.clear
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    _user_params = params.fetch(:user, {})
    if _user_params.fetch(:password, '').empty?
      _user_params = _user_params.except(:password)
    end
    _user_params.permit!
  end
end
