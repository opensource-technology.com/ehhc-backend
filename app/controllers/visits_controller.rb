class VisitsController < ApplicationController
  include Api

  before_action :set_referral
  before_action :set_visit, only: [:show, :edit, :update, :destroy]

  # GET /visits
  # GET /visits.json
  def index
    if @referral.present?
      @visits = @referral.visits
      ApiCallLogger.info "Get visits"
      render json: @visits.as_json, status: :ok
    else
      render json: { code: '31', message: 'Referral ID is missing.' }, status: :bad_request
    end
  end

  # GET /visits/1
  # GET /visits/1.json
  def show; end

  # GET /visits/new
  def new
    @visit = Visit.new
  end

  # GET /visits/1/edit
  def edit; end

  # POST /visits
  # POST /visits.json
  def create
    @visit = Visit.new(visit_params)
    if @visit.save
      ApiCallLogger.info "Create new visit #{@visit.id}"
      render json: @visit.as_json, status: :created
    else
      ApiCallLogger.error "Can not create new visit"
      render json: @visit.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /visits/1
  # PATCH/PUT /visits/1.json
  def update
    if @visit.update(visit_params)
      ApiCallLogger.info "Update visit #{@visit.id}"
      render json: @visit.as_json, status: :ok
    else
      ApiCallLogger.error "Can not update visit #{@visit.id}"
      render json: @visit.errors, status: :unprocessable_entity
    end
  end

  # DELETE /visits/1
  # DELETE /visits/1.json
  def destroy
    @visit.destroy
    ApiCallLogger.info "Delete visit #{@visit.id}"
    render json: { status: "OK" }, status: :no_content
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_visit
    @visit = Visit.find(params[:id])
  end

  def set_referral
    @referral = Referral.find(params[:referral_id]) if params.key?(:referral_id)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def visit_params
    params.fetch(:visit, {}).merge(referral_id: @referral.id).permit!
  end
end
