require 'ehhc/jsonwebtoken'
module ApplicationHelper
  LIMIT = 30
  @current_user

  def page_param
    params.fetch('page', '1').to_i
  end

  def get_page
    @page = params['page'].present? ? params['page'].to_i : 1
    @limit = params['limit'].present? ? params['limit'].to_i : LIMIT
    @offset = (@page - 1) * @limit
  end

  def current_page
    params['page'].present? ? params['page'].to_i : 1
  end

  def paginate_by_items(items)
    {
      current_page: items.current_page,
      total_pages: items.total_pages,
      limit: items.limit_value,
      total_count: items.total_count
    }
  end

  def render_token_not_found
    render json: {
      code: Error::TOKEN_NOT_FOUND.code,
      message: Error::TOKEN_NOT_FOUND.message
    }, status: :unauthorized
  end

  def render_token_is_expired
    render json: {
      code: Error::TOKEN_IS_EXPIRED.code,
      message: Error::TOKEN_IS_EXPIRED.message
    }, status: :unauthorized
  end

  def render_access_denied
    render json: {
      code: Error::ACCESS_DENIED.code,
      message: Error::ACCESS_DENIED.message
    }, status: :unauthorized
  end

  def check_token(token)
    payload = EHHC::JsonWebToken.get_payload token
    @access = accessor(payload, token)
    current_time = Time.now
    if !@access.present? || @access.nil?
      render_token_not_found
    elsif @access.expired_at < current_time
      render_token_is_expired
    elsif @access.is_a?(UserAccess) && @access.user.nil?
      render_token_is_expired
    end
    @user_info = OpenStruct.new(**payload['data'].symbolize_keys)
    @hospital = @access.hospcode

    puts "Current HOSPCODE #{@hospital}"
    @current_site = MHospital.hospcode @hospital
  end

  def accessor(payload, token)
    UserAccess.find_by(token: token)
  end

  def require_login
    if request.headers['X-Access-Token'].present?
      token = request.headers['X-Access-Token'].to_s
      check_token(token)
    else
      render_token_not_found
    end
  end

  def require_admin_nhso
    return if @user_info.roles.include?("admin_nhso")

    render_access_denied
  end

  def is_admin_reports
    @user_info.roles.any? { |r| ['admin_nhso', 'bma', 'admin'].include?(r) }
  end
  def is_bma
    @user_info.roles.include?("bma")
  end

  def user_info
    @user_info
  end

  def active_navigation_bar(options = {})
    action_name = options[:action] || nil
    if request.env['PATH_INFO'].include? action_name
      'btnLogout --active'
    else
      'btnLogout'
    end
  end

  def offset(page, per_page)
    (page - 1) * per_page
  end

  def running_number_index(number_index)
    number_index + 1
  end

  def basic_date_format(date)
    return "-" if date.nil?

    "#{date.strftime('%d/%m/')}#{(date.year + 543)}"
  end

  def number_index_of_page(index, page)
    per_page = Kaminari.config.default_per_page.to_i
    (per_page * (page - 1)) + index + 1
  end
end
