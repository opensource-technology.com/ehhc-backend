module AssessmentsHelper
  def find_history(referral_id, list_only)
    root_referral = find_chain_root(referral_id)
    # parent_referrals = find_chain_parent(root_referral.id) // useless
    assessments = []
    assessments.prepend(build_asessments(root_referral, list_only))
    traverse_chains(root_referral.id, assessments, list_only)
    assessments
  end

  def traverse_chains(referral_id, assessments, list_only)
    referrals = find_chain_referrals(referral_id)
    referrals.each do |referral|
      assessments.prepend(build_asessments(referral, list_only))
      traverse_chains(referral.id, assessments, list_only)
    end
  end

  # Find history methods
  def find_chain_root(referral_id)
    referral = Referral.find(referral_id)
    return referral if referral.referral_chain.nil?

    parent_referral = referral.referral_chain.parent_referral
    find_chain_root(parent_referral.id)
  end

  def find_chain_parent(root_referral_id)
    Referral.select(:id, :set_id, :hospital_source, :hospital_destination, :is_parallel, :is_continuous)
            .joins(:referral_chain)
            .where("node = true and referral_chains.parent_referral_id = :referral_id", referral_id: root_referral_id)
            .order(created_at: :asc)
  end

  def build_asessments(referral, list_only)
    {
      referral: referral.set_id,
      hospital_source: Hospital.by_code(referral.hospital_source).as_json,
      hospital_destination: Hospital.by_code(referral.hospital_destination).as_json,
      assessments: referral.assessments.order(assessment_at: :desc).as_json(list_only ? { list: true } : {}),
      is_parallel: referral.is_parallel,
      is_continuous: referral.is_continuous
    }
  end

  # pp find_chain_root(100000010477)
  def find_chain_referrals(referral_id)
    Referral.select(:id, :set_id, :hospital_source, :hospital_destination, :is_parallel, :is_continuous)
            .includes(:assessments).joins(:referral_chain)
            .where("status != 'R' and referral_chains.parent_referral_id = :referral_id", referral_id: referral_id)
            .order(created_at: :asc)
  end
end
