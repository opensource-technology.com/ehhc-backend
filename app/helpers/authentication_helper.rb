require 'base64'
require 'json'

# Authentication Helper can help user to check authentication, get payload from token
module AuthenticationHelper
  def current_authenticated_user
    @username ||= payload['data']['user_id']
  end

  def payload
    token = request.headers['X-Access-Token']
    splits = token.split('.')
    payload = JSON.parse(Base64.decode64(splits[1].encode('utf-8')))
    payload
  end
end
