module ParameterReportsHelper
  def roles_is_admin?(roles)
    roles == 'admin'
  end

  def roles_is_site?(roles)
    roles == 'site'
  end

  def parameter_reports_referral_patients_details_path(params)
    if roles_is_admin? params[:roles]
      reports_admin_referral_patients_details_path(
        from_date: revert_date(params[:searchable].from_date),
        to_date: revert_date(params[:searchable].to_date),
        date_type: params[:searchable].date_type,
        hospital: params[:searchable].hospital,
        hospital_destination: params[:searchable].hospital_destination,
        template: params[:searchable].template,
        report_type: params[:report_type],
        case_manager: params[:searchable].case_manager,
        destination: params[:searchable].destination,
        token: params[:searchable].token,
        # page: params[:searchable].page,
        format: params[:format],
        htype: params[:searchable].htype,
        hospital_select: params[:hospital_select]
      )
    else
      reports_site_referral_patients_details_path(
        from_date: revert_date(params[:searchable].from_date),
        to_date: revert_date(params[:searchable].to_date),
        date_type: params[:searchable].date_type,
        hospital: params[:searchable].hospital,
        hospital_destination: params[:searchable].hospital_destination,
        template: params[:searchable].template,
        report_type: params[:report_type],
        case_manager: params[:searchable].case_manager,
        destination: params[:searchable].destination,
        token: params[:searchable].token,
        # page: params[:searchable].page,
        format: params[:format],
        htype: params[:searchable].htype,
        hospital_select: params[:hospital_select]
      )
    end
  end

  def parameter_reports_referral_patients_path(params)
    if roles_is_admin? params[:roles]
      reports_admin_referral_patients_path(
        from_date: revert_date(params[:searchable].from_date),
        to_date: revert_date(params[:searchable].to_date),
        date_type: params[:searchable].date_type,
        hospital: params[:searchable].hospital,
        hospital_destination: params[:searchable].hospital_destination,
        template: params[:searchable].template,
        report_type: params[:report_type],
        case_manager: params[:searchable].case_manager,
        destination: params[:searchable].destination,
        token: params[:searchable].token,
        # page: params[:searchable].page,
        htype: params[:searchable].htype,
        format: params[:format]
      )
    else
      reports_site_referral_patients_path(
        from_date: revert_date(params[:searchable].from_date),
        to_date: revert_date(params[:searchable].to_date),
        date_type: params[:searchable].date_type,
        hospital: params[:searchable].hospital,
        hospital_destination: params[:searchable].hospital_destination,
        template: params[:searchable].template,
        report_type: params[:report_type],
        case_manager: params[:searchable].case_manager,
        destination: params[:searchable].destination,
        token: params[:searchable].token,
        # page: params[:searchable].page,
        htype: params[:searchable].htype,
        format: params[:format]
      )
    end
  end

  def parameter_reports_assessment_patients_details_path(params)
    if roles_is_admin? params[:roles]
      reports_admin_assessment_patients_details_path(
        from_date: revert_date(params[:searchable].from_date),
        to_date: revert_date(params[:searchable].to_date),
        date_type: params[:searchable].date_type,
        hospital: params[:searchable].hospital,
        hospital_destination: params[:searchable].hospital_destination,
        template: params[:searchable].template,
        report_type: params[:report_type],
        case_manager: params[:searchable].case_manager,
        destination: params[:searchable].destination,
        token: params[:searchable].token,
        # page: params[:searchable].page,
        format: params[:format],
        htype: params[:searchable].htype,
        hospital_select: params[:hospital_select]
      )
    else
      reports_site_assessment_patients_details_path(
        from_date: revert_date(params[:searchable].from_date),
        to_date: revert_date(params[:searchable].to_date),
        date_type: params[:searchable].date_type,
        hospital: params[:searchable].hospital,
        hospital_destination: params[:searchable].hospital_destination,
        template: params[:searchable].template,
        report_type: params[:report_type],
        case_manager: params[:searchable].case_manager,
        destination: params[:searchable].destination,
        token: params[:searchable].token,
        # page: params[:searchable].page,
        format: params[:format],
        htype: params[:searchable].htype,
        hospital_select: params[:hospital_select]
      )
    end
  end

  def parameter_reports_assessment_patients_path(params)
    if roles_is_admin? params[:roles]
      reports_admin_assessment_patients_path(
        from_date: revert_date(params[:searchable].from_date),
        to_date: revert_date(params[:searchable].to_date),
        date_type: params[:searchable].date_type,
        hospital: params[:searchable].hospital,
        hospital_destination: params[:searchable].hospital_destination,
        template: params[:searchable].template,
        case_manager: params[:searchable].case_manager,
        destination: params[:searchable].destination,
        htype: params[:searchable].htype,
        token: params[:searchable].token
      )
    else
      reports_site_assessment_patients_path(
        from_date: revert_date(params[:searchable].from_date),
        to_date: revert_date(params[:searchable].to_date),
        date_type: params[:searchable].date_type,
        hospital: params[:searchable].hospital,
        hospital_destination: params[:searchable].hospital_destination,
        template: params[:searchable].template,
        case_manager: params[:searchable].case_manager,
        destination: params[:searchable].destination,
        htype: params[:searchable].htype,
        token: params[:searchable].token
      )
    end
  end
end
