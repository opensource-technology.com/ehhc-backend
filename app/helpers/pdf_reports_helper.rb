module PdfReportsHelper
  def age(dob, today=nil)
    return "No date of birth" if dob.nil?
    today ||= Date.current

    month_diff = (12 * today.year + today.month) - (12 * dob.year + dob.month)
    y, m = month_diff.divmod 12

    y_text =      (y == 0) ? nil : (y == 1) ? '1 ปี'  : "#{y} ปี"
    m_text = y && (m == 0) ? nil : (m == 1) ? '1 เดือน' : "#{m} เดือน"
    [y_text, m_text].compact.join(' ')
  end

  def bmi(weight, height)
    return "-" if weight.nil? || height.nil?

    bmi = weight / ((height / 100)**2)
    return "%.2f" % bmi
  end

  def func_select_assessments(assessment, parallel, continuous)
    if parallel.nil? && continuous.nil?
      true
    else
      (true?(parallel) && assessment[:is_parallel]) ||
        (true?(continuous) && assessment[:is_continuous])
    end
  end

  def true?(obj)
    obj.to_s.downcase == "true"
  end
end
