module ReferralsHelper
  def patient_status(type, diff)
    if (type == '6' && diff >= 30) || (type != '6' && diff >= 7)
      3
    elsif (type == '6' && diff >= 28) || (type != '6' && diff >= 5)
      2
    elsif diff >= 0
      1
    else
      0
    end
  end

  def referral_query_params(default_statuses = DEFAULT_REFERRAL_STATUSES.dup)
    query = ''
    query_params = {}

    search_query_params(query, query_params)
    status_query_params(query, query_params, default_statuses)
    date_query_params(query, query_params)

    yield(query, query_params) if block_given?
    puts query, query_params
    [query, query_params]
  end

  def search_query_params(query, query_params)
    q = params.fetch(:q, nil)
    if q.present?
      query.concat("(patients.first_name ilike :q or patients.patient_id_list ilike :q)")
      query_params[:q] = q.concat("%")
    end
  end

  def status_query_params(query, query_params, default_statuses)
    status_param = params.fetch(:status, [])
    available_statuses = !status_param.empty? ? status_param : default_statuses
    status_query = "(#{available_statuses.map { |s| "status = :status_#{s.downcase}" }.join(' or ')})"
    status_query.prepend(' and ') if !query.empty?
    query.concat(status_query)
    available_statuses.each do |status|
      query_params["status_#{status.downcase}".to_sym] = status
    end
  end

  def date_query_params(query, query_params)
    start_date_param = params.fetch(:start_date, nil)
    end_date_param = params.fetch(:end_date, nil)

    if !start_date_param.nil? && !end_date_param.nil?
      
      date_query = query.empty? ? "referrals.created_at at time zone 'utc' at time zone 'ict' between :start_date AND :end_date" : " and referrals.updated_at at time zone 'utc' at time zone 'ict' between :start_date AND :end_date"
    elsif !start_date_param.nil?
      date_query = query.empty? ? "referrals.created_at at time zone 'utc' at time zone 'ict' >= :start_date" : " and referrals.updated_at at time zone 'utc' at time zone 'ict' >= :start_date"
    end
    if !start_date_param.nil?
      query.concat(date_query)
      query_params[:start_date] = start_date_param
    end
    query_params[:end_date] = end_date_param if !end_date_param.nil?
  end
end
