module ReportsHelper
  def admin?(array_roles)
    array_roles.include? "admin"
  end

  def revert_date(text)
    return if text.nil?

    list = text.split '-'
    "#{list[2].rjust(2, '0')}/#{list[1].rjust(2, '0')}/#{(list[0].to_i + 543)}"
  end

  def query_params(query, searchable)
    searchable.to_hash.each do |key, value|
      case key
      when :date_type
        is_data = if value == "refer"
                    "effective_at"
                  elsif value == "assessment"
                    "assessment_at"
                  else
                    "updated_at"
                  end
        query = query.gsub("{date_type}", is_data)
        # มี 2 ค่า refer = วันที่ส่งต่อผู้ป่วย, save = วันที่บันทึกข้อมูลม assessment = วันที่เยี่ยมประเมินผู้ป่วย
      when :hospital
        is_hospital = searchable.hospital_select || value
        query = query.gsub("{hospital}", is_hospital)
      when :paginate
        value && query += " OFFSET #{offset(searchable.page, Kaminari.config.default_per_page.to_i)} LIMIT #{Kaminari.config.default_per_page}"
      else
        query = query.gsub("{#{key}}", value.to_s || '')
      end
    end
    query
  end

  def translate_status(status, sent_status)
    data_return = {}
    data_return[:class_name] = '--info'
    data_return[:state] = t 'report.waiting_sent_label'
    if sent_status == 3
      data_return[:class_name] = '--danger'
      data_return[:state] = t 'report.fail_send_label'
    elsif sent_status == 2
      case status
      when 1
        data_return[:class_name] = '--warning'
        data_return[:state] = t 'report.wait_label'
      when 2
        data_return[:class_name] = '--success'
        data_return[:state] = t 'report.pass_label'
      when 3
        data_return[:class_name] = '--danger'
        data_return[:state] = t 'report.fail_label'
      end
    end
    data_return
  end

  def translate_status_description(
    sent_status,
    status_description,
    sent_errors
  )
    text_return = ''
    if sent_status == 3
      text_return =   sent_errors.to_s
    elsif sent_status == 2
      text_return = status_description
    end
    text_return
  end

  def execute_query(file_name, searchable)
    p "query #{file_name}"
    ActiveRecord::Base.connection.exec_query query_params(File.read("#{Rails.root}/lib/queries/#{file_name}.txt"), searchable)
  end

  class Searchable
    attr_accessor :page, :date_type, :from_date,
                  :to_date, :case_manager, :destination, :hospital,
                  :paginate, :template, :init, :token, :report_type,
                  :hospital_destination, :htype, :hospital_select
    # , :hospital_code
    def initialize(params = {}, is_bma)
      @date_type = check_data params[:date_type], nil
      @page = check_data_to_int params[:page], 1
      # @hospital_code = check_data params[:hospital_code], nil
      @from_date = check_data_date params[:from_date]
      @to_date = check_data_date params[:to_date]
      @case_manager = check_data params[:case_manager], nil
      @destination = check_data params[:destination], nil
      @hospital = check_data params[:hospital], nil
      @paginate = check_data params[:paginate], false
      @template = check_data params[:template], nil
      @init = check_data params[:init], nil
      @token = check_data params[:token], nil
      @report_type = check_data params[:report_type], nil
      @hospital_destination = check_data params[:hospital_destination], nil
      @htype = check_data_to_int params[:htype], is_bma ? 3 : 1
      @hospital_select = check_data params[:hospital_select], nil
    end

    def to_hash
      {
        date_type: @date_type,
        # page: @page, useless
        # hospital_code: @hospital_code,
        from_date: @from_date,
        to_date: @to_date,
        case_manager: @case_manager,
        destination: @destination,
        hospital: @hospital,
        paginate: @paginate,
        hospital_destination: @hospital_destination,
        htype: @htype,
        hospital_select: @hospital_select
      }
    end

    def use_paginate(state_paginate)
      @paginate = state_paginate
      self
    end

    def check_data_to_int(data, default_is_nil)
      data.blank? ? default_is_nil : data.to_i
    end

    def check_data(data, default_is_nil)
      data.blank? ? default_is_nil : check_data_null(data, default_is_nil)
    end

    def check_data_null(data, default_is_null)
      data == "null" ? default_is_null : data
    end

    def check_data_date(data)
      data.blank? ? Time.now.to_date.strftime('%Y-%m-%d') : convert_date(data)
    end

    # def convert_month_year(text)
    #   list = text.split '/'
    #   "#{(list[1].to_i - 543)}-#{list[0].rjust(2, '0')}"
    # end

    def convert_date(text)
      list = text.split '/'
      "#{list[2].to_i - 543}-#{list[1].rjust(2, '0')}-#{list[0].rjust(2, '0')}"
    end

    def report_type_pass?
      !@report_type.nil? && @report_type.eql?("pass")
    end
  end
end
