module SessionsHelper
  def current_user
    if !session.key?(:user_id)
      nil
    elsif session.key?(:user_id)
      OpenStruct.new(
        user_id: session[:user_id],
        username: session[:user_id],
        roles: session[:roles],
        hospcode: session[:hospcode],
        hospital_name: session[:hospcode]
      )
    end
  end

  def is_admin?
    current_user.authorities.include?("admin")
  end

  def is_logged_in?
    !current_user.nil?
  end

  def login(user)
    session[:user_id] = user[:uuid]
    session[:roles] = user[:roles]
    session[:hospcode] = user[:hospital]
    session[:expired_at] = 60.minutes.from_now
  end

  def logout
    flash.clear
    session.delete(:user_id)
    session.delete(:expired_at)
    @current_user = nil
  end

  def require_user_login
    redirect_to login_url unless is_logged_in?
  end

  def session_expires
    expired_at = session[:expired_at]

    if expired_at < Time.current
      flash[:error] = { code: 3, message: t(:session_timeout_message) }
      redirect_to login_url
    end
  end
end
