require 'interactor'
require 'net/http'
require 'securerandom'
require 'ehhc/user'
require 'ehhc/jsonwebtoken'

class AuthenticateUser
  include Interactor

  def call
    username = context.username
    password = context.password
    timestamp = Time.now.to_i * 1000
    user = authenticate(username, password)

    if user.present?
      payload = { data: { username: user.user_id, token: user.token, timestamp: timestamp, hospcode: user.hospcode, roles: user.role, uuid: username, grants: user.grants} }
      token, expired_at = EHHC::JsonWebToken.encode payload, user.token
      authentication = {
        uuid: username,
        username: username,
        token: token,
        roles: user.role,
        expired_at: expired_at,
        hospital: user.hospcode
      }
      context.authentication = authentication
      context.accessor = OpenStruct.new(type: :user, user_id: user.user_id, token: token, expired_at: expired_at, hospcode: user.hospcode)
    else
      context.fail!(error: Error::USERNAME_PASSWORD_MISMATCH)
    end
  end

  def authenticate(username, password)
    settings = Rails.configuration.nhso_authentication || {}
    uri = URI(settings[:url])
    params = { username: username, password: password, appid: 221 }

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = settings[:use_ssl]
    request = Net::HTTP::Post.new("#{uri.request_uri}/auth/login")
    request['Content-Type'] = 'application/json'
    res = http.request request, params.to_json

    body = JSON.parse(res.body)
    status = body['status']
    return nil if status == 'error'

    staff_id = body['staffid']
    staff_name = body['staffname']
    token = body['token_key']
    profile = body['profile']

    # p body
    grants = body['grant'].map { |grant| grant['permission_var'] }

    roles = body['grant'].map { |grant| parse_role(grant['permission_var']) }
    EHHC::User.new(staff_id, staff_name, token, profile, roles, grants)
  rescue OpenSSL::SSL::SSLError => e
    raise e
  rescue StandardError
    nil
  end

  def parse_role(type)
    type[7..-1].downcase
  end
end
