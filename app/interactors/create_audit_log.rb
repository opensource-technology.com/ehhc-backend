require 'interactor'

class CreateAuditLog
  include Interactor

  def call
    audit_log = AuditLog.create context.audit_log_params
    context.audit_log = audit_log if audit_log.persisted?
  end
end
