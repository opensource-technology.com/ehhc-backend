require 'interactor'

class CreateReferral
  include Interactor

  def call
    p context.referral_params
    puts "In create referral : -------------"
    referral = Referral.create context.referral_params
    if referral.persisted?
      context.referral = referral
    else
      p referral.errors
      context.fail! error: "Cannot create referral"
    end
  end

  def rollback
    context.referral.destroy
  end
end
