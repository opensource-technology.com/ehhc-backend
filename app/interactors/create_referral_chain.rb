require 'interactor'

class CreateReferralChain
  include Interactor

  def call
    p context.referral_chain_params
    puts "CHeck params"
    referral_chain = ReferralChain.create context.referral_chain_params
    if referral_chain.persisted?
      context.referral_chain = referral_chain
    else
      context.fail! error: "Cannot create referral chain"
    end
  end

  def rollback
    context.referral_chain.destroy
  end
end
