require 'interactor'

class LogAccess
  include Interactor

  def call
    accessor = context.accessor
    if accessor.type == :service
      ServiceAccess.create(service_id: accessor[:service_id], token: accessor[:token],
                           expired_at: accessor[:expired_at])
      AuditLog.create(event: 'AUTH', detail: "Service [#{accessor[:service_id]}] is logged-in", creator: 'system')
    elsif accessor.type == :user
      UserAccess.create(user: accessor[:user_id], token: accessor[:token],
                        hospcode: accessor[:hospcode],
                        expired_at: accessor[:expired_at])
      AuditLog.create(event: 'AUTH', detail: "User [#{accessor[:user_id]}] is logged-in", creator: 'system')
    else
      context.fail!(message: 'Accessor is not allowed object')
    end
  end
end
