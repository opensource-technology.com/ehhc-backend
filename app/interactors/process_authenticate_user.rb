require 'interactor'

class ProcessAuthenticateUser
  include Interactor::Organizer

  organize AuthenticateUser, LogAccess
end
