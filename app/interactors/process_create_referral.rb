require 'interactor'

class ProcessCreateReferral
  include Interactor::Organizer

  organize CreateReferral, CreateReferralChain
end
