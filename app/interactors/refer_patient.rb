require 'interactor'

class ReferPatient
  include Interactor

  def call
    referral = context.referral
    destination = context.destination

    referral.hospital_destination = destination
    referral.status = 'P'
    referral.save
    context.referral = referral
  end
end
