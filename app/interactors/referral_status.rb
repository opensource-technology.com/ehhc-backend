require 'interactor'

class ReferralStatus
  include Interactor

  STATUS_QUERY = <<~Q.freeze
    'status = ? AND (hospital_source = ? OR hospital_destination = ?)
  Q

  # Call logic functions
  def call
    puts context
    context.referrals = query_referrals
  end

  private

  def query_referrals
    Referral.where(STATUS_QUERY, context.status, context.hospcode)
            .offset(context.offset)
            .limit(context.limit)
            .order(id: context.order)
  end
end
