require 'interactor'
class ResponseReferral
  include Interactor

  STATUS_PARAMS = %i[referral status response_reason response_refer_at hospital_destination].freeze

  def call
    referral = context.referral
    if context.status.present?
      referral.status = context.status
      referral.response_reason = context.response_reason if context.response_reason.present?
      referral.response_refer_at = Time.now
      referral.hospital_destination = context.hospital_destination if context.hospital_destination.present?
    end

    reject_status_params.each do |k, v|
      referral.send("#{k}=", v)
    end

    referral.save
    context.referral = referral.reload
  end

  def reject_status_params
    context.to_h.reject { |k, _| STATUS_PARAMS.include?(k) }
  end
end
