class UpdateParentReferralStatus
  include Interactor

  def call
    referral = context.referral
    if referral.status == 'A'
      parent_referral = referral.chain_parent_referral
      return if parent_referral.nil?
      parent_referral.update(has_children_opened: true)
    elsif referral.status == 'R'
      # check another chains
      parent_referral = referral.chain_parent_referral
      return if parent_referral.nil?

      child_referrals = ReferralChain.where(parent_referral: parent_referral, is_continuous: false).map(&:referral)
      if child_referrals.all? { |r| r.status == 'F' || r.status == 'R' }
        parent_referral.update(has_children_opened: false)
      end
    end
  end
end
