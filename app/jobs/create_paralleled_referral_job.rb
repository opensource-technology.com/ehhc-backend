class CreateParalleledReferralJob < ApplicationJob
  queue_as :default

  def perform(**kwargs)
    # Do something later
    # puts kwargs[:referral_id]
    referral = Referral.find(kwargs[:referral_id])
    new_referral = Referral.find(kwargs[:new_referral_id])
    hospital = kwargs[:hospital]
    hospital_destination = kwargs[:hospital_destination]
    speciality = kwargs[:speciality]
    speciality_other = kwargs[:speciality_other]
    covisit_reason = kwargs[:covisit_reason]
    is_parallel = kwargs[:is_parallel] || false
    is_continuous = kwargs[:is_continuous] || false

    # TODO: Save in background mode
    # Clone Visits
    visits = referral.visits
    visits.each do |visit|
      new_visit = Visit.create(
        visit.attributes.except('id', 'set_id', 'created_at', 'updated_at', 'referral_id')
                        .merge(hospital: hospital, referral: new_referral)
      )

      # appointments
      visit.appointments.each do |appointment|
        Appointment.create(
          appointment.attributes.except('id', 'set_id', 'created_at', 'updated_at', 'visit_id')
                                .merge(hospital: hospital, visit: new_visit)
        )
      end

      # x_ray_results
      visit.x_ray_results.each do |x_ray_result|
        XRayResult.create(
          x_ray_result.attributes.except('id', 'set_id', 'creatd_at', 'updated_at', 'visit_id')
                                 .merge(hospital: hospital, visit: new_visit)
        )
      end

      # lab_results
      visit.lab_results.each do |lab_result|
        LabResult.create(
          lab_result.attributes.except('id', 'set_id', 'creatd_at', 'updated_at', 'visit_id')
                               .merge(hospital: hospital, visit: new_visit)
        )
      end

      # vital_signs
      visit.vital_signs.each do |vital_sign|
        VitalSign.create(
          vital_sign.attributes.except('id', 'set_id', 'creatd_at', 'updated_at', 'visit_id')
                               .merge(hospital: hospital, visit: new_visit)
        )
      end

      # procedures
      visit.procedures.each do |procedure|
        Procedure.create(
          procedure.attributes.except('id', 'set_id', 'creatd_at', 'updated_at', 'visit_id')
                              .merge(hospital: hospital, visit: new_visit)
        )
      end

      # treatment_orders
      visit.treatment_orders.each do |treatment_order|
        TreatmentOrder.create(
          treatment_order.attributes.except('id', 'set_id', 'creatd_at', 'updated_at', 'visit_id')
                                    .merge(hospital: hospital, visit: new_visit)
        )
      end

      # diagnoses
      visit.diagnoses.each do |diagnosis|
        Diagnosis.create(
          diagnosis.attributes.except('id', 'set_id', 'creatd_at', 'updated_at', 'visit_id')
                              .merge(hospital: hospital, visit: new_visit)
        )
      end
    end

    # Clone Equipments
    referral.equipments.each do |equipment|
      Equipment.create(
        equipment.attributes.except('id', 'set_id', 'created_at', 'updated_at', 'referral_id')
                            .merge(hospital: hospital, referral: new_referral)
      )
    end
    # Clone Care Plans
    referral.care_plans.each do |care_plan|
      CarePlan.create(
        care_plan.attributes.except('id', 'set_id', 'created_at', 'updated_at', 'referral_id')
                            .merge(hospital: hospital, referral: new_referral)
      )
    end

    # Clone Visit Plans
    referral.visit_plans.each do |visit_plan|
      VisitPlan.create(
        visit_plan.attributes.except('id', 'set_id', 'created_at', 'updated_at', 'referral_id')
                             .merge(hospital: hospital, referral: new_referral)
      )
    end

    # find parent in continuous
    parent_referral = referral
    if is_continuous
      parent_referral = referral.referral_chain.nil? ? referral : referral.referral_chain.parent_referral
    end

    ReferralChain.create(
      parent_referral: parent_referral,
      referral: new_referral,
      hospital: hospital,
      hospital_source: hospital,
      hospital_destination: hospital_destination,
      speciality_id: speciality,
      speciality_other: speciality_other,
      covisit_reason: covisit_reason,
      is_parallel: is_parallel,
      is_continuous: is_continuous
    )
  end
end
