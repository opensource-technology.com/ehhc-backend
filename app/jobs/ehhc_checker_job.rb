require 'ehhc'
class EHHCCheckerJob < ApplicationJob
  include EHHC::ProcessLoadData
  queue_as :default

  def perform
    puts "[CHECK] Prepare data"
    results = load_process_result
    items = results.map { |r| EHHC::Item.new(r) }
    c = EHHC::CheckCommand.new
    EHHC::Commander.call c, list: items
  end
end
