require 'ehhc'
class EHHCSendJob < ApplicationJob
  include EHHC::ProcessLoadData
  queue_as :default

  def perform
    puts "[Send] Prepare data"
    items = load_assessment.map { |i| EHHC::Item.new i }
    send_process items
  end

  def send_process(list)
    c = EHHC::SendCommand.new
    # puts "Send data to cloud"
    EHHC::Commander.call c, list: list
  end
end
