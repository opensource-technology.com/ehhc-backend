class Address < ApplicationRecord
  belongs_to :map, class_name: 'Resource', optional: true
  belongs_to :address_type, class_name: 'MAddressType', optional: true
  belongs_to :house_type, class_name: 'MHouseType', optional: true
  belongs_to :community_type, class_name: 'MCommunityType', optional: true

  ATTRS = {
    include: {
      address_type: {
        except: [ :created_at, :updated_at ]
      },
      house_type: {
        except: [ :created_at, :updted_at ]
      },
      map: {
        except: [ :created_at, :updated_at ]
      },
      community_type: {
        except: [ :created_at, :updated_at ]
      }
    },
    except: [:address_type_id, :house_type_id, :map_id, :community_type_id, :patient_id]
  }.freeze

end
