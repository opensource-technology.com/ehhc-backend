class Assessment < ApplicationRecord
  include SetId
  # validations
  # validates :adl_score, presence: false, on: :flush
  # validates :assessor, presence: true, on: :flush
  # validates :nursing_result, presence: true, on: :flush
  # validates :patient_type, presence: true, on: :flush
  # validates :appointment, presence: true, on: :flush
  # validates :care_activities, presence: true, on: :flush

  validate :quota_per_day
  validate :assessor_quota_per_day

  belongs_to :referral, touch: false
  belongs_to :health_condition, optional: true
  belongs_to :medication, optional: true
  belongs_to :mood_and_behavior_pattern, optional: true
  belongs_to :preventive_health_measure, optional: true
  belongs_to :physical_functioning, optional: true
  belongs_to :skin_condition, optional: true
  belongs_to :process_result, optional: true

  has_many :care_activities, dependent: :destroy
  has_many :multidisciplinary_teams, dependent: :destroy
  has_many :patient_assessment_equipments, dependent: :destroy
  has_many :pharmacies, dependent: :destroy

  ATTRS = {
    include: {
      preventive_health_measure: PreventiveHealthMeasure::ATTRS,
      health_condition: HealthCondition::ATTRS,
      skin_condition: SkinCondition::ATTRS,
      physical_functioning: PhysicalFunctioning::ATTRS,
      mood_and_behavior_pattern: MoodAndBehaviorPattern::ATTRS,
      medication: Medication::ATTRS
    }
  }.freeze

  REFER_ATTRS = {
    except: %i[id created_at updated_at]
  }.freeze

  LIST_ATTRS = {
    only: %i[id set_id assessment_at patient_type created_at updated_at]
  }.freeze

  def valid
    valid?
  end

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:list)
      json = super(LIST_ATTRS)
      json[:patient_type] = RPatientType.find_by(code: patient_type).as_json(RPatientType::ATTRS) if patient_type.present?
      json[:visit_terminated] = RTerminatedType.find_by(code: visit_terminated).as_json(RTerminatedType::ATTRS) if visit_terminated.present?
      json[:valid] = valid?(:flush) && check_validations
    elsif options.key?(:refer) || options.key?(:cloud)
      json = super
      json[:health_condition] = health_condition.as_json(refer: true) if health_condition.present?
      json[:medication] = medication.as_json(refer: true) if medication.present?
      json[:mood_and_behavior_pattern] = mood_and_behavior_pattern.as_json(refer: true) if mood_and_behavior_pattern.present?
      json[:preventive_health_measure] = preventive_health_measure.as_json(refer: true) if preventive_health_measure.present?
      json[:physical_functioning] = physical_functioning.as_json(refer: true) if physical_functioning.present?
      json[:skin_condition] = skin_condition.as_json(refer: true) if skin_condition.present?

      json[:care_activities] = care_activities.as_json(refer: true) if care_activities.present?
      json[:multidisciplinary_teams] = multidisciplinary_teams.as_json(refer: true) if multidisciplinary_teams.present?
      json[:patient_assessment_equipments] = patient_assessment_equipments.as_json(refer: true) if patient_assessment_equipments.present?
      json[:pharmacies] = pharmacies.as_json(refer: true) if pharmacies.present?
      json[:assessor] = Provider.find_by(set_id: assessor).as_json(refer: true) if assessor.present?
    else
      json = super
      json[:patient_type] = RPatientType.find_by(code: patient_type).as_json(RPatientType::ATTRS) if patient_type.present?
      json[:nursing_result] = RNursingResult.find_by(code: nursing_result).as_json(RNursingResult::ATTRS) if nursing_result.present?
      json[:visit_terminated] = RTerminatedType.find_by(code: visit_terminated).as_json(RTerminatedType::ATTRS) if visit_terminated.present?
      json[:health_condition] = health_condition.as_json if health_condition.present?
      json[:medication] = medication.as_json if medication.present?
      json[:mood_and_behavior_pattern] = mood_and_behavior_pattern.as_json if mood_and_behavior_pattern.present?
      json[:preventive_health_measure] = preventive_health_measure.as_json if preventive_health_measure.present?
      json[:physical_functioning] = physical_functioning.as_json if physical_functioning.present?
      json[:skin_condition] = skin_condition.as_json if skin_condition.present?
      json[:care_activities] = care_activities.as_json if care_activities.present?
      json[:multidisciplinary_teams] = multidisciplinary_teams.as_json if multidisciplinary_teams.present?
      json[:patient_assessment_equipments] = patient_assessment_equipments.as_json if patient_assessment_equipments.present?
      json[:pharmacies] = pharmacies.as_json if pharmacies.present?
      json[:assessor] = Provider.find_by(set_id: assessor).as_json(Provider::ATTRS) if assessor.present?
      json[:other_hospital_destination] = MHospital.find_by(code: other_hospital_destination).as_json if other_hospital_destination.present?
      json[:valid] = valid?(:flush) && check_validations
      # There are 3 process_status: 0 รอส่ง 1 ยังไม่ประมวลผล 2 ประมวลผลผ่าน 3 ประมวลผลไม่ผ่าน
      json[:process_status] = process_result.nil? ? 0 : process_result.status
    end
    json
  end

  # private

  def check_validations
    check_care_activities_validations && check_physical_functioning_validations
  end

  def check_care_activities_validations
    # p care_activities
    return false if !care_activities.present?

    care_activities.any? {|c| c.enabled == true }
  end

  def check_physical_functioning_validations
    return false if !physical_functioning.present?

    physical_functioning.patient_ability.present?
  end

  def quota_per_day
    count = Assessment.includes(referral: [:patient]).where("referrals.patient": referral.patient, assessment_at: range_assessment(assessment_at)).count
    if count > 1
      errors.add(:exceed_quota_per_day, "Exceed quota per day")
    end
  end

  def assessor_quota_per_day
    count = Assessment.where(assessment_at: range_assessment(assessment_at), assessor: assessor).count
    if count > 10
      errors.add(:assessor_exceed_quota_per_day, "Assessor exceed quouta per day")
    end
  end

  def range_assessment(assessment_at)
    (assessment_at.strftime('%Y-%m-%d'))..(assessment_at.next_day(1).strftime('%Y-%m-%d'))
  end
end
