class CareActivity < ApplicationRecord
  include SetId

  belongs_to :assessment, touch: true

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:refer)
      json = super
    else
      json = super
      json[:activity] = RCareActivityType.find_by(code: activity).as_json(RCareActivityType::ATTRS) if activity.present?
    end
    json
  end
end
