require 'active_support/concern'
require 'ehhc'
module SetId
  extend ActiveSupport::Concern

  included do
    attr_accessor :hospital

    before_create :init_set_id
  end

  def init_set_id
    self.set_id = EHHC::Utils.generate_set_id(hospital) unless set_id
  end
end
