require 'active_support/concern'

module UserAccessable
  extend ActiveSupport::Concern

  included do
    has_many :accesses, class_name: 'UserAccess', dependent: :destroy
  end
end
