require 'ehhc'
class Contact < ApplicationRecord
  before_create :init_params

  attr_accessor :hospital

  ALL_ATTRS = {
    # include: [:prename, :relationship]
  }.freeze

  ATTRS = {
    # include: [:prename, :relationship]
  }.freeze

  REFER_ATTRS = {
    except: %i[id created_at updated_at]
  }.freeze

  CLOUD_ATTRS = {
    except: %i[id first_name last_name created_at updated_at]
  }.freeze

  IGNORE_ATTRS = ->(k, _) { %w[created_at updated_at].include?(k) }

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:refer)
      json = super REFER_ATTRS
    elsif options.key?(:cloud)
      json = super CLOUD_ATTRS
    else
      json = super
      json[:prename] = MPrename.find_by(code: prename).as_json(MPrename::ATTRS) if prename
      json[:relationship] = RRelationship.find_by(code: relationship).as_json(RRelationship::ATTRS) if relationship
    end
    json
  end

  private

  def init_params
    self.set_id = EHHC::Utils.generate_set_id(hospital) unless set_id
  end
end
