class Diagnosis < ApplicationRecord
  include SetId

  belongs_to :visit

  ATTRS = {
    except: %i[created_at updated_at]
  }.freeze

  REFER_ATTRS = {
    except: %i[id visit_id created_at updated_at]
  }.freeze

  IGNORE_ATTRS = ->(k, _) { %w[created_at updated_at].include?(k) }

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:refer) || options.key?(:cloud)
      json = super REFER_ATTRS
    else
      json = super
      json[:disease_code] = RDiagnosis.find_by(code: disease_code).as_json(RDiagnosis::ATTRS) if disease_code
      json[:priority] = MDiagnosisPriority.find_by(code: priority).as_json(MDiagnosisPriority::ATTRS) if priority
    end
    json
  end
end
