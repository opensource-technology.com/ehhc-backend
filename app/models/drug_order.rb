class DrugOrder < ApplicationRecord
  belongs_to :referral
  belongs_to :order_type, class_name: 'ROrderType'

  ATTRS = {
    except: [:created_at, :updated_at]
  }.freeze
end
