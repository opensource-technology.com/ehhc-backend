class Equipment < ApplicationRecord
  include SetId

  self.table_name = 'equipments'
  belongs_to :referral

  ATTRS = {
  }.freeze

  REFER_ATTRS = {
    except: %i[id referral_id created_at updated_at]
  }.freeze

  IGNORE_ATTRS = ->(k, _) { %w[created_at updated_at].include?(k) }

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:refer) || options.key?(:cloud)
      json = super REFER_ATTRS
    else
      json = super
      json[:code] = REquipmentType.find_by(code: code).as_json(REquipmentType::ATTRS) if code
    end
    json
  end
end
