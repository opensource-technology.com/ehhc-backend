class HealthCondition < ApplicationRecord
  include SetId

  ATTRS = {
    include: {
    }
  }.freeze

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:refer)
      json = super
    else
      json = super
      json[:abnormality] = RAbnormality.find_by(code: abnormality).as_json(RAbnormality::ATTRS) if abnormality.present?
      json[:disability] = RYesNo.find_by(code: disability).as_json(RYesNo::ATTRS) if disability.present?
      json[:disability_characteristic] = RDisabilityCharacteristic.find_by(code: disability_characteristic).as_json(RDisabilityCharacteristic::ATTRS) if disability_characteristic.present?
      json[:weakness] = RYesNo.find_by(code: weakness).as_json(RYesNo::ATTRS) if weakness.present?
      json[:pain] = RYesNo.find_by(code: pain).as_json(RYesNo::ATTRS) if pain.present?
      json[:edema] = RYesNo.find_by(code: edema).as_json(RYesNo::ATTRS) if edema.present?
      json[:medication_adherence] = RAdherenceType.find_by(code: medication_adherence).as_json(RAdherenceType::ATTRS) if medication_adherence.present?
    end
    json
  end
end
