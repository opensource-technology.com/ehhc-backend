class Hospital < ApplicationRecord
  self.table_name = 'master.hospitals'

  validates :code, uniqueness: true

  scope :by_code, ->(code) { where(code: code).first }
end
