require 'ehhc/model_info'
class HostStatus < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
