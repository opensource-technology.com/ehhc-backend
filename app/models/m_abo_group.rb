require 'ehhc/model_info'

class MAboGroup < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = "master.abo_groups"
end
