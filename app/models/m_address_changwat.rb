require 'ehhc/model_info'

class MAddressChangwat < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = "master.address_changwats"
end
