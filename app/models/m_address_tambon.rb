require 'ehhc/model_info'

class MAddressTambon < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = "master.address_tambons"
end
