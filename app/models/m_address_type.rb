require 'ehhc/model_info'

class MAddressType < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = "master.address_types"
end
