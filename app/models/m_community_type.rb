require 'ehhc/model_info'

class MCommunityType < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = "master.community_types"
end
