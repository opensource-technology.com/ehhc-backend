require 'ehhc/model_info'

class MDiagnosisPriority < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = "master.diagnosis_priorities"
end
