require 'ehhc/model_info'

class MHospital < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
  self.table_name = "master.hospitals"

  scope :hospcode, ->(code) { find_by(code: code) }
end
