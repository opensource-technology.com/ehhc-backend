require 'ehhc/model_info'
class MHouseType < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = "master.house_types"
end
