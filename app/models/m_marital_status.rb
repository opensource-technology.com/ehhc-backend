require 'ehhc/model_info'

class MMaritalStatus < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = "master.marital_statuses"
end
