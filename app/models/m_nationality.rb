require 'ehhc/model_info'

class MNationality < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = "master.nationalities"
end
