require 'ehhc/model_info'

class MPrename < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
  self.table_name = "master.prenames"

  scope :code, ->(code) { find_by(code: code) }
end
