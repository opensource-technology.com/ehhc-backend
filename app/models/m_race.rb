require 'ehhc/model_info'
class MRace < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = "master.races"
end
