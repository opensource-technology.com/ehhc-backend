require 'ehhc/model_info'

class MReligion < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = "master.religions"
end
