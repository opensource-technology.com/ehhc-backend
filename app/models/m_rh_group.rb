require 'ehhc/model_info'

class MRhGroup < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = "master.rh_groups"
end
