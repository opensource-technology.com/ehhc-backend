require 'ehhc/model_info'
class MSex < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = "master.sex"
end
