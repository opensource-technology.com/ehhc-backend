require 'ehhc/model_info'

class MSpeciality < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = "master.specialities"
end
