class Medication < ApplicationRecord
  include SetId

  has_many :medicine_problems
  has_many :r_medicine_problems, through: :medicine_problems

  ATTRS = {
  }.freeze

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:refer)
      json = super
    else
      json = super
      json[:allergy] = RYesNo.find_by(code: allergy).as_json(RYesNo::ATTRS) if allergy.present?
      if medicine_problem.present?
        problems = medicine_problem.split ","
        medicine_problems = []
        problems.each do |problem|
          medicine_problems << RMedicineProblem.find_by(code: problem).as_json(RMedicineProblem::ATTRS)
        end
        json[:medicine_problem] = medicine_problems
      end
    end
    json
  end
end
