class MoodAndBehaviorPattern < ApplicationRecord
  include SetId

  ATTRS = {
  }.freeze

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:refer)
      json = super
    else
      json = super
      json[:mental_condition] = RYesNo.find_by(code: mental_condition).as_json(RYesNo::ATTRS) if mental_condition.present?
    end
    json
  end
end
