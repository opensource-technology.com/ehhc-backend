class MultidisciplinaryTeam < ApplicationRecord
  include SetId

  belongs_to :assessment, touch: true

  ATTRS = {
  }.freeze

  REFER_ATTRS = {
    except: [
      :id
    ]
  }.freeze

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:refer)
      json = super REFER_ATTRS
      json[:assessor] = Provider.find_by(set_id: assessor).as_json(refer: true) if assessor.present?
    else
      json = super
      json[:team] = RTeamType.find_by(code: team).as_json(RTeamType::ATTRS) if team.present?
      json[:assessor] = Provider.find_by(set_id: assessor).as_json(Provider::ATTRS) if assessor.present?
    end
    json
  end
end
