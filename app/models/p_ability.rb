class PAbility < ApplicationRecord
  self.table_name = 'p_abilities'
  validates :code, uniqueness: true

  ATTRS = {
    except: [:created_at, :updated_at]
  }.freeze
end
