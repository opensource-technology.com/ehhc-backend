class PAdlGroup < ApplicationRecord
  self.table_name = 'p_adl_groups'

  validates :code, uniqueness: true
end
