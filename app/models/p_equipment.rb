class PEquipment < ApplicationRecord
  self.table_name = 'p_equipments'

  validates :code, uniqueness: true
end
