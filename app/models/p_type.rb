class PType < ApplicationRecord
  self.table_name = 'p_types'

  validates :code, uniqueness: true
end
