require "base64"
require 'ehhc'
# Patient Class
class Patient < ApplicationRecord
  before_create :init_params

  has_many :contacts
  has_many :patient_allergies
  belongs_to :map, class_name: 'Resource', optional: true

  validates :set_id, uniqueness: true
  validates :first_name, presence: true
  validates :last_name, presence: false
  validates :patient_id, presence: false
  validates :patient_id_list, presence: false
  validates :pid, presence: false
  validates :map, presence: false

  scope :not_thai, -> { where("patient_id_list like 'ZZ%'") }
  scope :search, ->(query, hospital) { where('(first_name ilike ? or patient_id_list ilike ?) and hospital = ?', "#{query}%", "#{query}%", hospital) }

  IGNORE_ATTRS = ->(k, _) { %w[contacts map patient_allergies map_id map created_at updated_at].include?(k) }

  def init_params
    self.set_id ||= EHHC::Utils.generate_set_id(hospital)
    if nationality && nationality != '099'
      self.patient_id_list = EHHC::Utils.generate_dummy_patient_id(hospital) if patient_id_list.nil? || patient_id_list.empty?
    end
  end

  DEFAULT_ATTRS = Patient.column_names

  ATTRS = {
    include: {
      map: Resource::ATTRS,
      contacts: Contact::ATTRS
    }, except: []
  }.freeze

  API_ATTRS = {
    only: %i[id set_id first_name last_name birthdate patient_id_list phone_number_mobile current_address_road current_address_soi]
  }.freeze

  ALL_ATTRS = {
    include: {
      map: Resource::ALL_ATTRS,
      contacts: Contact::ALL_ATTRS
    }
  }.freeze

  REFER_ATTRS = {
    except: %i[id created_at updated_at]
  }.freeze

  CLOUD_ATTRS = {
    except: %i[id patient_id patient_id_list first_name last_name address_house
               address_road address_soi current_address_house current_address_village
               current_address_road current_address_soi current_address
               phone_number_home phone_number_mobile account_number abo_group rh_group
               address_house_id address_muban house_description map house_latitude house_longitude
               created_at updated_at]
  }.freeze

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:search)
      json = super only: %i[id first_name last_name patient_id_list]
      json[:prename] = MPrename.find_by(code: prename).as_json(MPrename::ATTRS) if prename.present?
    elsif options.key?(:refer)
      json = super REFER_ATTRS
      unless map_id.nil?
        begin
          map = Resource.find(map_id).as_json
          encoded_string = Base64.encode64(File.open("public/#{map['image']}", 'rb').read)
          map[:data] = encoded_string
          json[:map] = map
        rescue StandardError => error
          p error
        end
      end
      json[:contacts] = contacts.as_json(refer: true) if contacts.present?
    elsif options.key?(:cloud)
      json = super CLOUD_ATTRS
      json[:contacts] = contacts.as_json(cloud: true) if contacts.present?
    else
      json = super
      json[:prename] = MPrename.find_by(code: self.prename).as_json(MPrename::ATTRS) if self.prename.present?
      json[:sex] = MSex.find_by(code: self.sex).as_json(MSex::ATTRS) if self.sex.present?
      json[:marital_status] = MMaritalStatus.find_by(code: self.marital_status).as_json(MMaritalStatus::ATTRS) if self.marital_status.present?
      json[:religion] = MReligion.find_by(code: self.religion).as_json(MReligion::ATTRS) if self.religion.present?
      json[:race] = MRace.find_by(code: self.race).as_json(MReligion::ATTRS) if self.race.present?
      json[:nationality] = MNationality.find_by(code: self.nationality).as_json(MNationality::ATTRS) if self.nationality.present?
      json[:abo_group] = MAboGroup.find_by(code: self.abo_group).as_json(MReligion::ATTRS) if self.abo_group.present?
      json[:rh_group] = MRhGroup.find_by(code: self.rh_group).as_json(MRhGroup::ATTRS) if self.rh_group.present?
      json[:house_type] = MHouseType.find_by(code: self.house_type).as_json(MHouseType::ATTRS) if self.house_type.present?
      json[:community_type] = MCommunityType.find_by(code: self.community_type).as_json(MCommunityType::ATTRS) if self.community_type.present?
      json[:hospital] = MHospital.find_by(code: self.hospital).as_json(MHospital::ATTRS) if self.hospital.present?
      json[:hospital_primary] = MHospital.find_by(code: self.hospital_primary).as_json(MHospital::ATTRS) if self.hospital_primary.present?
      json[:hospital_destination] = MHospital.find_by(code: self.hospital_destination).as_json(MHospital::ATTRS) if self.hospital_destination.present?
      json[:hospital_usual] = MHospital.find_by(code: self.hospital_usual).as_json(MHospital::ATTRS) if self.hospital_usual.present?
      # allergies
      json[:patient_allergies] = self.patient_allergies.as_json if self.patient_allergies.present?
      # address
      json[:address_changwat] = MAddressChangwat.find_by(code: self.address_changwat).as_json(MAddressChangwat::ATTRS) if self.address_changwat.present?
      json[:address_amphur] = MAddressAmphur.find_by(code: self.address_amphur).as_json(MAddressAmphur::ATTRS) if self.address_amphur.present?
      json[:address_tambon] = MAddressTambon.find_by(code: self.address_tambon).as_json(MAddressTambon::ATTRS) if self.address_tambon.present?
      # current address
      json[:current_address_changwat] = MAddressChangwat.find_by(code: self.current_address_changwat).as_json(MAddressChangwat::ATTRS) if self.current_address_changwat.present?
      json[:current_address_amphur] = MAddressAmphur.find_by(code: self.current_address_amphur).as_json(MAddressAmphur::ATTRS) if self.current_address_amphur.present?
      json[:current_address_tambon] = MAddressTambon.find_by(code: self.current_address_tambon).as_json(MAddressTambon::ATTRS) if self.current_address_tambon.present?

      json[:contacts] = contacts.as_json
      unless map_id.nil?
        begin
          map = Resource.find(map_id).as_json
          json[:map] = map
        rescue StandardError => error
          p error
        end
      end
    end
    json
  end
end
