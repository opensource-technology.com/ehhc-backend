require 'ehhc'

class PatientAllergy < ApplicationRecord
  before_create :init_params

  attr_accessor :hospital

  belongs_to :patient, class_name: 'Patient', foreign_key: 'patient_id', primary_key: 'id'

  ATTRS = {
    except: []
  }.freeze

  IGNORE_ATTRS = ->(k, _) { %w[created_at updated_at].include?(k) }

  private

  def init_params
    self.set_id = EHHC::Utils.generate_set_id(hospital) unless set_id
  end
end
