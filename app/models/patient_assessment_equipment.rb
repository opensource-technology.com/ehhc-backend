class PatientAssessmentEquipment < ApplicationRecord
  include SetId

  belongs_to :assessment, touch: true

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:refer)
      json = super
    else
      json = super
      json[:code] = REquipmentType.find_by(code: code).as_json(REquipmentType::ATTRS) if code
    end
    json
  end
end
