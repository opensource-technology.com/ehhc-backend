class PatientProfile < ApplicationRecord
  has_one :current_address, class_name: "Address"
  has_one :address, class_name: "Address"
end
