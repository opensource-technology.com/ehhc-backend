class Pharmacy < ApplicationRecord
  include SetId

  belongs_to :assessment, touch: true

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:refer)
      json = super
    else
      json = super
      json[:drug_order] = ROrderType.find_by(code: drug_order).as_json(ROrderType::ATTRS) if drug_order.present?
    end
    json
  end
end
