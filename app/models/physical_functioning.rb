class PhysicalFunctioning < ApplicationRecord
  include SetId

  ATTRS = {
  }.freeze

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:refer)
      json = super
    else
      json = super
      json[:excretion] = RYesNo.find_by(code: excretion).as_json(RYesNo::ATTRS) if excretion.present?
      json[:patient_ability] = PAbility.find_by(code: patient_ability).as_json(PAbility::ATTRS) if patient_ability.present?
    end
    json
  end
end
