require 'ehhc'

class Procedure < ApplicationRecord
  before_create :init_params
  belongs_to :visit
  attr_accessor :hospital

  ATTRS = {
    except: []
  }.freeze

  IGNORE_ATTRS = ->(k, _) { %w[created_at updated_at].include?(k) }

  private

  def init_params
    self.set_id = EHHC::Utils.generate_set_id(hospital) unless set_id
  end
end
