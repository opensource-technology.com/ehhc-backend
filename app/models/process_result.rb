class ProcessResult < ApplicationRecord
  validates :client_code, uniqueness: true
end
