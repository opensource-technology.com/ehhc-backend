require 'ehhc'

class Provider < ApplicationRecord
  before_create :init_params

  ATTRS = {

  }.freeze

  REFER_ATTRS = {
    except: %i[id created_at updated_at]
  }.freeze

  CLOUD_ATTRS = {
    except: %i[id first_name last_name register_number provider_identifier
               phone_number_work phone_number_fax phone_number_mobile
               created_at updated_at]
  }.freeze

  IGNORE_ATTRS = ->(k, _) { %w[created_at updated_at].include?(k) }

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:refer)
      json = super REFER_ATTRS
    elsif options.key?(:cloud)
      json = super CLOUD_ATTRS
    else
      json = super
      json["prename"] = MPrename.find_by(code: prename).as_json(MPrename::ATTRS) if prename
      json["position_type"] = ProviderPosition.find_by(code: position_type).as_json(ProviderPosition::ATTRS) if position_type
      json["hospital"] = MHospital.hospcode(hospital).as_json(MHospital::ATTRS) if hospital
    end
    json
  end

  private

  def init_params
    self.set_id = EHHC::Utils.generate_set_id(hospital) unless set_id
  end

  HIDDEN_ATTRS = {
    except: %i[id first_name last_name register_number provider_identifier
               phone_number_work phone_number_fax phone_number_mobile
               created_at updated_at]
  }.freeze
end
