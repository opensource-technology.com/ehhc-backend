require 'ehhc/model_info'

class ProviderPosition < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
