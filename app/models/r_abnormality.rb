require 'ehhc/model_info'

class RAbnormality < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
