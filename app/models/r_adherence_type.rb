require 'ehhc/model_info'

class RAdherenceType < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
