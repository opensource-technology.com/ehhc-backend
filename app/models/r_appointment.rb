require 'ehhc/model_info'

class RAppointment < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
