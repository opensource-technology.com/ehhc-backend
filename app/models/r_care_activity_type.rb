require 'ehhc/model_info'

class RCareActivityType < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
