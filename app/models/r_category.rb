require 'ehhc/model_info'

class RCategory < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
