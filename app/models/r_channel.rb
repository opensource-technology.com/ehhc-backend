require 'ehhc/model_info'

class RChannel < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
