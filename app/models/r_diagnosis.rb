require 'ehhc/model_info'

class RDiagnosis < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = 'r_diagnoses'
end
