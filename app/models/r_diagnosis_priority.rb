require 'ehhc/model_info'

class RDiagnosisPriority < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
  self.table_name = 'r_diagnosis_priorities'
end
