require 'ehhc/model_info'

class RDisabilityCharacteristic < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
