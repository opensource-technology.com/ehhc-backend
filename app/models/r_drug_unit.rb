require 'ehhc/model_info'

class RDrugUnit < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
