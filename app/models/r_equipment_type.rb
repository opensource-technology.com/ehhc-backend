require 'ehhc/model_info'

class REquipmentType < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
