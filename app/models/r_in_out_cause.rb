require 'ehhc/model_info'

class RInOutCause < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
