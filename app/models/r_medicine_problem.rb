require 'ehhc/model_info'

class RMedicineProblem < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
