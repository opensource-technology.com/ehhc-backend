require 'ehhc/model_info'

class RNursingResult < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
