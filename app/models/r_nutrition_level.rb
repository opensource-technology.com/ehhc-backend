require 'ehhc/model_info'

class RNutritionLevel < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
