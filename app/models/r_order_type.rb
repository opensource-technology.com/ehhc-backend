require 'ehhc/model_info'

class ROrderType < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
