require 'ehhc/model_info'

class RPatientClass < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
