require 'ehhc/model_info'

class RPatientType < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true

  self.table_name = 'r_patient_types'
end
