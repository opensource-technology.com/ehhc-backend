require 'ehhc/model_info'

class RPriority < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
