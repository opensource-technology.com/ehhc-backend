require 'ehhc/model_info'

class RReceiveStatus < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
