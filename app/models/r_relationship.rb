require 'ehhc/model_info'

class RRelationship < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
