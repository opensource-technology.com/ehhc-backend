require 'ehhc/model_info'

class RTeamType < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
