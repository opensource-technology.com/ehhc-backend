class RType < ApplicationRecord
  validates :code, uniqueness: true
  
  ATTRS = {
    except: [:created_at, :updated_at]
  }.freeze
end
