require 'ehhc/model_info'

class RYesNo < ApplicationRecord
  include EHHC::ModelInfo
  validates :code, uniqueness: true
end
