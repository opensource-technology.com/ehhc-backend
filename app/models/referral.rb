require 'ehhc'
class Referral < ApplicationRecord
  validates :set_id, uniqueness: true
  validates :accident_indicator, presence: false
  validates :accident_detail, presence: false
  # validates :adl_score, presence: false
  validates :assessment_at, presence: false
  validates :case_manager, presence: true, length: { maximum: 25 }, on: :flush
  validates :category, presence: true, length: { is: 1 }, on: :flush
  validates :channel, presence: true, length: { is: 1 }, on: :flush
  validates :channel_other, presence: false
  validates :clinic, presence: false
  validates :effective_at, presence: false
  validates :expiration_at, presence: false
  validates :hospital_destination, presence: true, length: { is: 5 }, on: :flush
  validates :hospital_source, presence: true, length: { is: 5 }, on: :flush
  validates :in_out_family_symptom, presence: false
  validates :in_out_current_symptom, presence: false
  validates :in_out_summary_investigation, presence: false
  validates :in_out_summary_diagnosis, presence: false
  validates :in_out_summary_treatment, presence: false
  validates :in_out_case_id, presence: false
  validates :originating_referral_id, presence: false # change presence to false
  validates :patient_type, presence: true, length: { is: 1 }, on: :flush
  validates :priority, presence: true, length: { is: 1 }, on: :flush
  validates :reponse_reason, presence: false
  validates :request, presence: false
  validates :response_refer_at, presence: false
  validates :reponse_reason, presence: false
  validates :status, presence: true, length: { is: 1 }, on: :flush
  validates :assessments, presence: true, length: { minimum: 1 }, on: :flush
  validates :visits, presence: false, on: :flush

  belongs_to :patient, class_name: 'Patient'
  has_many :care_plans
  has_many :visit_plans
  has_many :assessments
  has_many :equipments
  has_many :visits
  has_one :referral_chain

  attr_accessor :hospital, :patient_status
  before_create :init_params

  scope :referrals, lambda { |query|
    joins(:patient).where(query.query_str, **query.query_params).order(created_at: :desc)
  }

  scope :parallel, lambda { |query|
    joins(:referral_chain, :patient).where(query.query_str, **query.query_params).order(created_at: :desc)
  }

  scope :continuous, lambda { |query|
    joins(:referral_chain, :patient).where(query.query_str, **query.query_params).order(created_at: :desc)
  }

  ALL_ATTRS = {
    include: {
      equipments: Equipment::ATTRS,
      patient: Patient::ALL_ATTRS,
      visits: Visit::ATTRS,
      assessments: Assessment::ATTRS
    }
  }.freeze

  API_ATTRS = {
    only: %i[
      id set_id clinic effective_at assessment_at read sent created_at updated_at response_reason
    ],
    include: {
      patient: Patient::API_ATTRS,
      visits: Visit::API_ATTRS
    }
  }.freeze

  ATTRS = {
    include: {
      equipments: Equipment::ATTRS,
      patient: Patient::ALL_ATTRS,
      visits: Visit::ATTRS,
      assessments: Assessment::ATTRS
    }
  }.freeze

  REFER_ATTRS = {
    except: [
      :id, :patient_id, :created_at, :updated_at
    ]
  }.freeze

  CLOUD_ATTRS = {
    except: %i[id patient_id created_at updated_at]
  }.freeze

  IGNORE_ATTRS = ->(k, _) { %w[patient visits equipments case_manager created_at updated_at].include?(k) }
  
  def chain_root?
    referral_chain.nil?
  end

  def chain_parent
    ReferralChain.where(parent_referral: self)
  end

  def chain_parent?
    ReferralChain.where(parent_referral: self).count > 0
  end

  def chain_child
    ReferralChain.where(referral: self)
  end

  def chain_child?
    ReferralChain.where(referral: self).count > 0
  end

  def chain_parent_referral
    return nil unless referral_chain.present?
      
    Referral.find(referral_chain.parent_referral.id)
  end

  def as_json(options = {})
    if options.key?(:ehhc)
      json = super ALL_ATTRS
    elsif options.key?(:refer)
      json = super REFER_ATTRS
      json[:patient] = patient.as_json(refer: true) if patient.present?
      json[:case_manager] = Provider.find_by(set_id: case_manager).as_json(refer: true) if case_manager.present?
      json[:equipments] = equipments.as_json(refer: true) if equipments.present?
      json[:visits] = visits.as_json(refer: true) if visits.present?
      json[:assessments] = assessments.as_json(refer: true) if assessments.present?
    elsif options.key?(:cloud)
      json = super CLOUD_ATTRS
      json[:patient] = patient.as_json(cloud: true) if patient.present?
      json[:case_manager] = Provider.find_by(set_id: case_manager).as_json(cloud: true) if case_manager.present?
      json[:equipments] = equipments.as_json(cloud: true) if equipments.present?
      json[:visits] = visits.as_json(cloud: true) if visits.present?
      json[:assessments] = assessments.as_json(cloud: true) if assessments.present?
    elsif options.key?(:api)
      json = super(API_ATTRS)
      json[:patient_status] = patient_status
      json[:category] = RCategory.find_by(code: category).as_json(RCategory::ATTRS) if category.present?
      json[:patient_type] = RPatientType.find_by(code: patient_type).as_json(RPatientType::ATTRS) if patient_type.present?
      json[:priority] = RPriority.find_by(code: priority).as_json(RPriority::ATTRS) if priority.present?
      json[:status] = RStatus.find_by(code: status).as_json(RStatus::ATTRS) if status.present?
      json[:hospital_source] = MHospital.find_by(code: hospital_source).as_json(MHospital::ATTRS) if hospital_source.present?
      json[:hospital_destination] = MHospital.find_by(code: hospital_destination).as_json(MHospital::ATTRS) if hospital_destination.present?
    else
      json = super
      json[:category] = RCategory.find_by(code: category).as_json(RCategory::ATTRS) if category.present?
      json[:channel] = RChannel.find_by(code: channel).as_json(RChannel::ATTRS) if channel.present?
      json[:patient_type] = RPatientType.find_by(code: patient_type).as_json(RPatientType::ATTRS) if patient_type.present?
      json[:priority] = RPriority.find_by(code: priority).as_json(RPriority::ATTRS) if priority.present?
      json[:status] = RStatus.find_by(code: status).as_json(RStatus::ATTRS) if status.present?
      json[:vehicle] = RVehicle.find_by(code: vehicle).as_json(RVehicle::ATTRS) if vehicle.present?
      json[:hospital_source] = MHospital.find_by(code: hospital_source).as_json(MHospital::ATTRS) if hospital_source.present?
      json[:hospital_destination] = MHospital.find_by(code: hospital_destination).as_json(MHospital::ATTRS) if hospital_destination.present?
      json[:case_manager] = Provider.find_by(set_id: case_manager).as_json(Provider::ATTRS) if case_manager.present?

      json[:equipments] = equipments.as_json if equipments.present?
      json[:visits] = visits.as_json if visits.present?
      json[:patient] = patient.as_json if patient.present?
      # Home care
      json[:assessments] = assessments.as_json if assessments.present?
    end
    json
  end

  private

  def init_params
    self.status = 'D' unless status
    self.accident_indicator = 'N' unless accident_indicator
    self.set_id = EHHC::Utils.generate_set_id(hospital) unless set_id
  end
end
