class ReferralChain < ApplicationRecord
  include SetId
  belongs_to :referral
  belongs_to :parent_referral, class_name: 'Referral'
end
