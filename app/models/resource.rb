require 'securerandom'
require 'fileutils'
require 'base64'

class Resource < ApplicationRecord
  attribute :actived, :boolean, default: true
  attr_accessor :data
  ROOT_PATH = "public"
  MEDIA_PATH = "media"

  def self.save(resource)
    image = resource[:image]
    puts image
    file = image.read
    filename = image.original_filename
    ext = File.extname(filename)  # get extension
    filename = SecureRandom.hex # random hex string
    now = Time.now()
    today_path = now.strftime("%Y%m%d")

    instance = create name: resource[:name], image: "#{MEDIA_PATH}/#{today_path}/#{filename}#{ext}"
    resource_path = "#{ROOT_PATH}/#{MEDIA_PATH}/#{today_path}/"
    FileUtils.mkdir_p resource_path unless File.exists? resource_path
    File.open(resource_path + filename + "#{ext}", "wb") do |f|
      f.write(file)
    end

    instance
  end

  def self.create_resource(resource)
    raw_image = Base64.decode64(resource['data'])
    ext = File.extname(resource['image'])
    filename = SecureRandom.hex
    now = Time.now
    today_path = now.strftime "%Y%m%d"
    file_path = "/media/#{today_path}/#{filename}#{ext}"
    resource_path = "public/media/#{today_path}"
    FileUtils.mkdir_p resource_path unless File.exist? resource_path
    File.open("public/#{file_path}", 'wb') do |file|
      file.write(raw_image)
    end
    Resource.create name: resource['name'], image: file_path
  end

  ATTRS = {
    except: [:created_at, :updated_at]
  }.freeze

  ALL_ATTRS = {
    methods: [:data],
    except: [:created_at, :updated_at]
  }.freeze
end
