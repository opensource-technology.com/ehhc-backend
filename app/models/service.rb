require 'securerandom'

class Service < ApplicationRecord
  before_create :init_params

  private

  def init_params
    self.id = "#{SecureRandom.hex(25)}_#{SecureRandom.hex(10)}"
    self.key = SecureRandom.uuid
    self.secret = SecureRandom.uuid
  end
end
