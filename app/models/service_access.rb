class ServiceAccess < ApplicationRecord
  belongs_to :service
  attribute :actived, :boolean, default: -> { true }
end
