class SkinCondition < ApplicationRecord
  include SetId

  ATTRS = {
  }.freeze

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:refer)
      json = super
    else
      json = super
      json[:bedsore] = RYesNo.find_by(code: bedsore).as_json(RYesNo::ATTRS) if bedsore.present?
    end
    json
  end
end
