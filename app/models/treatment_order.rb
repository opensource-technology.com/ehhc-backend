class TreatmentOrder < ApplicationRecord
  include SetId
  belongs_to :visit

  ATTRS = {
    # except: []
  }.freeze

  REFER_ATTRS = {
    except: %i[id visit_id created_at updated_at]
  }.freeze

  IGNORE_ATTRS = ->(k, _) { %w[created_at updated_at].include?(k) }
end
