require 'ehhc'

class Visit < ApplicationRecord
  before_create :init_params
  belongs_to :referral, touch: true
  has_many :appointments, dependent: :destroy
  has_many :x_ray_results, dependent: :destroy
  has_many :lab_results, dependent: :destroy
  has_many :vital_signs, dependent: :destroy
  has_many :procedures, dependent: :destroy
  has_many :treatment_orders, dependent: :destroy
  has_many :diagnoses, dependent: :destroy

  attr_accessor :hospital

  ATTRS = {
    include: {
      appointments: Appointment::ATTRS,
      diagnoses: Diagnosis::ATTRS,
      vital_signs: VitalSign::ATTRS,
      x_ray_results: XRayResult::ATTRS,
      lab_results: LabResult::ATTRS,
      procedures: Procedure::ATTRS,
      treatment_orders: TreatmentOrder::ATTRS
    }
  }.freeze

  API_ATTRS = {
    only: %i[id set_id diagnosis]
  }.freeze

  REFER_ATTRS = {
    except: %i[id referral_id created_at updated_at]
  }.freeze

  CLOUD_ATTRS = {
    except: %i[id referral_id admit_at discharge_at patient_account_number patient_account_name created_at updated_at]
  }.freeze

  IGNORE_ATTRS = lambda do |k, _|
    %w[appointments diagnoses lab_results patient_allergies procedures treatment_orders vital_signs x_ray_results created_at updated_at].include?(k)
  end

  def as_json(options = {})
    if options.key?(:ehhc)
      json = { ID: :id, Sex: sex, Prename: prename, House_Type: house_type }
    elsif options.key?(:cloud)
      json = super CLOUD_ATTRS
    else
      options = options.key?(:refer) ? options.merge(REFER_ATTRS) : options
      json = super options
      json[:attending_doctor] = Provider.find_by(set_id: attending_doctor).as_json(options) if attending_doctor
      json[:referring_doctor] = Provider.find_by(set_id: referring_doctor).as_json(options) if referring_doctor
      json[:consulting_doctor] = Provider.find_by(set_id: consulting_doctor).as_json(options) if consulting_doctor
      json[:appointments] = appointments.as_json(options) if appointments.present?
      json[:vital_signs] = vital_signs.as_json(options) if vital_signs.present?
      json[:x_ray_results] = x_ray_results.as_json(options) if x_ray_results.present?
      json[:lab_results] = lab_results.as_json(options) if lab_results.present?
      json[:procedures] = procedures.as_json(options) if procedures.present?
      json[:diagnoses] = diagnoses.as_json(options) if diagnoses.present?
      json[:treatment_orders] = treatment_orders.as_json(options) if treatment_orders.present?
    end
    json
  end

  private

  def init_params
    self.set_id = EHHC::Utils.generate_set_id(hospital) unless set_id
  end
end
