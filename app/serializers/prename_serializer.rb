class PrenameSerializer < Blueprinter::Base
  identifier :code

  fields :code, :label
end
