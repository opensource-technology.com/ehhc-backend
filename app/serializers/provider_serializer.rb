class ProviderSerializer < Blueprinter::Base
  fields :id, :set_id, :first_name, :last_name, :communication, :register_number, :provider_identifier, :position,
         :department, :effective_start_at, :effective_end_at, :phone_number_work, :phone_number_fax,
         :phone_number_mobile, :created_at, :updated_at

  # API
  view :api do
    association :prename, blueprint: PrenameSerializer do |provider, _|
      provider.prename ? MPrename.code(provider.prename) : nil
    end

    association :position_type, blueprint: ProviderPositionTypeSerializer do |provider, _|
      provider.position_type ? ProviderPosition.code(prodiver.position_type) : nil
    end

    association :hospital, blueprint: HospitalSerializer do |provider, _|
      provider.hospital ? MHospital.hospcode(provider.hospital) : nil
    end
  end

  view :cloud do
  end

  view :refer do
    excludes :id, :created_at, :updated_at
  end
end
