class ReferralSerializer < Blueprinter::Base
  fields :set_id

  view :normal do
    fields :effective_at, :originating_referral_id
  end

  view :cloud do
    fields :appoint_at
  end
end
