json.extract! drug_order, :id, :created_at, :updated_at
json.url drug_order_url(drug_order, format: :json)
