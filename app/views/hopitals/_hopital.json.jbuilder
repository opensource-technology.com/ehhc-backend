json.extract! hopital, :id, :created_at, :updated_at
json.url hopital_url(hopital, format: :json)
