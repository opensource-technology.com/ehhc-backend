json.extract! multidisciplinary_team, :id, :created_at, :updated_at
json.url multidisciplinary_team_url(multidisciplinary_team, format: :json)
