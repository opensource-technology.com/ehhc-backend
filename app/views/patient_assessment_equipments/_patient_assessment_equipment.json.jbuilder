json.extract! patient_assessment_equipment, :id, :created_at, :updated_at
json.url patient_assessment_equipment_url(patient_assessment_equipment, format: :json)
