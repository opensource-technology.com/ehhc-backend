json.extract! pharmarcy, :id, :created_at, :updated_at
json.url pharmarcy_url(pharmarcy, format: :json)
