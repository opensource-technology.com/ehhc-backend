json.extract! treatment_order, :id, :created_at, :updated_at
json.url treatment_order_url(treatment_order, format: :json)
