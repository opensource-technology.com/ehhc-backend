require_relative 'boot'

require 'rails/all'
require 'rack/cors'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module EHHC
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0
    config.encoding = "utf-8"
    config.time_zone = 'Bangkok'
    config.i18n.default_locale = :th
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', headers: :any, methods: %i[get post put delete patch options]
      end
    end
    config.eager_load_paths << Rails.root.join('app', 'serializers')
    config.web_console.development_only = false
    config.camelize_wsdl = true
    config.redis = {
      host: ENV.fetch('REDIS_HOST', 'localhost'),
      port: ENV.fetch('REDIS_PORT', 6379)
    }
    Dir[Rails.root.join("config/routes/**/*.rb")].sort.each { |route_file| config.paths["config/routes.rb"] << route_file }

    config.app = {
      version: "2.1.0"
    }

    config.nhso_authentication = {
      url: 'https://bkkapp.nhso.go.th/bkkapp/api',
      use_ssl: true,
      service_username: 'bkk1525165',
      service_password: 'OWrHNBQP'
    }
  end
end
