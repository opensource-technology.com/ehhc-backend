# Error codes and messages
# Code      Message
# 1         Unknown
# 10        Parameters are not correct
# 11        User/Service doesn't exist
# 12        Username and password missmatch/Service ID and signature missmathc
# 13        Access denied
# 14        Access Timeout
# 21        Patient doesn't exist
module Error
  # Handle error code and message
  class ActionError < StandardError
    attr_reader :code, :message

    def initialize(code, message)
      super(message)
      @code = code
      @message = message
    end

    def to_s
      "[#{@code}]: #{super}"
    end
  end

  UNKNOWN = ActionError.new(1, :UNKNOWN_ERROR)
  MISSING_PARAMETER = ActionError.new(2, :MISSING_PARAMETER_ERROR)
  USER_DOES_NOT_EXIST = ActionError.new(11, :USER_DOES_NOT_EXIST_ERROR)
  USERNAME_PASSWORD_MISMATCH = ActionError.new(12, :USERNAME_PASSWORD_MISMATCH_ERROR)
  SERVICE_DOES_NOT_EXIST = ActionError.new(11, :SERVICE_DOES_NOT_EXIST_ERROR)
  SERVICE_SIGNATURE_MISMATCH = ActionError.new(12, :SERVICE_SIGNATURE_MISMATCH_ERROR)
  ACCESS_DENIED = ActionError.new(13, :ACCESS_DENIED_ERROR)
  TOKEN_IS_EXPIRED = ActionError.new(14, :TOKEN_IS_EXPIRED_ERROR)
  TOKEN_NOT_FOUND = ActionError.new(15, :TOKEN_NOT_FOUND_ERROR)
  PATIENT_NOT_EXIST = ActionError.new(21, :PATIENT_DOES_NOT_EXIST_ERROR)
  PATIENT_ALREADY_EXIST = ActionError.new(21, :PATIENT_ALREADY_EXIST_ERROR)
  REFERRAL_DESTINATION_IS_REQUIRED = ActionError.new(31, :REFERRAL_DESTINATION_IS_REQUIRED_ERROR)
  REFERRAL_IS_NOT_COMPLETE = ActionError.new(32, :REFERRAL_IS_NOT_COMPLETE_ERROR)
  REFERRAL_STATUS_IS_REQUIRED = ActionError.new(33, :REFERRAL_STATUS_IS_REQUIRED_ERROR)
  REFERRAL_INVALID_JSON_FORMAT = ActionError.new(34, :REFERRAL_INVALID_JSON_FORMAT_ERROR)
  REDIS_CONNECTION_TIMEOUT = ActionError.new(51, :REDIS_CONNECTION_TIMEOUT_ERROR)
end

DEFAULT_REFERRAL_STATUSES = %w[P A R H F].freeze
