require 'rufus-scheduler'

scheduler = Rufus::Scheduler.new

scheduler.cron '0 */6 * * *' do
  puts "[EHHCSendJob] #{Time.now}"
  EHHCSendJob.perform_now
end

scheduler.cron '*/30 00-06 * * *' do
  puts "[TEST] #{Time.now}"
  EHHCCheckerJob.perform_now
end
