Rails.application.routes.draw do
  root 'setup#index'

  scope "/admin" do
    get '', to: 'setup#index', as: :setup
    get '/login', to: 'sessions#new'
    post '/login', to: 'sessions#create'
    delete 'logout', to: 'sessions#destroy'
    resources :sites
    resources :roles
    resources :users
    resources :employees
    resources :services
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # get 'demo', controller: :report, action: :demo
  get 'report/referral/:id', controller: :pdf_reports, action: :refer
  get 'report/hhc/:id', controller: :pdf_reports, action: :hhc
  get 'report/assessments/history/:id', controller: :pdf_reports, action: :assessments_history
  get 'assessment', controller: :pdf_reports, action: :assessment
  # post 'test', controller: :test, action: :refer
  # get 'health', controller: :application, action: :health
  get 'about', controller: :application, action: :about
  get 'test', controller: :test, action: :index, as: :test

  #  vvvvvv reports routes vvvvvv
  get 'providers/filter', controller: :reports, action: :providers_filter
  get 'hospital/filter', controller: :reports, action: :hospital_filter_by_type
  get 'reports', controller: :reports, action: :index
  get 'reports/site/referral_patients', controller: :reports, action: :site_referral_patients
  get 'reports/site/referral_patients/details', controller: :reports, action: :site_referral_patients_details
  get 'reports/admin/referral_patients', controller: :reports, action: :admin_referral_patients
  get 'reports/admin/referral_patients/details', controller: :reports, action: :admin_referral_patients_details
  get 'reports/site/assessment_patients', controller: :reports, action: :site_assessment_patients
  get 'reports/site/assessment_patients/details', controller: :reports, action: :site_assessment_patients_details
  get 'reports/admin/assessment_patients', controller: :reports, action: :admin_assessment_patients
  get 'reports/admin/assessment_patients/details', controller: :reports, action: :admin_assessment_patients_details
  #  ^^^^^^ reports routes ^^^^^^
end
