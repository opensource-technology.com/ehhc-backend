Rails.application.routes.draw do
  scope :api do
    post 'auth/login', controller: 'authentication', action: 'login'
    post 'auth/logout', controller: 'authentication', action: 'logout'
    post 'auth/logout', controller: 'authentication', action: 'logout'
    # post 'auth/service_login', controller: 'authentication', action: 'login'
    # post 'auth/user_login', controller: 'authentication', action: 'login'
    get 'patients/search', controller: :patients, action: :search
    get 'patients/index_next', controller: 'patients', action: 'index_next'

    resources :patients do
      resources :contacts
      resources :addresses
    end
    get 'referrals/queue', controller: 'referrals', action: 'queue'
    get 'referrals/inbound', controller: 'referrals', action: 'inbound'
    get 'referrals/outbound', controller: 'referrals', action: 'outbound'
    get 'referrals/waiting', controller: 'referrals', action: 'waiting'
    get 'referrals/onsite', controller: 'referrals', action: 'onsite'
    get 'referrals/parallel_inbound', controller: 'referrals', action: 'parallel_inbound'
    get 'referrals/parallel_outbound', controller: 'referrals', action: 'parallel_outbound'
    get 'referrals/continuous_inbound', controller: 'referrals', action: 'continuous_inbound'
    get 'referrals/continuous_outbound', controller: 'referrals', action: 'continuous_outbound'
    get 'referrals/info', controller: 'referrals', action: 'info'
    post 'referrals/refer', controller: 'referrals', action: 'external_refer'
    post 'referrals/:id/accept', controller: 'referrals', action: 'accept'
    post 'referrals/:id/refer', controller: 'referrals', action: 'refer'
    post 'referrals/:id/flush', controller: 'referrals', action: 'flush'
    put 'referrals/:id/status', controller: 'referrals', action: 'update_status'
    get 'referrals/test', controller: 'referrals', action: :test
    resources :referrals do
      resources :equipments
      resources :visits do
        post '/diagnoses/update_disease_text', controller: :diagnoses, action: :update_disease_text
        resources :diagnoses
        resources :treatment_orders
      end
      get 'assessments/parallel', controller: :assessments, action: :parallel
      get 'assessments/continuous', controller: :assessments, action: :continuous
      get 'assessments/history', controller: :assessments, action: :history
      resources :assessments
    end
    post 'referrals/:id/paralleled', controller: 'referrals', action: 'new_paralleled_referral'

    # Get asessments
    get 'assessments', controller: 'assessments', action: 'list'
    get 'assessments/:id', controller: 'assessments', action: 'by_id'

    # post 'referrals/:id/continuous', controller: 'referrals', action: 'new_continuous_referral'
    get 'hospitals/search', controller: 'hospitals', action: 'search'
    resources :hospitals
    resources :contacts
    resources :resources
    get 'providers/search', controller: 'providers', action: 'search'
    resources :providers

    get 'diagnoses/search', controller: 'diagnoses', action: 'search'
    get 'addresses/changwats', controller: 'addresses', action: 'changwats'
    get 'addresses/amphurs', controller: 'addresses', action: 'amphurs'
    get 'addresses/tambons', controller: 'addresses', action: 'tambons'

    delete 'pharmacies/:id', controller: 'pharmacies', action: 'destroy'
    delete 'patient_assessment_equipments/:id', controller: 'patient_assessment_equipments', action: 'destroy'
    delete 'multidisciplinary_teams/:id', controller: 'multidisciplinary_teams', action: 'destroy'
    get 'families/:id', controller: 'families', action: 'list'
  end
end
