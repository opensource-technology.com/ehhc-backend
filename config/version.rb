module App
  module Version
    MAJOR = 2
    MINOR = 1
    PATCH = 0
    BUILD = a6e0bc8

    STRING = [MAJOR, MINOR, PATCH].compact.join('.')
  end
end
