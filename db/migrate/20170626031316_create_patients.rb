class CreatePatients < ActiveRecord::Migration[5.1]
  def change
    create_table :patients do |t|
      t.string :firstname
      t.string :lastname
      t.string :hn
      t.string :vn
      t.date :birthdate

      t.timestamps
    end
  end
end
