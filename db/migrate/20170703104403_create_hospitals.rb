class CreateHospitals < ActiveRecord::Migration[5.1]
  def change
    create_table :hospitals do |t|
      t.string :code
      t.string :name

      t.timestamps
    end

    create_table :hospitals_users, id: false do |t|
      t.belongs_to :hospital, index: true
      t.belongs_to :user, index: true
    end
  end
end
