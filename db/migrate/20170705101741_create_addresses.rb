class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.string :street
      t.string :district
      t.string :region
      t.string :province
      t.string :postcode
      t.float :latitude
      t.float :longitude
      t.belongs_to :patient_profile, index: true
      t.timestamps
    end

  end
end
