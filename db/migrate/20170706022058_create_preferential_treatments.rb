class CreatePreferentialTreatments < ActiveRecord::Migration[5.1]
  def change
    create_table :preferential_treatments do |t|
      t.string :code
      t.belongs_to :patient

      t.timestamps
    end
  end
end
