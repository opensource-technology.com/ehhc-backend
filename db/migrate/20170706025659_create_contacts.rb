class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.string :firstname
      t.string :lastname
      t.string :telephone_number
      t.string :phone_number
      t.integer :relationship
      t.string :other_relationship
      t.references :patient, index: true

      t.timestamps
    end

    remove_column :patients, :vn, :string
  end
end
