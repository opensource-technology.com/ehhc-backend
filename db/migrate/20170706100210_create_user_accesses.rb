class CreateUserAccesses < ActiveRecord::Migration[5.1]
  def change
    create_table :user_accesses do |t|
      #t.references :user, foreign_key: true
      t.string :token
      t.datetime :expired_at
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
