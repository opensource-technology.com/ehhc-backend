class UpdateInfoToPatients < ActiveRecord::Migration[5.1]
  def change
    add_column :patients, :pid, :string
    add_column :patients, :cid, :string, limit: 30
    add_column :patients, :passport_number, :string, null:true
    add_column :patients, :passport_country_issues, :string, null: true
    add_column :patients, :passport_expired_at, :date, null: true
    add_column :patients, :pre_name, :string, limit: 3
    add_column :patients, :sex, :integer
    add_column :patients, :marital_status, :integer
    add_column :patients, :religion, :string
    add_column :patients, :race, :string
    add_column :patients, :nationality, :string
    add_column :patients, :abo_group, :integer
    add_column :patients, :rh_group, :integer
    add_column :patients, :account_name, :string, null: true
    add_column :patients, :account_number, :string, null: true

    rename_column :patients, :firstname, :first_name
    rename_column :patients, :lastname, :last_name
  end
end
