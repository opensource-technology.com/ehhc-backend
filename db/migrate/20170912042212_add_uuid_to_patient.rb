require "securerandom"

class AddUuidToPatient < ActiveRecord::Migration[5.1]
  def change
    add_column :patients, :uuid, :string, null:false, default: SecureRandom.uuid
  end
end
