class RenameAttributesInContact < ActiveRecord::Migration[5.1]
  def change
    rename_column :contacts, :firstname, :first_name
    rename_column :contacts, :lastname, :last_name
    rename_column :contacts, :telephone_number, :mobile_phone_number
  end
end
