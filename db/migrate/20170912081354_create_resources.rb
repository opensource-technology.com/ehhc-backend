class CreateResources < ActiveRecord::Migration[5.1]
  def change
    create_table :resources do |t|
      t.string :name
      t.string :image
      t.boolean :actived

      t.timestamps
    end
  end
end
