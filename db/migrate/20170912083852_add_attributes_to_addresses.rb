class AddAttributesToAddresses < ActiveRecord::Migration[5.1]
  def change
    add_column :addresses, :address_type, :integer
    add_column :addresses, :house_id, :string
    add_column :addresses, :house, :string
    add_column :addresses, :village, :string
    remove_column :addresses, :street, :string
    rename_column :addresses, :district, :soi
    remove_column :addresses, :region, :string
    add_column :addresses, :tambon, :integer
    remove_column :addresses, :province, :string
    add_column :addresses, :amphur, :integer
    add_column :addresses, :changwat, :integer
    add_column :addresses, :phone_number_home, :string
    add_column :addresses, :phone_number_business, :string
    add_column :addresses, :house_type, :integer
    add_reference :addresses, :map, foreign_key: { to_table: :resources }, index: true
    remove_column :addresses, :patient_profile_id, :integer, index: true
  end
end
