class AddReferenceToAddresses < ActiveRecord::Migration[5.1]
  def change
    add_reference :addresses, :patient, foreign_key: true
  end
end
