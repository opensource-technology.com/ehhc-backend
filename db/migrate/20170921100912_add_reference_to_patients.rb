class AddReferenceToPatients < ActiveRecord::Migration[5.1]
  def change
    remove_column :patients, :sex, :integer

    # execute 'CREATE SCHEMA master'
    # create_table 'master.sex' do |t|
    #   t.string :code
    #   t.string :label
    #   # t.timestamps
    # end

    add_reference :patients, :sex, foreign_key: {to_table: 'master.sex'}, index: true
  end
end
