class AddReferencesToPatients < ActiveRecord::Migration[5.1]
  def change
    remove_column :patients, :pre_name, :string, limit: 3
    # create_table 'master.prenames' do |t|
    #   t.string :code
    #   t.string :label
    #   # t.timestamps
    # end
    add_reference :patients, :prename, foreign_key: { to_table: 'master.prenames' }, index: true

    remove_column :patients, :marital_status, :integer
    # create_table 'master.marital_statuses' do |t|
    #   t.string :code
    #   t.string :label
    #   # t.timestamps
    # end
    add_reference :patients, :martial_status, foreign_key: { to_table: 'master.marital_statuses' }, index: true

    remove_column :patients, :religion, :string
    # create_table 'master.religions' do |t|
    #   t.string :code
    #   t.string :label
    #   # t.timestamps
    # end
    add_reference :patients, :religion, foreign_key: { to_table: 'master.religions' }, index: true

    remove_column :patients, :race, :string
    # create_table 'master.races' do |t|
    #   t.string :code
    #   t.string :label
    #   # t.timestamps
    # end
    add_reference :patients, :race, foreign_key: { to_table: 'master.races' }, index: true

    remove_column :patients, :nationality, :string
    # create_table 'master.nationalities' do |t|
    #   t.string :code
    #   t.string :label
    #   # t.timestamps
    # end
    add_reference :patients, :nationality, foreign_key: { to_table: 'master.nationalities' }, index: true

    remove_column :patients, :abo_group, :integer
    # create_table 'master.abo_groups' do |t|
    #   t.string :code
    #   t.string :label
    #   # t.timestamps
    # end
    add_reference :patients, :abo_group, foreign_key: { to_table: 'master.abo_groups' }, index: true

    remove_column :patients, :rh_group, :integer
    # create_table 'master.rh_groups' do |t|
    #   t.string :code
    #   t.string :label
    #   # t.timestamps
    # end
    add_reference :patients, :rh_group, foreign_key: { to_table: 'master.rh_groups' }, index: true
  end
end
