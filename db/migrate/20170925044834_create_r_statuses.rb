class CreateRStatuses < ActiveRecord::Migration[5.1]
  def change
    # create_table :r_statuses do |t|
    #   t.string :code
    #   t.string :label
    #   t.timestamps
    # end

    # create_table :r_in_out_causes do |t|
    #   t.string :code
    #   t.string :label
    #   t.timestamps
    # end

    # create_table :r_patient_types do |t|
    #   t.string :code
    #   t.string :label
    #   t.timestamps
    # end

    create_table :m_vehicles do |t|
      t.string :code
      t.string :label
      t.timestamps
    end

    create_table :d_priorities do |t|
      t.string :code
      t.string :label
      t.timestamps
    end
  end
end
