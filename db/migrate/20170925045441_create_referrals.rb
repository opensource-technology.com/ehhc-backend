class CreateReferrals < ActiveRecord::Migration[5.1]
  def change
    create_table :referrals do |t|
      t.references :status, class_name: 'RStatus', index: true, foreign_key: { to_table: 'r_statuses' }
      t.references :priority, class_name: 'RPriority', index: true, foreign_key: { to_table: 'r_priorities' }
      t.references :category, class_name: 'RCategory', index: true, foreign_key: { to_table: 'r_categories' }
      t.string :clinic
      t.string :originating_referral_id
      t.datetime :effective_at
      t.datetime :appointment_at
      t.datetime :expiration_at
      t.string :other_reason
      t.string :in_out_family_symptom
      t.string :in_out_current_symptom
      t.string :in_out_summary_investigation
      t.string :in_out_summary_diagnosis
      t.string :in_out_summary_treatment
      t.references :in_out_case, class_name: 'RInOutCause', foreign_key: { to_table: 'r_in_out_causes' }
      t.string :request
      t.string :accident_indicator
      t.string :accident_detail
      t.string :in_out_other_detail
      t.references :hospital_source, class_name: 'Hospital', foreign_key: { to_table: 'hospitals' }
      t.references :hospital_destination, class_name: 'Hospital', foreign_key: { to_table: 'hospitals' }
      t.string :reponse_reason
      t.datetime :response_refer_at
      t.references :channel, class_name: 'RChannel', foreign_key: { to_table: 'r_channels' }
      t.string :channel_other
      t.references :patient_type, class_name: 'PType', foreign_key: { to_table: 'p_types' }
      t.datetime :assessment_at
      t.integer :adl_score
      t.integer :nutrition_level
      t.string :patient_condition
      t.string :patient_activiety
      t.references :patient, foreign_key: true
      t.timestamps
    end
  end
end
