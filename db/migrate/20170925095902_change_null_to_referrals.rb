class ChangeNullToReferrals < ActiveRecord::Migration[5.1]
  def change
    change_column_null :referrals, :clinic, true
    change_column_null :referrals, :originating_referral_id, true
    change_column_null :referrals, :effective_at, true
    change_column_null :referrals, :expiration_at, true
    change_column_null :referrals, :in_out_case_id, true
    change_column_null :referrals, :request, true
    change_column_null :referrals, :accident_indicator, true
    change_column_null :referrals, :accident_detail, true
    change_column_null :referrals, :in_out_family_symptom, true
    change_column_null :referrals, :in_out_current_symptom, true
    change_column_null :referrals, :in_out_summary_investigation, true
    change_column_null :referrals, :in_out_summary_diagnosis, true
    change_column_null :referrals, :in_out_summary_treatment, true
    change_column_null :referrals, :in_out_case_id, true
    change_column_null :referrals, :hospital_source_id, true
    change_column_null :referrals, :hospital_destination_id, true
    change_column_null :referrals, :reponse_reason, true
    change_column_null :referrals, :response_refer_at, true
    change_column_null :referrals, :reponse_reason, true
    change_column_null :referrals, :channel_id, true
    change_column_null :referrals, :channel_other, true
    change_column_null :referrals, :patient_type_id, true
    change_column_null :referrals, :assessment_at, true
    change_column_null :referrals, :adl_score, true
    change_column_null :referrals, :nutrition_level, true
    change_column_null :referrals, :patient_condition, true
    change_column_null :referrals, :patient_activiety, true
  end
end
