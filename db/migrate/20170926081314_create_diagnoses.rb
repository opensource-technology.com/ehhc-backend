class CreateDiagnoses < ActiveRecord::Migration[5.1]
  def change
    # create_table :r_diagnosis_priorities do |t|
    #   t.string :code
    #   t.string :label
    #   t.timestamps
    # end

    create_table :diagnoses do |t|
      t.references :referral, foreign_key: true
      t.string :disease_text
      t.references :disease_code, class_name: 'RDiagnosis', foreign_key: { to_table: 'r_diagnoses'}
      t.datetime :diagnosis_at
      t.references :priority, class_name: 'RDiagnosisPriority', foreign_key: { to_table: 'r_diagnosis_priorities' }
      t.boolean :confidential_indicator, default: false
      t.timestamps
    end
  end
end
