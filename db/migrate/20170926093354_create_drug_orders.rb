class CreateDrugOrders < ActiveRecord::Migration[5.1]
  def change
    # create_table :r_order_types do |t|
    #   t.string :code
    #   t.string :label
    #   t.timestamps
    # end

    create_table :drug_orders do |t|
      t.references :referral, foreign_key: true
      t.string :code, null: true
      t.string :name
      t.string :unit, null: true
      t.string :dosage_form, null: true
      t.text :instruction, null: true
      t.string :total_usage, null: true
      t.references :order_type, foreign_key: { to_table: 'r_order_types' }, null: true
      t.timestamps
    end
  end
end
