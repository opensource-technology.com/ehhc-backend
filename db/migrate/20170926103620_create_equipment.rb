class CreateEquipment < ActiveRecord::Migration[5.1]
  def change
    # create_table :r_equipment_types do |t|
    #   t.string :code
    #   t.string :label
    #   t.timestamps
    # end

    create_table :equipments do |t|
      t.references :referral, foreign_key: true
      t.references :type, foreign_key: { to_table: 'r_equipment_types' }, null: true
      t.string :other_description, null: true
      t.string :method, null: true
      t.timestamps
    end
  end
end
