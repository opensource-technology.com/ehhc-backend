class CreateProviders < ActiveRecord::Migration[5.1]
  def change
    create_table :provider_positions do |t|
      t.string :code
      t.string :label
      t.timestamps
    end

    create_table :providers do |t|
      t.references :prename, class_name: 'MPrename', foreign_key: { to_table: 'master.prenames' }
      t.string :first_name
      t.string :last_name, null: true
      t.references :position, class_name: 'ProviderPosition', foreign_key: { to_table: 'provider_positions' }
      t.string :communication, null: true
      t.string :register_number, null: true
      t.string :provider_identifier, null: true
      t.datetime :effective_start_at, null: true
      t.datetime :effective_end_at, null: true
      t.references :hospital, foreign_key: true
      t.string :department, null: true
      t.string :phone_number_work, null: true
      t.string :phone_number_fax, null: true
      t.string :phone_number, null: true
      t.boolean :actived
      t.timestamps
    end
  end
end
