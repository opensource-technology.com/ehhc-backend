class AllowNullToPatients < ActiveRecord::Migration[5.1]
  def change
    change_column_null :patients, :sex_id, true
    change_column_null :patients, :martial_status_id, true
    change_column_null :patients, :religion_id, true
    change_column_null :patients, :race_id, true
    change_column_null :patients, :nationality_id, true
    change_column_null :patients, :abo_group_id, true
    change_column_null :patients, :rh_group_id, true
  end
end
