class AddAndRename < ActiveRecord::Migration[5.1]
  def change
    add_column :patients, :phone_number_home, :string, null: true
    add_column :patients, :phone_number, :string, null: true

    add_column :addresses, :moo, :string, null: true
    add_column :addresses, :address_more, :string, null: true
    add_column :addresses, :map_description, :string, null: true
  end
end
