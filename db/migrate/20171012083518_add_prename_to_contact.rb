class AddPrenameToContact < ActiveRecord::Migration[5.1]
  def change
    add_reference :contacts, :prename, foreign_key: { to_table: 'master.prenames' }, index: true
  end
end
