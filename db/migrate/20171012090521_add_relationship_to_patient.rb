class AddRelationshipToPatient < ActiveRecord::Migration[5.1]
  def change
    # create_table :r_relationships do |t|
    #   t.string :code
    #   t.string :label
    #   t.timestamps
    # end

    remove_column :contacts, :relationship, :integer
    add_reference :contacts, :relationship, foreign_key: { to_table: 'r_relationships' }, index: true
  end
end
