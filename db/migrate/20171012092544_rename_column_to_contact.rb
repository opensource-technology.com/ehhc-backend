class RenameColumnToContact < ActiveRecord::Migration[5.1]
  def change
    rename_column :contacts, :mobile_phone_number, :phone_number_mobile
  end
end
