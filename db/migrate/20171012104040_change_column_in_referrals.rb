class ChangeColumnInReferrals < ActiveRecord::Migration[5.1]
  def change
    remove_column :referrals, :accident_indicator, :string
    add_column :referrals, :accident_indicator, :boolean
   
  end
end
