class CreateHosptilsUsers < ActiveRecord::Migration[5.1]
  def change
    drop_table :hospitals_users do |t|
      t.belongs_to :hospital, index: true
      t.belongs_to :user, index: true
    end

    # create_table 'master.hospitals' do |t|
    #   t.string :code
    #   t.string :label
    #   # t.timestamps
    # end

    add_reference :users, :hospital, foreign_key: { to_table: 'master.hospitals' }, index: true
    add_reference :patients, :hospital, foreign_key: { to_table: 'master.hospitals' }, index: true
  end
end
