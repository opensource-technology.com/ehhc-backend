class ChangeAttrsInReferrals < ActiveRecord::Migration[5.1]
  def change
    remove_reference :referrals, :hospital_source, index: true
    remove_reference :referrals, :hospital_destination, index: true
    add_reference :referrals, :hospital_source, foreign_key: { to_table: 'master.hospitals' }, index: true
    add_reference :referrals, :hospital_destination, foreign_key: { to_table: 'master.hospitals' }, index: true
    remove_reference :referrals, :patient_type, class_name: 'PType', foreign_key: { to_table: 'p_types' }
    add_reference :referrals, :patient_type, class_name: 'RPatientType', foreign_key: { to_table: 'r_patient_types' }
  end
end
