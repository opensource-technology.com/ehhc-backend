class AddAttrsToPatients < ActiveRecord::Migration[5.1]
  def change
    add_reference :patients, :hospital_primary, foreign_key: { to_table: 'master.hospitals' }, index: true
    add_reference :patients, :hospital_destination, foreign_key: { to_table: 'master.hospitals' }, index: true
    add_reference :patients, :hospital_usual, foreign_key: { to_table: 'master.hospitals' }, index: true
  end
end
