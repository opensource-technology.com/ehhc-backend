class ChangeNameToPatients < ActiveRecord::Migration[5.1]
  def change
    remove_reference :patients, :martial_status, foreign_key: { to_table: 'master.marital_statuses' }, index: true
    add_reference :patients, :marital_status,  foreign_key: { to_table: 'master.marital_statuses' }, index: true
  end
end
