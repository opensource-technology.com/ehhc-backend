class ChangeTypeToAddresses < ActiveRecord::Migration[5.1]
  def change
    remove_column :addresses, :address_type, :integer
    # create_table 'master.address_types' do |t|
    #   t.string :code
    #   t.string :label
    #   # t.timestamps
    # end
    add_reference :addresses, :address_type, foreign_key: { to_table: 'master.address_types' }, index: true
  end
end
