class CreateHouseTypes < ActiveRecord::Migration[5.1]
  def change
    remove_column :addresses, :house_type, :integer
    # create_table 'master.house_types' do |t|
    #   t.string :code
    #   t.string :label
    #   # t.timestamps
    # end
    add_reference :addresses, :house_type, foreign_key: { to_table: 'master.house_types' }, index: true
  end
end
