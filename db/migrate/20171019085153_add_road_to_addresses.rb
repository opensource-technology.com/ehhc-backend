class AddRoadToAddresses < ActiveRecord::Migration[5.1]
  def change
    add_column :addresses, :road, :string, null:true
  end
end
