class AddCommunityTypes < ActiveRecord::Migration[5.1]
  def change
    # create_table "master.community_types" do |t|
    #   t.string :code
    #   t.string :label
    #   t.timestamps
    # end

    add_reference :addresses, :community_type, foreign_key: { to_table: 'master.community_types' }, index: true
  end
end
