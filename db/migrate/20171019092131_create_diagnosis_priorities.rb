class CreateDiagnosisPriorities < ActiveRecord::Migration[5.1]
  def change
    # create_table "master.diagnosis_priorities" do |t|
    #   t.string :code
    #   t.string :label
    #   t.timestamps
    # end

    remove_reference :diagnoses, :priority, foreign_key: { to_table: 'r_diagnosis_priorities' }, index: true
    add_reference :diagnoses, :priority, foreign_key: { to_table: 'master.diagnosis_priorities' }, index: true
    remove_reference :referrals, :in_out_case, foreign_key: { to_table: 'r_in_out_causes' }
    add_reference :referrals, :in_out_cause, foreign_key: { to_table: 'r_in_out_causes' }

    drop_table :m_vehicles do |t|
      t.string :code
      t.string :label
      t.timestamps
    end

    add_reference :referrals, :vehicle, foreign_key: { to_table: 'r_vehicles' }, index: true
  end
end
