class ChangeModelTypes < ActiveRecord::Migration[5.1]
  def change
    change_column :sites, :hospcode, :string
    rename_column :sites, :hospcode, :code
    rename_column :referrals, :reponse_reason, :response_reason
  end
end
