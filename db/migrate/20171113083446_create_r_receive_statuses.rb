class CreateRReceiveStatuses < ActiveRecord::Migration[5.1]
  def change
    # create_table :r_receive_statuses do |t|
    #   t.string :code
    #   t.string :label
    #   t.timestamps
    # end
    add_reference :referrals, :receive_status, foreign_key: { to_table: 'r_receive_statuses' }, index: true
    add_reference :referrals, :notification_status, foreign_key: { to_table: 'r_time_notification_statuses' }, index: true
  end
end
