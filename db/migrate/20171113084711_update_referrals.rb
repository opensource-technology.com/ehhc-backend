class UpdateReferrals < ActiveRecord::Migration[5.1]
  def change
    add_column :referrals, :disease_text, :string, null: true
    add_column :referrals, :has_comorbidity, :boolean, default: false
    add_reference :referrals, :refer_type, foreign_key: { to_table: 'r_types' }, index: true
    add_reference :referrals, :patient_ability, foreign_key: { to_table: 'p_abilities' }, index: true
    add_column :referrals, :patient_medicine, :text, null: true
    add_column :referrals, :patient_problem, :text, null: true
    rename_column :referrals, :patient_activiety, :patient_activity
    add_column :referrals, :patient_appointment, :boolean, default: false
    add_column :referrals, :patient_care_giver, :boolean, default: false
    add_reference :referrals, :patient_sign_consent, foreign_key: { to_table: 'resources' }, index: true 
    add_reference :referrals, :patient_consent_form, foreign_key: { to_table: 'resources' }, index: true 
  end
end
