class CreateRTerminatedTypes < ActiveRecord::Migration[5.1]
  def change
    # create_table :r_terminated_types do |t|
    #   t.string :code
    #   t.string :label
    #   t.timestamps
    # end

    add_reference :referrals, :terminated_type, foreign_key: { to_table: 'r_terminated_types' }, index: true 
  end
end
