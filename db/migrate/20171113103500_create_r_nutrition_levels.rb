class CreateRNutritionLevels < ActiveRecord::Migration[5.1]
  def change
    # create_table :r_nutrition_levels do |t|
    #   t.string :code
    #   t.string :label
    #   t.timestamps
    # end

    remove_column :referrals, :nutrition_level, :integer
    add_reference :referrals, :nutrition_level, foreign_key: { to_table: 'r_nutrition_levels' }, index: true
  end
end
