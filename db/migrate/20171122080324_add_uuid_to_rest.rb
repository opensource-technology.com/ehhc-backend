class AddUuidToRest < ActiveRecord::Migration[5.1]
  def change
    add_column :contacts, :uuid, :string, null:false, default: SecureRandom.uuid
  end
end
