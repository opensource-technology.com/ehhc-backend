class CreateStates < ActiveRecord::Migration[5.1]
  def change
    create_table :states do |t|
      t.integer :running, default: 1

      t.timestamps
    end
  end
end
