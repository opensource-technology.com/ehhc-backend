class CreateServices < ActiveRecord::Migration[5.1]
  def change
    create_table :services, id: false, force: true do |t|
      t.string :id, primary_key: true, null: false
      t.string :key
      t.string :secret

      t.timestamps
    end
  end
end
