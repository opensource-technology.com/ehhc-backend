class UpdateAttributesInPatients < ActiveRecord::Migration[5.1]
  def change
    remove_column :patients, :passport_number, :string
    remove_column :patients, :passport_country_issues, :string
    remove_column :patients, :passport_expired_at, :date
    remove_column :patients, :phone_number, :string
    change_column_null :patients, :hn, true

    add_column :patients, :set_id, :string
    add_column :patients, :address, :string
    add_column :patients, :address_house, :string
    add_column :patients, :address_village, :string
    add_column :patients, :address_road, :string
    add_column :patients, :address_soi, :string
    add_column :patients, :address_tambon, :string
    add_column :patients, :address_amphur, :string
    add_column :patients, :address_changwat, :string
    add_column :patients, :current_address, :string
    add_column :patients, :current_address_house, :string
    add_column :patients, :current_address_village, :string
    add_column :patients, :current_address_road, :string
    add_column :patients, :current_address_soi, :string
    add_column :patients, :current_address_tambon, :string
    add_column :patients, :current_address_amphur, :string
    add_column :patients, :current_address_changwat, :string
    add_column :patients, :address_muban, :string
    add_column :patients, :community_type, :string
    add_column :patients, :house_type, :string
    add_column :patients, :house_description, :text
    add_column :patients, :house_latitude, :string
    add_column :patients, :house_logntitude, :string
    add_reference :patients, :map, foreign_key: { to_table: 'resources' }, index: true

  end
end
