class AddAttributesToPatients < ActiveRecord::Migration[5.1]
  def change
    add_column :patients, :address_postcode, :string
    add_column :patients, :current_address_postcode, :string
    add_column :patients, :address_house_id, :string
    add_column :patients, :current_address_house_id, :string
    add_column :patients, :current_address_muban, :string
    add_column :patients, :phone_number_business, :string
  end
end
