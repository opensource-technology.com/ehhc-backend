class ChangeAttributesToPatients < ActiveRecord::Migration[5.1]
  def change
    remove_column :patients, :house_type, :string
    add_reference :patients, :house_type, foreign_key: { to_table: 'master.house_types' }, index: true
    remove_column :patients, :community_type, :string
    add_reference :patients, :community_type, foreign_key: { to_table: 'master.community_types' }, index: true
  end
end
