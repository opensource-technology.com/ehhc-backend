class UpdateColumnInProviders < ActiveRecord::Migration[5.1]
  def change
    remove_reference :providers, :position, foreign_key: { to_table: :provider_positions }, index: true
    add_reference :providers, :position_type, foreign_key: { to_table: :provider_positions }, index: true
    add_column :providers, :set_id, :string, null: false
    add_column :providers, :position, :string
    change_column_default :providers, :actived, from: false, to: true
  end
end
