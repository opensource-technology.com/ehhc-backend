class DropHospitals < ActiveRecord::Migration[5.1]
  def change
    drop_table :user_profiles do |t|
      t.references :user, foreign_key: true
      t.string :prename
      t.string :first_name
      t.string :last_name
      t.string :communication_info
      t.string :register_number
      t.string :cide
      t.datetime :effective_start_at
      t.datetime :effective_end_at
      t.references :hospital, foreign_key: true
      t.references :position_code, foreign_key: true
      t.string :position
      t.string :department
      t.string :working_phone_number
      t.string :fax_number
      t.string :mobile_number
      t.timestamps
    end
    drop_table :hospitals, force: :cascade do |t|
      t.string :code
      t.string :name
      t.timestamps
    end
  end
end
