class CreateAssessments < ActiveRecord::Migration[5.1]
  def change
    create_table :assessments do |t|
      t.string :set_id, uniqueness: true, null: false
      t.references :patient_type, class_name: 'PType', foreign_key: { to_table: 'p_types' }
      t.string :change_type_reason
      t.integer :adl_score
      t.references :nursing_result, class_name: 'RNursingResult', foreign_key: { to_table: :r_nursing_results }
      t.string :care_note_summary
      t.references :appointment, class_name: 'RAppointment', foreign_key: { to_table: :r_appointments }
      t.string :visit_terminated_reason
      t.datetime :appointment_at
      t.references :visit_terminated, class_name: 'RTerminatedType', foreign_key: { to_table: :r_terminated_types }
      t.string :referral_set_id
      t.timestamps
    end
  end
end
