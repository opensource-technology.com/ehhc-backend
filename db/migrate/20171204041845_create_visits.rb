class CreateVisits < ActiveRecord::Migration[5.1]
  def change
    create_table :visits do |t|
      t.string :set_id, uniqueness: true, null: false
      t.string :patient_class
      t.string :attending_doctor
      t.string :referring_doctor
      t.string :consulting_doctor
      t.string :visit_description
      t.string :referral_source_code
      t.string :hospital_service
      t.string :visit_number
      t.datetime :admit_at
      t.datetime :discharge_at
      t.string :patient_set_id
      t.string :hospital_code
      t.string :patient_account_number
      t.string :patient_account_name
      t.timestamps
    end
  end
end
