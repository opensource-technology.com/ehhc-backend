class LinkVisitsToReferrals < ActiveRecord::Migration[5.1]
  def change
    add_reference :visits, :referral, index: true
  end
end
