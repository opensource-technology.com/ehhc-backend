class CreatePreventiveHealthMeasures < ActiveRecord::Migration[5.1]
  def change
    create_table :preventive_health_measures do |t|
      t.string :set_id
      t.string :general_characteristics
      t.float :temperature
      t.integer :pulse
      t.integer :heart_rate
      t.string :blood_pressure
      t.float :weight
      t.float :height
      t.integer :dtx
      t.references :assessments, foreign_key: true, index: true
      t.timestamps
    end
  end
end
