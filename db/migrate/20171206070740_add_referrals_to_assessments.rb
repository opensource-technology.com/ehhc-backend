class AddReferralsToAssessments < ActiveRecord::Migration[5.1]
  def change
    add_reference :assessments, :referral, foreign_key: true, index: true
    remove_reference :assessments, :patient_type, foreign_key: { to_table: 'p_types' }, index: true
    add_reference :assessments, :patient_type, foreign_key: { to_table: :r_patient_types }, index: true
    add_reference :referrals, :assessment, foreign_key: true, index: true
  end
end
