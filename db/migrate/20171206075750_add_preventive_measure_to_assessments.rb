class AddPreventiveMeasureToAssessments < ActiveRecord::Migration[5.1]
  def change
    add_reference :assessments, :preventive_health_measure, foreign_key: true, index: true
  end
end
