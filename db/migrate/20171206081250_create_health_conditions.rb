class CreateHealthConditions < ActiveRecord::Migration[5.1]
  def change
    create_table :health_conditions do |t|
      t.string :set_id
      t.string :symptom
      t.references :abnormality, class_name: 'RAbnormality', foreign_key: { to_table: 'r_abnormalities' }, index: true
      t.string :abnormality_other
      t.boolean :disability
      t.references :disability_characteristic, class_name: 'RDisabilityCharacteristic', foreign_key: { to_table: 'r_disability_characteristics' }, index: true 
      t.boolean :weakness
      t.string :weakness_characteristic
      t.boolean :pain
      t.string :pain_characteristic
      t.boolean :medication_adherence
      t.string :medicine_name
      t.boolean :edema
      t.string :edema_characteristic
      t.string :edema_grade
      t.string :problem_continuing
      t.string :problem_new_found
      t.timestamps
    end
  end
end
