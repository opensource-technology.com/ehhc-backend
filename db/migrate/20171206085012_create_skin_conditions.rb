class CreateSkinConditions < ActiveRecord::Migration[5.1]
  def change
    create_table :skin_conditions do |t|
      t.string :set_id
      t.boolean :bedsore
      t.string :bedsore_characteristic
      t.timestamps
    end
  end
end
