class CreateMoodAndBehaviorPatterns < ActiveRecord::Migration[5.1]
  def change
    create_table :mood_and_behavior_patterns do |t|
      t.string :set_id
      t.boolean :mental_condition
      t.string :mental_condition_characteristic
      t.timestamps
    end
  end
end
