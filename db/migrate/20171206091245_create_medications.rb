class CreateMedications < ActiveRecord::Migration[5.1]
  def change
    create_table :medications do |t|
      t.string :set_id
      t.boolean :allergy
      t.string :allergy_symptoms
      t.references :medicine_problem, class_name: 'RMedicineProblem', foreign_key: { to_table: 'r_medicine_problems' }, index: true 
      t.string :medicine_problem_other
      t.timestamps
    end
  end
end
