class AddAttrsToAssessments < ActiveRecord::Migration[5.1]
  def change
    add_reference :assessments, :health_condition, foreign_key: true, index: true
    add_reference :assessments, :skin_condition, foreign_key: true, index: true
    add_reference :assessments, :physical_functioning, foreign_key: true, index: true
    add_reference :assessments, :medication, foreign_key: true, index: true
    add_reference :assessments, :mood_and_behavior_pattern, foreign_key: true, index: true
    add_reference :assessments, :preventive_health_measure, foreign_key: true, index: true
  end
end
