class AddAttrsToSeviceAccess < ActiveRecord::Migration[5.1]
  def change
    add_column :service_accesses, :service_id, :string, null: false
    add_column :service_accesses, :token, :string, null: false
    add_column :service_accesses, :expired_at, :datetime, null: false
    add_column :service_accesses, :actived, :boolean
  end
end
