class AddAttrsToVisits < ActiveRecord::Migration[5.1]
  def change
    add_column :visits, :primary_hospital, :string
    add_column :visits, :regular_hospital, :string
    add_column :visits, :refer_hospital, :string
    add_reference :referrals, :visit, foreign_key: true, index: true
  end
end
