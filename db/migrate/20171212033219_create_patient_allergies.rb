class CreatePatientAllergies < ActiveRecord::Migration[5.1]
  def change
    create_table :patient_allergies do |t|
      t.string :set_id, uniqueness: true
      t.references :allegy_profile, class_name: 'PAllergyProfile', foreign_key: { to_table: 'p_allergy_profiles' }, index: true 
      t.string :reaction
      t.string :reaction_code
      t.datetime :identification_at
      t.timestamps
    end
  end
end
