class CreateAppointments < ActiveRecord::Migration[5.1]
  def change
    create_table :appointments do |t|
      t.string :set_id, uniqueness: true
      t.string :appoint_status
      t.string :clinic
      t.string :appoint_doctor
      t.datetime :appoint_at
      t.string :refer_in_out_cause
      t.string :refer_other_reason
      t.references :visit, foreign_key: true, index: true
      t.timestamps
    end
  end
end
