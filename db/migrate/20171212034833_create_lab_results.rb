class CreateLabResults < ActiveRecord::Migration[5.1]
  def change
    create_table :lab_results do |t|
      t.string :set_id, uniqueness: true
      t.string :order_control
      t.datetime :transaction_at
      t.datetime :observation_at
      t.string :observation_id
      t.string :observation_name
      t.string :observation_detail
      t.string :observation_value
      t.string :group
      t.string :unit
      t.string :reference_range
      t.string :other_detail
      t.references :visit, foreign_key: true, index: true
      t.timestamps
    end
  end
end
