class CreateVitalSigns < ActiveRecord::Migration[5.1]
  def change
    create_table :vital_signs do |t|
      t.string :set_id, uniqueness: true
      t.datetime :transaction_at
      t.float :weight
      t.float :height
      t.integer :sbp
      t.integer :dbp
      t.float :temperature
      t.integer :heart_rate
      t.integer :respiratory_rate
      t.datetime :checked_at
      t.string :other_info
      t.string :other_detail
      t.references :visit, foreign_key: true, index: true
      t.timestamps
    end
  end
end
