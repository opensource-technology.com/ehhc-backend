class CreateProcedures < ActiveRecord::Migration[5.1]
  def change
    create_table :procedures do |t|
      t.string :set_id, uniqueness: true
      t.string :code
      t.string :name
      t.datetime :procedure_at
      t.string :other_info
      t.string :other_detail
      t.references :visit, foreign_key: true, index: true
      t.timestamps
    end
  end
end
