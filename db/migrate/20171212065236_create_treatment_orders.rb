class CreateTreatmentOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :treatment_orders do |t|
      t.string :set_id, uniqueness: true
      t.string :drug_code
      t.string :drug_name
      t.string :drug_unit
      t.string :dosage_form
      t.string :instruction
      t.string :total_usage
      t.string :drug_order
      t.references :visit, foreign_key: true, index: true
      t.timestamps
    end
  end
end
