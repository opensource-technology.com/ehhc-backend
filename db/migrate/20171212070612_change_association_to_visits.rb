class ChangeAssociationToVisits < ActiveRecord::Migration[5.1]
  def change
    # remove_reference :referrals, :assessment, foreign_key: true, index: true
    # add_reference :visits, :assessment, foreign_key: true, index: true

    
    # remove_reference :patient_allergies, :visit, foreign_key: true, index: true
    add_reference :visits, :patient_allergy, foreign_key: true, index: true

    remove_reference :appointments, :visit, foreign_key: true, index: true
    add_reference :visits, :appointment, foreign_key: true, index: true

    remove_reference :x_ray_results, :visit, foreign_key: true, index: true
    add_reference :visits, :x_ray_result, foreign_key: true, index: true

    remove_reference :lab_results, :visit, foreign_key: true, index: true
    add_reference :visits, :lab_result, foreign_key: true, index: true

    remove_reference :vital_signs, :visit, foreign_key: true, index: true
    add_reference :visits, :vital_sign, foreign_key: true, index: true

    remove_reference :procedures, :visit, foreign_key: true, index: true
    add_reference :visits, :procedure, foreign_key: true, index: true

    remove_reference :treatment_orders, :visit, foreign_key: true, index: true
    add_reference :visits, :treatment_order, foreign_key: true, index: true

  end
end
