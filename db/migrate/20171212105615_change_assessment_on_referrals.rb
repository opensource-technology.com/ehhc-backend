class ChangeAssessmentOnReferrals < ActiveRecord::Migration[5.1]
  def change
    remove_reference :referrals, :assessment, foreign_key: true, index: true
  end
end
