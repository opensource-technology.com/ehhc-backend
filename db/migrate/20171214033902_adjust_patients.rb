class AdjustPatients < ActiveRecord::Migration[5.1]
  def change
    rename_column :patients, :pid, :patient_id
    rename_column :patients, :cid, :patient_id_list
    remove_reference :patients, :prename, foreign_key: { to_table: 'master.prenames' }, index: true
    add_column :patients, :prename, :string, limit: 3
    remove_reference :patients, :sex, foreign_key: { to_table: 'master.sex' }, index: true
    add_column :patients, :sex, :string, limit: 1
    remove_reference :patients, :marital_status, foreign_key: { to_table: 'master.marital_statuses' }, index: true
    add_column :patients, :marital_status, :string, limit: 1
    remove_reference :patients, :religion, foreign_key: { to_table: 'master.religions' }, index: true
    add_column :patients, :religion, :string, limit: 2
    remove_reference :patients, :race, foreign_key: { to_table: 'master.races' }, index: true
    add_column :patients, :race, :string, limit: 3
    remove_reference :patients, :nationality, foreign_key: { to_table: 'master.nationalities' }, index: true
    add_column :patients, :nationality, :string, limit: 3
    remove_reference :patients, :abo_group, foreign_key: { to_table: 'master.abo_groups' }, index: true
    add_column :patients, :abo_group, :string, limit: 1
    remove_reference :patients, :rh_group, foreign_key: { to_table: 'master.rh_groups' }, index: true
    add_column :patients, :rh_group, :string, limit: 1
    remove_reference :patients, :house_type, foreign_key: { to_table: 'master.house_types' }, index: true
    add_column :patients, :house_type, :string, limit: 3
    remove_reference :patients, :community_type, foreign_key: { to_table: 'master.community_types' }, index: true
    add_column :patients, :community_type, :string, limit: 1
  end
end
