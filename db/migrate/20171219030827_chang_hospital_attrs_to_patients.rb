class ChangHospitalAttrsToPatients < ActiveRecord::Migration[5.1]
  def change
    remove_reference :patients, :hospital, foreign_key: { to_table: 'master.hospitals' }, index: true
    add_column :patients, :hospital, :string, limit: 5
    remove_reference :patients, :hospital_primary, foreign_key: { to_table: 'master.hospitals' }, index: true
    add_column :patients, :hospital_primary, :string, limit: 5
    remove_reference :patients, :hospital_destination, foreign_key: { to_table: 'master.hospitals' }, index: true
    add_column :patients, :hospital_destination, :string, limit: 5
    remove_reference :patients, :hospital_usual, foreign_key: { to_table: 'master.hospitals' }, index: true
    add_column :patients, :hospital_usual, :string, limit: 5
  end
end
