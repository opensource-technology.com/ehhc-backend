class ChangeAttrsToContacts < ActiveRecord::Migration[5.1]
  def change
    remove_reference :contacts, :prename, foreign_key: { to_table: 'master.prenames' }, index: true
    add_column :contacts, :prename, :string, limit: 3
    remove_reference :contacts, :relationship, foreign_key: { to_table: 'r_relationships' }, index: true
    add_column :contacts, :relationship, :string, limit: 1

    rename_column :contacts, :phone_number, :phone_number_home
    rename_column :patients, :phone_number_business, :phone_number_mobile
    remove_column :patients, :uuid, :string
    remove_column :contacts, :uuid, :string
  end
end
