class ChangeAttrsToReferrals < ActiveRecord::Migration[5.1]
  def change
    remove_reference :referrals, :status, foreign_key: { to_table: 'r_statuses' }, index: true
    add_column :referrals, :status, :string, limit: 1
    remove_reference :referrals, :priority, foreign_key: { to_table: 'r_priorities' }, index: true
    add_column :referrals, :priority, :string, limit: 1
    remove_reference :referrals, :category, foreign_key: { to_table: 'r_categories' }, index: true
    add_column :referrals, :category, :string, limit: 1
    remove_reference :referrals, :in_out_cause, foreign_key: { to_table: 'r_in_out_causes' }, index: true
    add_column :referrals, :in_out_cause, :string, limit: 1
    remove_reference :referrals, :vehicle, foreign_key: { to_table: 'r_vehicles' }, index: true
    add_column :referrals, :vehicle, :string, limit: 2
    remove_reference :referrals, :hospital_source, foreign_key: { to_table: 'master.hospitals' }, index: true
    add_column :referrals, :hospital_source, :string, limit: 5
    remove_reference :referrals, :hospital_destination, foreign_key: { to_table: 'master.hospitals' }, index: true
    add_column :referrals, :hospital_destination, :string, limit: 5
    remove_reference :referrals, :channel, foreign_key: { to_table: 'r_channels' }, index: true
    add_column :referrals, :channel, :string, limit: 1
    remove_reference :referrals, :patient_type, foreign_key: { to_table: 'r_patient_types' }, index: true
    add_column :referrals, :patient_type, :string, limit: 1
    add_column :referrals, :case_manager, :string, limit: 25
  end
end
