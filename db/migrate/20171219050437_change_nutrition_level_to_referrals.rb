class ChangeNutritionLevelToReferrals < ActiveRecord::Migration[5.1]
  def change
    remove_reference :referrals, :nutrition_level, foreign_key: { to_table: 'r_nutrition_levels' }, index: true
    add_column :referrals, :nutrition_level, :string, limit: 1
    remove_reference :referrals, :notification_status, foreign_key: { to_table: 'r_time_notification_statuses' }, index: true
    remove_reference :referrals, :receive_status, foreign_key: { to_table: 'r_receive_statuses' }, index: true
    remove_reference :referrals, :terminated_type, foreign_key: { to_table: 'r_terminated_types' }, index: true
  end
end
