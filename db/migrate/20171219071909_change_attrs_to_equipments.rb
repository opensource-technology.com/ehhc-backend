class ChangeAttrsToEquipments < ActiveRecord::Migration[5.1]
  def change
    remove_reference :equipments, :type, foreign_key: { to_table: 'r_equipment_types' }, index: true
    add_column :equipments, :code, :string, limit: 2
    add_column :equipments, :set_id, :string
  end
end
