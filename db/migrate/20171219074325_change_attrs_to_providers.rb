class ChangeAttrsToProviders < ActiveRecord::Migration[5.1]
  def change
    remove_reference :providers, :prename, foreign_key: { to_table: 'master.prenames' }, index: true
    add_column :providers, :prename, :string, limit: 3
    remove_reference :providers, :position_type, foreign_key: { to_table: 'provider_positions' }, index: true
    add_column :providers, :position_type, :string, limit: 2
    # remove_reference :providers, :hospital, foreign_key: { to_table: 'master.hospitals' }, index: true
    add_column :providers, :hospital, :string, limit: 5
    rename_column :providers, :phone_number, :phone_number_mobile
  end
end
