class RemoveVisitFromReferrals < ActiveRecord::Migration[5.1]
  def change
    remove_reference :referrals, :visit, foreign_key: true, index: true
  end
end
