class ChangeAttrsToVisits < ActiveRecord::Migration[5.1]
  def change
    remove_reference :visits, :diagnosis, foreign_key: true, index: true
    add_reference :diagnoses, :visit, foreign_key: true, index: true
  end
end
