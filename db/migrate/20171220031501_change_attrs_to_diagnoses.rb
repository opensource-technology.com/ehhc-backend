class ChangeAttrsToDiagnoses < ActiveRecord::Migration[5.1]
  def change
    remove_reference :diagnoses, :referral, foreign_key: true, index: true
    remove_reference :diagnoses, :disease_code, foreign_key: { to_table: 'r_diagnoses'}, index: true
    add_column :diagnoses, :disease_code, :string, limit: 6
    # add_reference :diagnoses, :priority, foreign_key: { to_table: 'master.diagnosis_priorities' }, index: true
    remove_reference :diagnoses, :priority, foreign_key: { to_table: 'master.diagnosis_priorities' }, index: true
    add_column :diagnoses, :priority, :string, limit: 1
  end
end
