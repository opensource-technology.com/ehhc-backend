class AddDiagnosisToVisits < ActiveRecord::Migration[5.1]
  def change
    add_column :visits, :diagnosis, :text
  end
end
