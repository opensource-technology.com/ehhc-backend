class AddSetIdToDiagnoses < ActiveRecord::Migration[5.1]
  def change
    add_column :diagnoses, :set_id, :string, uniqueness: true
  end
end
