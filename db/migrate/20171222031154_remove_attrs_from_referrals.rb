class RemoveAttrsFromReferrals < ActiveRecord::Migration[5.1]
  def change
    remove_column :referrals, :patient_activity, :string
    remove_column :referrals, :disease_text, :string
    remove_column :referrals, :has_comorbidity, :boolean
    remove_reference :referrals, :patient_ability, foreign_key: { to_table: 'p_abilities' }, index: true
    remove_column :referrals, :patient_medicine, :text
    remove_column :referrals, :patient_problem, :text
    remove_column :referrals, :patient_appointment, :boolean
    remove_column :referrals, :patient_care_giver, :boolean
    remove_reference :referrals, :patient_sign_consent, foreign_key: { to_table: 'resources' }, index: true 
    remove_reference :referrals, :patient_consent_form, foreign_key: { to_table: 'resources' }, index: true 
  end
end
