class ChangeNameToVitalSigns < ActiveRecord::Migration[5.1]
  def change
    rename_column :vital_signs, :other_detail, :more_detail
  end
end
