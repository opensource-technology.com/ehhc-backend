class ChangeAttrsToPatientAllergies < ActiveRecord::Migration[5.1]
  def change
    remove_reference :patient_allergies, :allegy_profile, foreign_key: { to_table: 'p_allergy_profiles' }, index: true 
    add_column :patient_allergies, :profile, :string, limit: 1
  end
end
