class ChangeNameToProcedures < ActiveRecord::Migration[5.1]
  def change
    rename_column :procedures, :other_detail, :more_detail
  end
end
