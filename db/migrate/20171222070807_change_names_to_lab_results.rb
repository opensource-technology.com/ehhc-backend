class ChangeNamesToLabResults < ActiveRecord::Migration[5.1]
  def change
    rename_column :lab_results, :other_detail, :more_detail
  end
end
