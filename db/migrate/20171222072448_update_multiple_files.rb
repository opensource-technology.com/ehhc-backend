class UpdateMultipleFiles < ActiveRecord::Migration[5.1]
  def change
    remove_reference :referrals, :refer_type, foreign_key: { to_table: 'r_types' }, index: true    
    add_column :visits, :hospital_primary, :string, limit: 5
    add_column :visits, :hospital_regular, :string, limit: 5
    add_column :visits, :hospital_refer, :string, limit: 5
    rename_column :patients, :house_logntitude, :house_longitude
  end
end
