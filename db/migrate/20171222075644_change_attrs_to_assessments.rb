class ChangeAttrsToAssessments < ActiveRecord::Migration[5.1]
  def change
    remove_reference :assessments, :nursing_result, class_name: 'RNursingResult', foreign_key: { to_table: :r_nursing_results }, index: true
    add_column :assessments, :nursing_result, :string, limit: 1
    remove_reference :assessments, :patient_type, foreign_key: { to_table: :r_patient_types }, index: true
    add_column :assessments, :patient_type, :string, limit: 1
    remove_reference :assessments, :appointment, foreign_key: { to_table: :r_appointments }, index: true
    add_column :assessments, :appointment, :string, limit: 1
    remove_reference :assessments, :visit_terminated, foreign_key: { to_table: :r_terminated_types }, index: true
    add_column :assessments, :visit_terminated, :string, limit: 1
  end
end
