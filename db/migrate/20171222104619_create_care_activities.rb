class CreateCareActivities < ActiveRecord::Migration[5.1]
  def change
    create_table :care_activities do |t|
      t.string :set_id, uniqueness: true
      t.string :activity, limit: 2
      t.string :note
      t.references :assessment, foreign_key: true, index: true
      t.timestamps
    end
  end
end
