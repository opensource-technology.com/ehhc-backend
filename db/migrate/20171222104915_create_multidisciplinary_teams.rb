class CreateMultidisciplinaryTeams < ActiveRecord::Migration[5.1]
  def change
    create_table :multidisciplinary_teams do |t|
      t.string :set_id, uniqueness: true
      t.string :team, limit: 2
      t.string :assessor
      t.references :assessment, foreign_key: true, index: true
      t.timestamps
    end
  end
end
