class ChangeNameToPreventiveHealthMeasures < ActiveRecord::Migration[5.1]
  def change
    rename_column :preventive_health_measures, :general_characteristics, :general_characteristic
  end
end
