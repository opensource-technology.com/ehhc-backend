class ChangeAttrsToHealthConditions < ActiveRecord::Migration[5.1]
  def change
    remove_reference :health_conditions, :abnormality, foreign_key: { to_table: 'r_abnormalities' }, index: true
    add_column :health_conditions, :abnormality, :string, limit: 1
    remove_reference :health_conditions, :disability_characteristic, foreign_key: { to_table: 'r_disability_characteristics' }, index: true 
    add_column :health_conditions, :disability_characteristic, :string, limit: 1
  end
end
