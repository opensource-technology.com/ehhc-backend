class ChangeAttrsToPhysicalFunctionings < ActiveRecord::Migration[5.1]
  def change
    remove_reference :physical_functionings, :patient_ability, class_name: 'PAbility', foreign_key: { to_table: 'p_abilities' }, index: true 
  end
end
