class ChangeAttrsToMedications < ActiveRecord::Migration[5.1]
  def change
    rename_column :medications, :allergy_symptoms, :allergy_symptom
    remove_reference :medications, :medicine_problem, foreign_key: { to_table: 'r_medicine_problems' }, index: true 
    add_column :medications, :medicine_problem, :string, limit: 1
  end
end
