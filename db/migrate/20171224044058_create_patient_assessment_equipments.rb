class CreatePatientAssessmentEquipments < ActiveRecord::Migration[5.1]
  def change
    create_table :patient_assessment_equipments do |t|
      t.string :set_id, uniqueness: true
      t.string :code, limit: 2
      t.string :other_description
      t.string :method
      t.references :assessment, foreign_key: true, index: true
      t.timestamps
    end
  end
end
