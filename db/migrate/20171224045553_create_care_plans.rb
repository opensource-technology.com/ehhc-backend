class CreateCarePlans < ActiveRecord::Migration[5.1]
  def change
    create_table :care_plans do |t|
      t.string :set_id, uniqueness: true
      t.datetime :care_plan_at
      t.string :nursing_phenomena
      t.string :outcome_criteria
      t.string :nursing_intervention
      t.string :nursing_outcome
      t.string :care_manager, :string, limit: 25
      t.references :referral, foreign_key: true, index: true
      t.timestamps
    end
  end
end
