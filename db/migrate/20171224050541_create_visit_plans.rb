class CreateVisitPlans < ActiveRecord::Migration[5.1]
  def change
    create_table :visit_plans do |t|
      t.string :set_id, uniqueness: true
      t.datetime :visit_plan_at
      t.integer :visit_plan_time
      t.string :visit_plan_area, limit: 8
      t.datetime :visit_postpone_at
      t.string :visit_postpone_result
      t.string :visit_planner
      t.references :referral, foreign_key: true, index: true
      t.timestamps
    end
  end
end
