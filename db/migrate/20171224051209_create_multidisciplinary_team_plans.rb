class CreateMultidisciplinaryTeamPlans < ActiveRecord::Migration[5.1]
  def change
    create_table :multidisciplinary_team_plans do |t|
      t.string "set_id"
      t.string "team", limit: 2
      t.string "assessor"
      t.references :visit_plan, foreign_key: true, index: true
      t.timestamps
    end
  end
end
