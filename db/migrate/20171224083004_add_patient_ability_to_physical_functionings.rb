class AddPatientAbilityToPhysicalFunctionings < ActiveRecord::Migration[5.1]
  def change
    add_column :physical_functionings, :patient_ability, :string, limit: 1
  end
end
