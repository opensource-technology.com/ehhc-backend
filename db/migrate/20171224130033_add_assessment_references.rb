class AddAssessmentReferences < ActiveRecord::Migration[5.1]
  def change
    add_reference :physical_functionings, :assessment, foreign_key: true, index: true
    remove_reference :preventive_health_measures, :assessments, foreign_key: true, index: true
    add_reference :preventive_health_measures, :assessment, foreign_key: true, index: true
    add_reference :health_conditions, :assessment, foreign_key: true, index: true
    add_reference :skin_conditions, :assessment, foreign_key: true, index: true
    add_reference :mood_and_behavior_patterns, :assessment, foreign_key: true, index: true
    add_reference :medications, :assessment, foreign_key: true, index: true
  end
end
