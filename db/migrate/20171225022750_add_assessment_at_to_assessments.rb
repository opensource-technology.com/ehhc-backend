class AddAssessmentAtToAssessments < ActiveRecord::Migration[5.1]
  def change
    add_column :assessments, :assessment_at, :datetime
  end
end
