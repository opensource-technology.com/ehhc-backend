class ChangeIntegerToBooleanInHealthCondition < ActiveRecord::Migration[5.1]
  def change
    change_column :health_conditions, :disability, :string, limit: 1
    change_column :health_conditions, :weakness, :string, limit: 1
    change_column :health_conditions, :pain, :string, limit: 1
    change_column :health_conditions, :medication_adherence, :string, limit: 1
    change_column :health_conditions, :edema, :string, limit: 1
  end
end
