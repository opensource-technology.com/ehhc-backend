class ChangeBooleanToIntegerInMoodAndBehaviourPatterns < ActiveRecord::Migration[5.1]
  def change
    change_column :mood_and_behavior_patterns, :mental_condition, :string, limit: 1
  end
end
