class ChangeBooleanToIntegerInMedications < ActiveRecord::Migration[5.1]
  def change
    change_column :medications, :allergy, :string, limit: 1
  end
end
