class ChangeBooleanToIntegerInSkinCondition < ActiveRecord::Migration[5.1]
  def change
    change_column :skin_conditions, :bedsore, :string, limit: 1
  end
end
