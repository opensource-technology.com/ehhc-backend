class ChangeBooleanToIntegerInAssessments < ActiveRecord::Migration[5.1]
  def change
    change_column :assessments, :appointment, :string, limit: 1
  end
end
