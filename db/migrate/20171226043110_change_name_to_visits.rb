class ChangeNameToVisits < ActiveRecord::Migration[5.1]
  def change
    rename_column :visits, :hospital_code, :hospital
    remove_column :visits, :primary_hospital, :string
    remove_column :visits, :regular_hospital, :string
    remove_column :visits, :refer_hospital, :string
    remove_column :visits, :patient_set_id, :string
  end
end
