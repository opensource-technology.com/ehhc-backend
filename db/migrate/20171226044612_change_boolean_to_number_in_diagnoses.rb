class ChangeBooleanToNumberInDiagnoses < ActiveRecord::Migration[5.1]
  def change
    change_column :diagnoses, :confidential_indicator, :string, limit: 1
  end
end
