class ChangeBooleanToIntegerInReferrals < ActiveRecord::Migration[5.1]
  def change
    change_column :referrals, :accident_indicator, :string, limit: 1
  end
end
