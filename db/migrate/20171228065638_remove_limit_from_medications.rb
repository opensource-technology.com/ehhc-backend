class RemoveLimitFromMedications < ActiveRecord::Migration[5.1]
  def change
    change_column :medications, :medicine_problem, :string, limit: nil 
  end
end
