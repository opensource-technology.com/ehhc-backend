class AddEnabledToCareActivities < ActiveRecord::Migration[5.1]
  def change
    add_column :care_activities, :enabled, :boolean, default: false
  end
end
