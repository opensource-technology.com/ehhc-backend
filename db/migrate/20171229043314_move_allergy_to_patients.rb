class MoveAllergyToPatients < ActiveRecord::Migration[5.1]
  def change
    remove_reference :patient_allergies, :visit, foreign_key: true, index: true
    add_reference :patient_allergies, :patient, foreign_key: true, index: true
  end
end
