class AddAssessorToAssessments < ActiveRecord::Migration[5.1]
  def change
    add_column :assessments, :assessor, :string
  end
end
