class RemoveHnFromPatients < ActiveRecord::Migration[5.1]
  def change
    remove_column :patients, :hn, :string
    change_column_null :patients, :patient_id, false
    add_index :patients, :patient_id, unique: true
  end
end
