class AddReadToReferrals < ActiveRecord::Migration[5.1]
  def change
    add_column :referrals, :read, :boolean, default: false
    add_column :referrals, :sent, :boolean, default: false
  end
end
