class ChangeConstraintToPatients < ActiveRecord::Migration[5.1]
  def change
    change_column_null(:patients, :patient_id, true)
    change_column_null(:patients, :patient_id_list, false)
    add_index :patients, :patient_id_list, unique: true
    remove_index :patients, :patient_id
  end
end
