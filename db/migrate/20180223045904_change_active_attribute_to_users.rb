class ChangeActiveAttributeToUsers < ActiveRecord::Migration[5.1]
  def change
    rename_column :users, :actived, :active
  end
end
