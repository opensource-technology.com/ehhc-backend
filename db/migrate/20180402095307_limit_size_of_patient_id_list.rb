class LimitSizeOfPatientIdList < ActiveRecord::Migration[5.1]
  def change
    change_column :patients, :patient_id_list, :string, limit: 13
  end
end
