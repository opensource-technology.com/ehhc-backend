class AddFlagCreatedByClientOnPatients < ActiveRecord::Migration[5.1]
  def change
    add_column :patients, :created_by_client, :boolean, default: false
  end
end
