class AddCreatorToPatients < ActiveRecord::Migration[5.1]
  def change
    add_column :patients, :creator, :string
  end
end
