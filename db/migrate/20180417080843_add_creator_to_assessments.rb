class AddCreatorToAssessments < ActiveRecord::Migration[5.1]
  def change
    add_column :assessments, :creator, :string
  end
end
