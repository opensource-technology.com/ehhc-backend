class ChangeAccidentIndicatorToReferrals < ActiveRecord::Migration[5.1]
  def change
    change_column :referrals, :accident_indicator, :string, limit: 1, default: 'N'
  end
end
