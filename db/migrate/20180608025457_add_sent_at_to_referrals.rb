# Add sent_at (datetime) to referrals
class AddSentAtToReferrals < ActiveRecord::Migration[5.1]
  def change
    add_column :referrals, :sent_at, :datetime
  end
end
