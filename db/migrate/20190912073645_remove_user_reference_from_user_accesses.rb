class RemoveUserReferenceFromUserAccesses < ActiveRecord::Migration[5.2]
  def change
    remove_reference(:user_accesses, :user, index: true, foreign_key: true)

    add_column(:user_accesses, :user, :string)
    add_column(:user_accesses, :hospcode, :string, limit: 5)
  end
end
