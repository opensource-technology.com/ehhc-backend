class RemoveNotNullOnPatientIdList < ActiveRecord::Migration[5.2]
  def change
    change_column_null :patients, :patient_id_list, true
    remove_index :patients, :patient_id_list
  end
end
