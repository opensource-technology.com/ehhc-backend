class ChangeDefaultValueOfConfidentialIndicator < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:diagnoses, :confidential_indicator, 'N')
  end
end
