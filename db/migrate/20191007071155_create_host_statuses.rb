class CreateHostStatuses < ActiveRecord::Migration[6.0]
  def change
    create_table :host_statuses do |t|
      t.string :code
      t.string :label
      t.timestamps
    end
  end
end
