class CreateReferralChains < ActiveRecord::Migration[6.0]
  def change
    create_table :referral_chains do |t|
      t.string :set_id, uniqueness: true, null: false
      t.references :referral
      t.references :status, class_name: 'RStatus', index: true, foreign_key: { to_table: 'r_statuses' }
      t.string :hospital_source, class_name: 'Hospital'
      t.string :hospital_destination, class_name: 'Hospital'
      t.string :response_reason
      t.datetime :response_refer_at
      t.text :covisit_reason
      t.references :speciality, null: true
      t.text :speciality_other
      t.references :host_status, null: true
      t.references :parent_referral, class_name: 'Referral', null: true
      t.timestamps
    end
  end
end
