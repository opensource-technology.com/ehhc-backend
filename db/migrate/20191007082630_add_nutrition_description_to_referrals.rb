class AddNutritionDescriptionToReferrals < ActiveRecord::Migration[6.0]
  def change
    add_column :referrals, :nutrition_description, :string, null: true
    add_column :referrals, :adl_score_20, :integer, null: true
    add_column :referrals, :adl_score_100, :integer, null: true
  end
end
