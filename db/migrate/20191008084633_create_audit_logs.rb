class CreateAuditLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :audit_logs do |t|
      t.string :event, null: false
      t.text :detail, null: false
      t.string :creator, null: false
      t.timestamps
    end
  end
end
