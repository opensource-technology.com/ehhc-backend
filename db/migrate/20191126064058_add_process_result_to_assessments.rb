class AddProcessResultToAssessments < ActiveRecord::Migration[6.0]
  def change
    add_reference :assessments, :process_result
    # add_index :process_results, %i[client_code process_type], unique: true
  end
end
