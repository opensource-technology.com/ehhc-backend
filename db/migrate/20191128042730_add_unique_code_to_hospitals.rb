class AddUniqueCodeToHospitals < ActiveRecord::Migration[6.0]
  def change
    add_index 'master.hospitals', :code, unique: true
  end
end
