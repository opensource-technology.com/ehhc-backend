class RemoveAssessmentReferencs < ActiveRecord::Migration[6.0]
  def change
    remove_reference :health_conditions, :assessment
    remove_reference :skin_conditions, :assessment
    remove_reference :physical_functionings, :assessment
    remove_reference :medications, :assessment
    remove_reference :mood_and_behavior_patterns, :assessment
  end
end
