class AddParallelAndContinuousFlagToReferrals < ActiveRecord::Migration[6.0]
  def change
    add_column :referrals, :is_parallel, :boolean, null: false, default: false
    add_column :referrals, :is_continuous, :boolean, null: false, default: false
  end
end
