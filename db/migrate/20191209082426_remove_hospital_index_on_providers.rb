class RemoveHospitalIndexOnProviders < ActiveRecord::Migration[6.0]
  def change
    remove_index :providers, :hospital_id
  end
end
