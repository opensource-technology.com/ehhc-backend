class AddOtherDestinationToAssessments < ActiveRecord::Migration[6.0]
  def change
    add_column :assessments, :other_hospital_destination, :string, null: true, limit: 5
  end
end
