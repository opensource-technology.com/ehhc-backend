class ChangeNutritionLevelLimitOnReferrals < ActiveRecord::Migration[6.0]
  def change
    change_column :referrals, :nutrition_level, :string, limit: 250
  end
end
