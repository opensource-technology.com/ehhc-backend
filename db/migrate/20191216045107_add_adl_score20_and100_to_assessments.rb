class AddAdlScore20And100ToAssessments < ActiveRecord::Migration[6.0]
  def change
    add_column :assessments, :adl_score_20, :integer, null: true
    add_column :assessments, :adl_score_100, :integer, null: true
  end
end
