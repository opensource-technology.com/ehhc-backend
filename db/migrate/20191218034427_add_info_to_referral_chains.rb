class AddInfoToReferralChains < ActiveRecord::Migration[6.0]
  def change
    add_column :referral_chains, :is_parallel, :boolean, default: false
    add_column :referral_chains, :is_continuous, :boolean, default: false
  end
end
