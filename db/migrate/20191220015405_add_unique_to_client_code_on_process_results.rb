class AddUniqueToClientCodeOnProcessResults < ActiveRecord::Migration[6.0]
  def change
    add_index :process_results, :client_code, unique: true
  end
end
