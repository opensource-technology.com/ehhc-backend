class AddSomeFlagsToReferrals < ActiveRecord::Migration[6.0]
  def change
    add_column :referrals, :has_parent_opened, :boolean, null: true
    add_column :referrals, :has_children_opened, :boolean, null: true
    add_column :referrals, :pending_final, :boolean, null: true
  end
end
