class AddNodeToReferrals < ActiveRecord::Migration[6.0]
  def change
    add_column :referrals, :node, :boolean, default: false
  end
end
