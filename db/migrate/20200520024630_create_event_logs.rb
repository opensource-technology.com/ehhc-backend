class CreateEventLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :event_logs do |t|
      t.string :name, null: false
      t.string :message, null: true
      t.string :actor, null: false
      t.integer :level, null: false, default: 0
      t.timestamps
    end
  end
end
