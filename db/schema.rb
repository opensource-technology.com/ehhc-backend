# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_20_024630) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "soi"
    t.string "postcode"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "house_id"
    t.string "house"
    t.string "village"
    t.integer "tambon"
    t.integer "amphur"
    t.integer "changwat"
    t.string "phone_number_home"
    t.string "phone_number_business"
    t.bigint "map_id"
    t.bigint "patient_id"
    t.string "moo"
    t.string "address_more"
    t.string "map_description"
    t.bigint "address_type_id"
    t.bigint "house_type_id"
    t.string "road"
    t.bigint "community_type_id"
    t.index ["address_type_id"], name: "index_addresses_on_address_type_id"
    t.index ["community_type_id"], name: "index_addresses_on_community_type_id"
    t.index ["house_type_id"], name: "index_addresses_on_house_type_id"
    t.index ["map_id"], name: "index_addresses_on_map_id"
    t.index ["patient_id"], name: "index_addresses_on_patient_id"
  end

  create_table "appointments", force: :cascade do |t|
    t.string "set_id"
    t.string "appoint_status"
    t.string "clinic"
    t.string "appoint_doctor"
    t.datetime "appoint_at"
    t.string "refer_in_out_cause"
    t.string "refer_other_reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "visit_id"
    t.index ["visit_id"], name: "index_appointments_on_visit_id"
  end

  create_table "assessments", force: :cascade do |t|
    t.string "set_id", null: false
    t.string "change_type_reason"
    t.integer "adl_score"
    t.string "care_note_summary"
    t.string "visit_terminated_reason"
    t.datetime "appointment_at"
    t.string "referral_set_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "referral_id"
    t.bigint "preventive_health_measure_id"
    t.bigint "health_condition_id"
    t.bigint "skin_condition_id"
    t.bigint "physical_functioning_id"
    t.bigint "medication_id"
    t.bigint "mood_and_behavior_pattern_id"
    t.string "nursing_result", limit: 1
    t.string "patient_type", limit: 1
    t.string "appointment", limit: 1
    t.string "visit_terminated", limit: 1
    t.datetime "assessment_at"
    t.string "assessor"
    t.string "creator"
    t.bigint "process_result_id"
    t.string "other_hospital_destination", limit: 5
    t.integer "adl_score_20"
    t.integer "adl_score_100"
    t.boolean "ready", default: false
    t.index ["health_condition_id"], name: "index_assessments_on_health_condition_id"
    t.index ["medication_id"], name: "index_assessments_on_medication_id"
    t.index ["mood_and_behavior_pattern_id"], name: "index_assessments_on_mood_and_behavior_pattern_id"
    t.index ["physical_functioning_id"], name: "index_assessments_on_physical_functioning_id"
    t.index ["preventive_health_measure_id"], name: "index_assessments_on_preventive_health_measure_id"
    t.index ["process_result_id"], name: "index_assessments_on_process_result_id"
    t.index ["referral_id"], name: "index_assessments_on_referral_id"
    t.index ["skin_condition_id"], name: "index_assessments_on_skin_condition_id"
  end

  create_table "audit_logs", force: :cascade do |t|
    t.string "event", null: false
    t.text "detail", null: false
    t.string "creator", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "care_activities", force: :cascade do |t|
    t.string "set_id"
    t.string "activity", limit: 2
    t.string "note"
    t.bigint "assessment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "enabled", default: false
    t.index ["assessment_id"], name: "index_care_activities_on_assessment_id"
  end

  create_table "care_plans", force: :cascade do |t|
    t.string "set_id"
    t.datetime "care_plan_at"
    t.string "nursing_phenomena"
    t.string "outcome_criteria"
    t.string "nursing_intervention"
    t.string "nursing_outcome"
    t.string "care_manager", limit: 25
    t.string "string", limit: 25
    t.bigint "referral_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["referral_id"], name: "index_care_plans_on_referral_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "phone_number_mobile"
    t.string "phone_number_home"
    t.string "other_relationship"
    t.bigint "patient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "patients_id"
    t.string "set_id"
    t.string "prename", limit: 3
    t.string "relationship", limit: 1
    t.index ["patient_id"], name: "index_contacts_on_patient_id"
    t.index ["patients_id"], name: "index_contacts_on_patients_id"
  end

  create_table "d_priorities", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "diagnoses", force: :cascade do |t|
    t.string "disease_text"
    t.datetime "diagnosis_at"
    t.string "confidential_indicator", limit: 1, default: "N"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "visit_id"
    t.string "disease_code", limit: 6
    t.string "priority", limit: 1
    t.string "set_id"
    t.index ["visit_id"], name: "index_diagnoses_on_visit_id"
  end

  create_table "districts", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.bigint "province_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["province_id"], name: "index_districts_on_province_id"
  end

  create_table "drug_orders", force: :cascade do |t|
    t.bigint "referral_id"
    t.string "code"
    t.string "name"
    t.string "unit"
    t.string "dosage_form"
    t.text "instruction"
    t.string "total_usage"
    t.bigint "order_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_type_id"], name: "index_drug_orders_on_order_type_id"
    t.index ["referral_id"], name: "index_drug_orders_on_referral_id"
  end

  create_table "equipments", force: :cascade do |t|
    t.bigint "referral_id"
    t.string "other_description"
    t.string "method"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code", limit: 2
    t.string "set_id"
    t.index ["referral_id"], name: "index_equipments_on_referral_id"
  end

  create_table "event_logs", force: :cascade do |t|
    t.string "name", null: false
    t.string "message"
    t.string "actor", null: false
    t.integer "level", default: 0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "health_conditions", force: :cascade do |t|
    t.string "set_id"
    t.string "symptom"
    t.string "abnormality_other"
    t.string "disability", limit: 1
    t.string "weakness", limit: 1
    t.string "weakness_characteristic"
    t.string "pain", limit: 1
    t.string "pain_characteristic"
    t.string "medication_adherence", limit: 1
    t.string "medicine_name"
    t.string "edema", limit: 1
    t.string "edema_characteristic"
    t.string "edema_grade"
    t.string "problem_continuing"
    t.string "problem_new_found"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "abnormality", limit: 1
    t.string "disability_characteristic", limit: 1
  end

  create_table "host_statuses", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "lab_results", force: :cascade do |t|
    t.string "set_id"
    t.string "order_control"
    t.datetime "transaction_at"
    t.datetime "observation_at"
    t.string "observation_id"
    t.string "observation_name"
    t.string "observation_detail"
    t.string "observation_value"
    t.string "group"
    t.string "unit"
    t.string "reference_range"
    t.string "more_detail"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "visit_id"
    t.index ["visit_id"], name: "index_lab_results_on_visit_id"
  end

  create_table "medications", force: :cascade do |t|
    t.string "set_id"
    t.string "allergy", limit: 1
    t.string "allergy_symptom"
    t.string "medicine_problem_other"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "medicine_problem"
  end

  create_table "mood_and_behavior_patterns", force: :cascade do |t|
    t.string "set_id"
    t.string "mental_condition", limit: 1
    t.string "mental_condition_characteristic"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "multidisciplinary_team_plans", force: :cascade do |t|
    t.string "set_id"
    t.string "team", limit: 2
    t.string "assessor"
    t.bigint "visit_plan_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["visit_plan_id"], name: "index_multidisciplinary_team_plans_on_visit_plan_id"
  end

  create_table "multidisciplinary_teams", force: :cascade do |t|
    t.string "set_id"
    t.string "team", limit: 2
    t.string "assessor"
    t.bigint "assessment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["assessment_id"], name: "index_multidisciplinary_teams_on_assessment_id"
  end

  create_table "p_abilities", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "p_adl_groups", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "p_allergy_profiles", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "p_appointments", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "p_equipments", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "p_statuses", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "p_types", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "p_visit_consents", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "p_visit_terminated_statuses", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "patient_allergies", force: :cascade do |t|
    t.string "set_id"
    t.string "reaction"
    t.string "reaction_code"
    t.datetime "identification_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "profile", limit: 1
    t.bigint "patient_id"
    t.index ["patient_id"], name: "index_patient_allergies_on_patient_id"
  end

  create_table "patient_assessment_equipments", force: :cascade do |t|
    t.string "set_id"
    t.string "code", limit: 2
    t.string "other_description"
    t.string "method"
    t.bigint "assessment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["assessment_id"], name: "index_patient_assessment_equipments_on_assessment_id"
  end

  create_table "patients", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.date "birthdate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "patient_id"
    t.string "patient_id_list", limit: 13
    t.string "account_name"
    t.string "account_number"
    t.string "phone_number_home"
    t.string "set_id"
    t.string "address"
    t.string "address_house"
    t.string "address_village"
    t.string "address_road"
    t.string "address_soi"
    t.string "address_tambon"
    t.string "address_amphur"
    t.string "address_changwat"
    t.string "current_address"
    t.string "current_address_house"
    t.string "current_address_village"
    t.string "current_address_road"
    t.string "current_address_soi"
    t.string "current_address_tambon"
    t.string "current_address_amphur"
    t.string "current_address_changwat"
    t.string "address_muban"
    t.text "house_description"
    t.string "house_latitude"
    t.string "house_longitude"
    t.bigint "map_id"
    t.string "address_postcode"
    t.string "current_address_postcode"
    t.string "address_house_id"
    t.string "current_address_house_id"
    t.string "current_address_muban"
    t.string "phone_number_mobile"
    t.string "prename", limit: 3
    t.string "sex", limit: 1
    t.string "marital_status", limit: 1
    t.string "religion", limit: 2
    t.string "race", limit: 3
    t.string "nationality", limit: 3
    t.string "abo_group", limit: 1
    t.string "rh_group", limit: 1
    t.string "house_type", limit: 3
    t.string "community_type", limit: 1
    t.string "hospital", limit: 5
    t.string "hospital_primary", limit: 5
    t.string "hospital_destination", limit: 5
    t.string "hospital_usual", limit: 5
    t.boolean "created_by_client", default: false
    t.string "creator"
    t.index ["map_id"], name: "index_patients_on_map_id"
  end

  create_table "pharmacies", force: :cascade do |t|
    t.string "set_id"
    t.string "drug_code"
    t.string "drug_name"
    t.string "drug_unit"
    t.string "dosage_form"
    t.string "instruction"
    t.string "total_usage"
    t.string "drug_order"
    t.bigint "assessment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["assessment_id"], name: "index_pharmacies_on_assessment_id"
  end

  create_table "physical_functionings", force: :cascade do |t|
    t.string "set_id"
    t.string "excretion", limit: 1
    t.string "excretion_abnormally"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "patient_ability", limit: 1
  end

  create_table "position_codes", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "preferential_treatments", force: :cascade do |t|
    t.string "code"
    t.bigint "patient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["patient_id"], name: "index_preferential_treatments_on_patient_id"
  end

  create_table "preventive_health_measures", force: :cascade do |t|
    t.string "set_id"
    t.string "general_characteristic"
    t.float "temperature"
    t.integer "pulse"
    t.integer "heart_rate"
    t.string "blood_pressure"
    t.float "weight"
    t.float "height"
    t.integer "dtx"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "assessment_id"
    t.index ["assessment_id"], name: "index_preventive_health_measures_on_assessment_id"
  end

  create_table "procedures", force: :cascade do |t|
    t.string "set_id"
    t.string "code"
    t.string "name"
    t.datetime "procedure_at"
    t.string "other_info"
    t.string "more_detail"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "visit_id"
    t.index ["visit_id"], name: "index_procedures_on_visit_id"
  end

  create_table "process_results", force: :cascade do |t|
    t.string "code_check", limit: 20
    t.string "client_code", limit: 20, null: false
    t.string "hospcode", limit: 5, null: false
    t.integer "status", limit: 2, default: 1, null: false
    t.string "status_description"
    t.integer "sent_status", limit: 2, default: 1, null: false
    t.string "sent_errors"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["client_code"], name: "index_process_results_on_client_code", unique: true
  end

  create_table "provider_positions", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "providers", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "communication"
    t.string "register_number"
    t.string "provider_identifier"
    t.datetime "effective_start_at"
    t.datetime "effective_end_at"
    t.bigint "hospital_id"
    t.string "department"
    t.string "phone_number_work"
    t.string "phone_number_fax"
    t.string "phone_number_mobile"
    t.boolean "actived", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "set_id", null: false
    t.string "position"
    t.string "prename", limit: 3
    t.string "position_type", limit: 2
    t.string "hospital", limit: 5
  end

  create_table "provinces", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.bigint "region_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["region_id"], name: "index_provinces_on_region_id"
  end

  create_table "r_abnormalities", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_adherence_types", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_appointments", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_care_activity_types", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_categories", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_channels", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_diagnoses", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_diagnosis_priorities", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_disability_characteristics", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_drug_units", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_equipment_types", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_in_out_causes", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_medicine_problems", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_nursing_results", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_nutrition_levels", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_order_types", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_patient_classes", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_patient_types", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_priorities", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_receive_statuses", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_relationships", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_statuses", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_team_types", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_terminated_types", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_time_notification_statuses", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_types", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_vehicles", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_yes_nos", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "referral_chains", force: :cascade do |t|
    t.string "set_id", null: false
    t.bigint "referral_id"
    t.bigint "status_id"
    t.string "hospital_source"
    t.string "hospital_destination"
    t.string "response_reason"
    t.datetime "response_refer_at"
    t.text "covisit_reason"
    t.bigint "speciality_id"
    t.text "speciality_other"
    t.bigint "host_status_id"
    t.bigint "parent_referral_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "is_parallel", default: false
    t.boolean "is_continuous", default: false
    t.index ["host_status_id"], name: "index_referral_chains_on_host_status_id"
    t.index ["parent_referral_id"], name: "index_referral_chains_on_parent_referral_id"
    t.index ["referral_id"], name: "index_referral_chains_on_referral_id"
    t.index ["speciality_id"], name: "index_referral_chains_on_speciality_id"
    t.index ["status_id"], name: "index_referral_chains_on_status_id"
  end

  create_table "referrals", force: :cascade do |t|
    t.string "clinic"
    t.string "originating_referral_id"
    t.datetime "effective_at"
    t.datetime "appointment_at"
    t.datetime "expiration_at"
    t.string "other_reason"
    t.string "in_out_family_symptom"
    t.string "in_out_current_symptom"
    t.string "in_out_summary_investigation"
    t.string "in_out_summary_diagnosis"
    t.string "in_out_summary_treatment"
    t.string "request"
    t.string "accident_detail"
    t.string "in_out_other_detail"
    t.string "response_reason"
    t.datetime "response_refer_at"
    t.string "channel_other"
    t.datetime "assessment_at"
    t.integer "adl_score"
    t.string "patient_condition"
    t.bigint "patient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "accident_indicator", limit: 1, default: "N"
    t.string "set_id"
    t.string "status", limit: 1
    t.string "priority", limit: 1
    t.string "category", limit: 1
    t.string "in_out_cause", limit: 1
    t.string "vehicle", limit: 2
    t.string "hospital_source", limit: 5
    t.string "hospital_destination", limit: 5
    t.string "channel", limit: 1
    t.string "patient_type", limit: 1
    t.string "case_manager", limit: 25
    t.string "nutrition_level", limit: 250
    t.boolean "read", default: false
    t.boolean "sent", default: false
    t.datetime "sent_at"
    t.string "nutrition_description"
    t.integer "adl_score_20"
    t.integer "adl_score_100"
    t.boolean "is_parallel", default: false, null: false
    t.boolean "is_continuous", default: false, null: false
    t.boolean "has_parent_opened"
    t.boolean "has_children_opened"
    t.boolean "pending_final"
    t.boolean "node", default: false
    t.index ["patient_id"], name: "index_referrals_on_patient_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "resources", force: :cascade do |t|
    t.string "name"
    t.string "image"
    t.boolean "actived"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.boolean "actived"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_accesses", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "service_id", null: false
    t.string "token", null: false
    t.datetime "expired_at", null: false
    t.boolean "actived"
  end

  create_table "services", id: :string, force: :cascade do |t|
    t.string "key"
    t.string "secret"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sites", force: :cascade do |t|
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "skin_conditions", force: :cascade do |t|
    t.string "set_id"
    t.string "bedsore", limit: 1
    t.string "bedsore_characteristic"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "specialities", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "states", force: :cascade do |t|
    t.integer "running", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "treatment_orders", force: :cascade do |t|
    t.string "set_id"
    t.string "drug_code"
    t.string "drug_name"
    t.string "drug_unit"
    t.string "dosage_form"
    t.string "instruction"
    t.string "total_usage"
    t.string "drug_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "visit_id"
    t.index ["visit_id"], name: "index_treatment_orders_on_visit_id"
  end

  create_table "user_accesses", force: :cascade do |t|
    t.string "token"
    t.datetime "expired_at"
    t.boolean "actived", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "user"
    t.string "hospcode", limit: 5
  end

  create_table "users", force: :cascade do |t|
    t.string "uuid"
    t.string "username"
    t.string "password_digest", null: false
    t.string "token"
    t.string "retoken"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: true
    t.bigint "hospital_id"
    t.index ["hospital_id"], name: "index_users_on_hospital_id"
  end

  create_table "users_roles", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  create_table "visit_plans", force: :cascade do |t|
    t.string "set_id"
    t.datetime "visit_plan_at"
    t.integer "visit_plan_time"
    t.string "visit_plan_area", limit: 8
    t.datetime "visit_postpone_at"
    t.string "visit_postpone_result"
    t.string "visit_planner"
    t.bigint "referral_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["referral_id"], name: "index_visit_plans_on_referral_id"
  end

  create_table "visits", force: :cascade do |t|
    t.string "set_id", null: false
    t.string "patient_class"
    t.string "attending_doctor"
    t.string "referring_doctor"
    t.string "consulting_doctor"
    t.string "visit_description"
    t.string "referral_source_code"
    t.string "hospital_service"
    t.string "visit_number"
    t.datetime "admit_at"
    t.datetime "discharge_at"
    t.string "hospital"
    t.string "patient_account_number"
    t.string "patient_account_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "referral_id"
    t.text "diagnosis"
    t.string "hospital_primary", limit: 5
    t.string "hospital_regular", limit: 5
    t.string "hospital_refer", limit: 5
    t.index ["referral_id"], name: "index_visits_on_referral_id"
  end

  create_table "vital_signs", force: :cascade do |t|
    t.string "set_id"
    t.datetime "transaction_at"
    t.float "weight"
    t.float "height"
    t.integer "sbp"
    t.integer "dbp"
    t.float "temperature"
    t.integer "heart_rate"
    t.integer "respiratory_rate"
    t.datetime "checked_at"
    t.string "other_info"
    t.string "more_detail"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "visit_id"
    t.index ["visit_id"], name: "index_vital_signs_on_visit_id"
  end

  create_table "x_ray_results", force: :cascade do |t|
    t.string "set_id"
    t.string "order_control"
    t.datetime "transaction_at"
    t.datetime "observation_at"
    t.string "observation_detail"
    t.string "observation_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "visit_id"
    t.index ["visit_id"], name: "index_x_ray_results_on_visit_id"
  end

  add_foreign_key "addresses", "master.address_types", column: "address_type_id"
  add_foreign_key "addresses", "master.community_types", column: "community_type_id"
  add_foreign_key "addresses", "master.house_types", column: "house_type_id"
  add_foreign_key "addresses", "patients"
  add_foreign_key "addresses", "resources", column: "map_id"
  add_foreign_key "appointments", "visits"
  add_foreign_key "assessments", "health_conditions"
  add_foreign_key "assessments", "medications"
  add_foreign_key "assessments", "mood_and_behavior_patterns"
  add_foreign_key "assessments", "physical_functionings"
  add_foreign_key "assessments", "preventive_health_measures"
  add_foreign_key "assessments", "referrals"
  add_foreign_key "assessments", "skin_conditions"
  add_foreign_key "care_activities", "assessments"
  add_foreign_key "care_plans", "referrals"
  add_foreign_key "contacts", "patients", column: "patients_id"
  add_foreign_key "diagnoses", "visits"
  add_foreign_key "districts", "provinces"
  add_foreign_key "drug_orders", "r_order_types", column: "order_type_id"
  add_foreign_key "drug_orders", "referrals"
  add_foreign_key "equipments", "referrals"
  add_foreign_key "lab_results", "visits"
  add_foreign_key "multidisciplinary_team_plans", "visit_plans"
  add_foreign_key "multidisciplinary_teams", "assessments"
  add_foreign_key "patient_allergies", "patients"
  add_foreign_key "patient_assessment_equipments", "assessments"
  add_foreign_key "patients", "resources", column: "map_id"
  add_foreign_key "pharmacies", "assessments"
  add_foreign_key "preventive_health_measures", "assessments"
  add_foreign_key "procedures", "visits"
  add_foreign_key "provinces", "regions"
  add_foreign_key "referral_chains", "r_statuses", column: "status_id"
  add_foreign_key "referrals", "patients"
  add_foreign_key "treatment_orders", "visits"
  add_foreign_key "visit_plans", "referrals"
  add_foreign_key "vital_signs", "visits"
  add_foreign_key "x_ray_results", "visits"
end
