# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# require 'csv'

# unless Role.find_by_name('admin')
#   Role.create name: 'admin'
# end

# admin_role = Role.find_by_name('admin')

# unless User.find_by_username('admin')
#   admin_user = User.create username: 'admin', password: 'admin'
#   admin_user.roles << admin_role
# end

# if MAddressChangwat.first.nil?
#   CSV.readlines('db/loads/addr_changwat_list.csv').each do |row|
#     puts "#{row[0]} : #{row[1]}"
#     MAddressChangwat.create code: row[0], label: row[1] unless row[1].include?('*')
#   end
# end

# if MAddressAmphur.first.nil?
#   CSV.readlines('db/loads/addr_amphur_list.csv').each do |row|
#     puts "#{row[0]} : #{row[1]}" unless row[1].include?('*')
#     MAddressAmphur.create code: row[0], label: row[1] unless row[1].include?('*')
#   end
# end

# if MAddressTambon.first.nil?
#   CSV.readlines('db/loads/addr_tambon_list.csv').each do |row|
#     puts "#{row[0]} : #{row[1]}" unless row[1].include?('*')
#     MAddressTambon.create code: row[0], label: row[1] unless row[1].include?('*')
#   end
# end

# CSV.readlines('db/loads/hospital_list.csv').each do |row|
#   puts "#{row[0]} : #{row[1]}"
#   Hospital.create code: row[0], label: row[1] unless Hospital.find_by(code: row[0])
# end
# require 'csv'
# # import hoscode
# CSV.readlines('db/loads/ehhc_hcode_diff.csv', headers: true).each do |row|
#   puts "#{row[0]} #{row[1]}"
#   Hospital.create code: row[0], label: row[1] unless Hospital.find_by(code: row[0])
# end

# require 'ehhc'
# # require_relative './migrator
# array_texts = [
#   "adl_score",
#   "age_mm",
#   "age_year",
#   "ass_set_id",
#   "assessment_date",
#   "assessor_first_name",
#   "assessor_id",
#   "assessor_last_name",
#   "assessor_position",
#   "disease_code",
#   "disease_text",
#   "birth_date",
#   "category",
#   "catm",
#   "first_name",
#   "hcode_destination",
#   "hospcode",
#   "house",
#   "hv_client_code",
#   "last_name",
#   "patient_type",
#   "phonenumber",
#   "pid",
#   "postcode",
#   "road",
#   "sex",
#   "soimain",
#   "status",
#   "villaname",
#   "sbp",
#   "dbp",
#   "btemp",
#   "pr",
#   "rr",
#   "weight",
#   "height",
#   "physical_dtx"
# ]
# year = [2018] # ปีที่ run
# year.each do |y|
#   range_date = (1...13).map do |m| # เดือนที่ run 1-20
#     [Date.new(y, m, 1).to_s, Date.new(y, m, 1).end_of_month.to_s]
#   end

#   range_date.each do |range|
#     p "**********"
#     p "#{range[0]} - #{range[1]}"
#     ass_list = Assessment.where('created_at BETWEEN ? AND ?', range[0], range[1])
#     CSV.open("./db/loads/assessment#{range[0]}-#{range[1]}.csv", "wb") do |csv|
#       csv << array_texts
#       ass_list.each do |ass|
#         p ass
#         item = EHHC::Item.new ass
#         csv << item.to_data
#       end
#     end
#   end
# end
# last_id = 100000010436
# list = []

# def find_chain_root(referral_id)
#   referral = Referral.find(referral_id)
#   return referral if referral.referral_chain.nil?

#   parent_referral = referral.referral_chain.parent_referral
#   return find_chain_root(parent_referral.id)
# end

# # pp find_chain_root(100000010477)

# def find_chain_referrals(referral_id)
#   Referral.select(:id, :set_id, :hospital_source, :hospital_destination, :is_parallel, :is_continuous)
#         .includes(:assessments).joins(:referral_chain)
#         .where("referral_chains.parent_referral_id = :referral_id", referral_id: referral_id)
# end

# def find_chain_parent(root_referral_id)
#   Referral.select(:id, :set_id, :hospital_source, :hospital_destination, :is_parallel, :is_continuous)
#         .joins(:referral_chain)
#         .where("node = true and referral_chains.parent_referral_id = :referral_id", referral_id: root_referral_id)
# end

# def build_asessments(referral)
#   {
#     referal: referral.set_id,
#     hospital_source: Hospital.by_code(referral.hospital_source).as_json,
#     hospital_destination: Hospital.by_code(referral.hospital_destination).as_json,
#     assessments: referral.assessments.order(assessment_at: :desc).as_json(list: true),
#     is_parallel: referral.is_parallel,
#     is_continuous: referral.is_continuous
#   }
# end

# def traverse_chains(referral_id, assessments)
#   referrals = find_chain_referrals(referral_id)
#   referrals.each do |referral|
#     assessments.append(build_asessments(referral)) # add cha
#     traverse_chains(referral.id, assessments)
#   end
# end

# def find_history(referral_id)
#   root_referral = find_chain_root(referral_id)
#   parent_referrals = find_chain_parent(root_referral.id)
#   assessments = []
#   assessments.append(build_asessments(root_referral))

#   traverse_chains(root_referral.id, assessments)
#   assessments
# end

# pp find_history(100000010477)
# def find_associate_parent(list, last_id)
#   list.prepend(last_id) unless list.include?(last_id)
#   chains = ReferralChain.where("parent_referral_id = :id or referral_id = :id", id: last_id).map(&:referral_id).uniq
#   chains = chains - list
#   chains.each do |c|
#     find_associate_parent(list, c)
#   end

#   list
# end

# # chains = ReferralChain.where("parent_referral_id = :id or referral_id = :id", id: 100000010401)
# pp find_associate_parent(list, last_id)

# def parent_assessment(referral)
#   {
#     referral: referral.set_id,
#     hospital_source: Hospital.by_code(referral.hospital_source).as_json,
#     hospital_destination: Hospital.by_code(referral.hospital_destination).as_json,
#     assessments: referral.assessments.order(assessment_at: :desc).as_json(list: true)
#   }
# end



# def extract_assessments(chains)
#   assessments = []
#   return assessments if chains.nil?
#   chains.each do |chain|
#     referral = chain.referral
#     if referral.status != 'R'
#       ams = {
#         referral: referral.set_id,
#         hospital_source: Hospital.by_code(referral.hospital_source).as_json,
#         hospital_destination: Hospital.by_code(referral.hospital_destination).as_json,
#         assessments: referral.assessments.order(assessment_at: :desc).as_json(list: true),
#         is_parallel: referral.is_parallel,
#         is_continuous: referral.is_continuous
#       }
#       assessments.push(ams)
#     end
#   end
#   assessments
# end

# @referral = Referral.find(100000010401)

# assessments = []
# parent_referral = if @referral.chain_parent?
#                     # find both parallel and continuous
#                     @referral
#                   else
#                     @referral.referral_chain.parent_referral
#                   end

# # assessments.push(parent_assessment(parent_referral))
# associate_list = find_associate_parent([], parent_referral.id)
# associate_list.each do |parent|
#   puts "Parent is #{parent}"
#   assessments.push(parent_assessment(Referral.find(parent)))
#   chains = ReferralChain.where("parent_referral_id = :parent_referral_id and (is_parallel = 'true' or is_continuous = 'true')",
#                             { parent_referral_id: parent })
#   assessments.concat(extract_assessments(chains))
# end


# pp assessments
require_relative './seeds/assessments'
puts "[SEED]: Success"
