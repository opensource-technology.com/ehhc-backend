# Error Codes

| Code | Message |
|:----:|---------|
| 10   | user doesn't exist
| 11   | username or password mismatch
| 22   | token not found