# วิธีการเข้ารหัสเพื่อให้ได้ service_signature
SERVICE_ID = 'a2290b89a2901e97b4d0e5537356179393d6e224d6042d69dd4fabd403f0_53f235a23b68a6bde588'
SERVICE_KEY = '41ea8462-aced-4cd9-9ee5-629fce39aadc'
SERVICE_SECRET = '6bf2c3c3-0afa-4377-bd8e-f58b97d8c494'

- จาก service account ด้านบนให้สร้าง message ที่เกิดจากการต่อกันระหว่าง SERVICE_ID, timestamp (เวลาปัจจุบัน หน่วยมิลลิเซค) และ SERVICE_KEY โดยมีเครื่องหมาย : คั่นกลางแต่ละตัว ดังตัวอย่าง

```
message = "a2290b89a2901e97b4d0e5537356179393d6e224d6042d69dd4fabd403f0_53f235a23b68a6bde588:41ea8462-aced-4cd9-9ee5-629fce39aadc:6bf2c3c3-0afa-4377-bd8e-f58b97d8c494"
```

- จากนั้นนำ message ที่ได้ไปเข้ารหัส HMAC-SHA256 โดยใช้ message เป็น data และใช้ SERVICE_SECRET เป็น key ในการเข้ารหัส ตัวอย่างเช่น

```
signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest::SHA256.new, SERVICE_SECRET, message)
```

# ตัวอย่างการล๊อกอิน
curl -X POST http://203.185.67.81:5000/api/auth/login -H "Content-Type: application/json" -d '{"service_id":"a2290b89a2901e97b4d0e5537356179393d6e224d6042d69dd4fabd403f0_53f235a23b68a6bde588", "service_key":"41ea8462-aced-4cd9-9ee5-629fce39aadc", "service_signature":"822001f0a13c04a245902e0c8baadb735302614722d3d39754467018eda9ce59", "timestamp":"1531724638930"}'

# ผลลัพท์
{"service_id":"a2290b89a2901e97b4d0e5537356179393d6e224d6042d69dd4fabd403f0_53f235a23b68a6bde588","token":"eyJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7InNlcnZpY2VfaWQiOiJhMjI5MGI4OWEyOTAxZTk3YjRkMGU1NTM3MzU2MTc5MzkzZDZlMjI0ZDYwNDJkNjlkZDRmYWJkNDAzZjBfNTNmMjM1YTIzYjY4YTZiZGU1ODgiLCJ0aW1lc3RhbXAiOiIxNTMxNzI0NjM4OTMwIn0sImV4cCI6MTUzMTczOTE1OX0.hu17hM8yJN3K6grIYJVRVTJaBWLgzX-IaTXsiFtbkAY","roles":["service"],"expired_at":"2018-07-16T18:05:59.894+07:00","hospital":{"code":"11482","label":"โรงพยาบาลภูมิพลอดุลยเดช พอ.บนอ."}}

จากนั้นเมื่อมีการเรียกใช้ service อื่นๆ จะต้องนำ token ที่ได้มาใส่ใน Header ของการ request ทุกครั้ง โดยใส่ใน Header ชื่อ X-Access-token ดังตัวอย่าง
curl -X GET http://203.185.67.81:5000/api/referrals -n" -H "X-Access-Token: eyJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7InNlcnZpY2VfaWQiOiJhMjI5MGI4OWEyOTAxZTk3YjRkMGU1NTM3MzU2MTc5MzkzZDZlMjI0ZDYwNDJkNjlkZDRmYWJkNDAzZjBfNTNmMjM1YTIzYjY4YTZiZGU1ODgiLCJ0aW1lc3RhbXAiOiIxNTMxNzI0NjM4OTMwIn0sImV4cCI6MTUzMTczOTE1OX0.hu17hM8yJN3K6grIYJVRVTJaBWLgzX-IaTXsiFtbkAY"