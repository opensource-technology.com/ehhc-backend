Title: EHHC-BKK Flows

_: **1. Login เพื่อเข้าใช้งานระบบ**
Client1 -> Server: POST /api/auth/login
Server -> Client1: 200 OK with token

_: **2. ส่งข้อมูล refer ไปยังปลายทางจาก HIS**
Client1 -> Server: POST /api/referrals/refer
note: Attach token in X-Access-Token Header
Server -> Client1: 200 OK

_: **3. ดึงข้อมูล refer ขาเข้ามายังศูนย์**
Client2 -> Server: GET /api/referrals/inbound
note: Attach token in X-Access-Token Header
Server -> Client2: 200 OK

_: **4. ดึงข้อมูล refer ขาออกที่ส่งไปยังปลายทาง**
Client1 -> Server: GET /api/referrals/inbound
note: Attach token in X-Access-Token Header
Server -> Client1: 200 OK

_: **5. ดึงข้อมูล refer รายตัว**
Client2 -> Server: GET /api/referrals/:referral_id
note: Attach token in X-Access-Token Header
Server -> Client2: 200 OK

_: **6. ทำการสร้าง assessment ให้กับ refer**
Client2 -> Server: POST /api/referrals/:referral_id/assessments
note: Attach token in X-Access-Token Header
Server -> Client2: 200 OK

_: **7. ทำการอัพเดต assessment รายตัวให้กับ refer**
Client2 -> Server: PUT /api/referrals/:referral_id/assessments/:assessment_id
note: Attach token in X-Access-Token Header
Server -> Client2: 200 OK

_: **8. ทำการลบ assessment รายตัวให้กับ refer**
Client2 -> Server: DELETE /api/referrals/:referral_id/assessments/:assessment_id
note: Attach token in X-Access-Token Header
Server -> Client2: 200 OK

_: **9. ทำการส่งข้อมูล refer กลับไปยังต้นทาง พร้อมข้อมูล assessments**
Client2 -> Server: POST /api/referrals/:referral_id/flush
note: Attach token in X-Access-Token Header
Server -> Client2: 200 OK