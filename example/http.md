# Login
http http://localhost:5000/api/auth/login username=demo password=demo

# Get patients
http http://localhost:5000/api/patients X-Access-Token:{ACCESS_TOKEN}

# Create new patient
http POST http://localhost:5000/api/patients X-Access-Token:{ACCESS_TOKEN} \
  first_name=patient1 last_name=patient1 hn=HN100001 prename_id=2 

# Create new contact belongs to patient
http POST http://localhost:5000/api/patients/{patientId}/contacts X-Access-Token:{ACCESS_TOKEN} \
  prename_id=2 first_name=contact1 last_name=contact_1 phone_number_mobile=0817729932 relationship_id=1

### Example:
```
http POST http://localhost:5000/api/patients/7/contacts X-Access-Token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiZGVtbyIsInJldG9rZW4iOiJlZDk0ZmEyMGZhMzcxODlkYjc2NmE1YzQyM2EyNmIwOSJ9LCJleHAiOjE1MDc4MTQ1MjJ9.j3ewI1wNyOmyXGxA-mZRdyZzn6B6PQ7doQI1rIUNr5E \
prename_id=2 first_name=contact1 last_name=contact_1 phone_number_mobile=0817729932 relationship_id=1
```

# วิธีการเข้ารหัสเพื่อให้ได้ service_signature
SERVICE_ID = 'a2290b89a2901e97b4d0e5537356179393d6e224d6042d69dd4fabd403f0_53f235a23b68a6bde588'
SERVICE_KEY = '41ea8462-aced-4cd9-9ee5-629fce39aadc'
SERVICE_SECRET = '6bf2c3c3-0afa-4377-bd8e-f58b97d8c494'

- จาก service account ด้านบนให้สร้าง message ที่เกิดจากการต่อกันระหว่าง SERVICE_ID, timestamp (เวลาปัจจุบัน หน่วยมิลลิเซค) และ SERVICE_KEY โดยมีเครื่องหมาย : คั่นกลางแต่ละตัว ดังตัวอย่าง

```
message = "a2290b89a2901e97b4d0e5537356179393d6e224d6042d69dd4fabd403f0_53f235a23b68a6bde588:41ea8462-aced-4cd9-9ee5-629fce39aadc:6bf2c3c3-0afa-4377-bd8e-f58b97d8c494"
```

- จากนั้นนำ message ที่ได้ไปเข้ารหัส HMAC-SHA256 โดยใช้ message เป็น data และใช้ SERVICE_SECRET เป็น key ในการเข้ารหัส ตัวอย่างเช่น

```
signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest::SHA256.new, SERVICE_SECRET, message)
```

# ตัวอย่างการล๊อกอิน
curl -X POST http://203.185.67.81:5000/api/auth/login -H "Content-Type: application/json" -d '{"service_id":"a2290b89a2901e97b4d0e5537356179393d6e224d6042d69dd4fabd403f0_53f235a23b68a6bde588", "service_key":"41ea8462-aced-4cd9-9ee5-629fce39aadc", "service_signature":"822001f0a13c04a245902e0c8baadb735302614722d3d39754467018eda9ce59", "timestamp":"1531724638930"}'

# Create new address belongs to patient
http POST http://localhost:5000/api/patients/7/addresses


# Get contacts belongs to patient
้http http://localhost:5000/api/patients/7/contacts X-Access-Token:{ACCESS_TOKEN}

# Create new address belongs to patients
http POST http://localhost:5000/api/patients/{patientId}/addresses X-Access-Token:

# Get all referrals
http GET http://localhost:5000/api/referrals?status=1 X-Access-Token:{ACCESS_TOKEN}

# Get all referrals waiting in the queue
http GET http://localhost:5000/api/referrals/queue X-Access-Token:{ACCESS_TOKEN}

# Get all referrals (incoming)
http GET http://localhost:5000/api/referrals/inbound X-Access-Token:{ACCESS_TOKEN}

# Get all referrals (outgoing)
http GET http://localhost:5000/api/referrals/outbound X-Access-Token:{ACCESS_TOKEN}

# Create new referral
http POST http://localhost:5000/api/referrals X-Access-Token:{ACCESS_TOKEN} \
patient_id=9 hospital_source_id=1 hospital_destination_id=2 channel_id=1 status_id=3 priority_id=2 category_id=2 patient_type_id=1

# Refer patient to another hospital
http POST http://localhost:5000/api/referrals/1/refer X-Access-Token:{ACCESS_TOKEN} hospital_destination_id=2

# Create new diagnosis
http POST http://localhost:5000/api/referrals/1/diagnoses X-Access-Token:{ACCESS_TOKEN} \
disease_text=abcd disease_code_id=1 priority_id=1 diagnosis_at=2017-10-20T03:43:48.199Z confidential_indicator=true

# Get all diagnoses belongs to referral
http GET http://localhost:5000/api/referrals/1/diagnoses X-Access-Token:{ACCESS_TOKEN}

# Get specific diagnonsis by ID
http GET http://localhost:5000/api/referrals/1/diagnoses/1 X-Access-Token:{ACCESS_TOKEN}

# Update speicific diangonsis by ID
http PUT http://localhost:5000/api/referrals/1/diagnoses/1 X-Access-Token:{ACCESS_TOKEN}

# Create new equipements

# Read referral
http put http://localhost:5000/api/referrals/26 read=true

# Login with service ID
http post http://localhost:4000/api/auth/login service_id=69f4ac0a171a5a8b501da2b8cd3015f42f670019c7a424f378_fd396d6c4e277913e744 service_key=40bb7b00-7f74-4802-853d-382e3c323b34 service_signature=267f52f176e10650a6f4275d472d5a650e12951d599b375261c2c3fbbfffc26b timestamp=1531305339789
