require('openssl')

DIGEST = OpenSSL::Digest::SHA256.new
SERVICE_ID = 'a2290b89a2901e97b4d0e5537356179393d6e224d6042d69dd4fabd403f0_53f235a23b68a6bde588'.freeze
SERVICE_KEY = '41ea8462-aced-4cd9-9ee5-629fce39aadc'.freeze
SERVICE_SECRET = '6bf2c3c3-0afa-4377-bd8e-f58b97d8c494'.freeze

def get_signature(id, key, secret, timestamp)
  message = "#{id}:#{timestamp}:#{key}"
  signature = OpenSSL::HMAC.hexdigest(DIGEST, secret, message)
  signature
end

timestamp = (Time.now.to_f * 1000).to_i
signature = get_signature(SERVICE_ID, SERVICE_KEY, SERVICE_SECRET, timestamp)
puts "ID: #{SERVICE_ID}"
puts "Key: #{SERVICE_KEY}"
puts "Secret: #{SERVICE_SECRET}"
puts "Timestamp: #{timestamp}"
puts "Signature: #{signature}"
