require 'json'
module EHHC
  class Api
    API_HOST = 'https://bkkapp.nhso.go.th'.freeze
    class << self
      def connection(url, token = nil)
        Faraday.new(url) do |conn|
          conn.adapter :net_http
          conn.headers['token-key'] = token unless token.nil?
          conn.headers['content-type'] = 'application/json'
        end
      end

      def auth(username, password)
        params = { username: username, password: password, appid: 221 }
        response = connection(API_HOST).post("/bkkapp/api/auth/login", params.to_json)
        body = JSON.parse(response.body)
        status = body['status']
        return nil if status == 'error'

        body['token_key']
      end
    end
  end
end
