module EHHC
  class CheckCommand < Command
    def call(*_, **kwargs)
      # FIXED: username and password
      params = { username: service_username, password: service_password, appid: 221 }
      login_response = connection(api_url).post("/bkkapp/api/auth/login", params.to_json)
      data = JSON.parse(login_response.body)
      token = data['token_key']
      puts "Login sucesss"

      conn = connection(api_url, token)
      process(conn, kwargs[:list])
    end

    def process(conn, items)
      items.each do |item|
        res = conn.post("/bkkapp/api/v1/EHHCService/check_approved", item.to_json)
        process_response(res, item)
      end
    end

    def process_response(res, item)
      if res.status == 200
        begin
          body = JSON.parse(res.body)
          parse_body(body, item)
        rescue JSON::ParserError
          puts res.body
          puts "Error:: Cannot parse JSON"
        end
      else
        puts "Error:: Cannot Call API"
      end
    end

    def parse_body(body, item)
      if body['status'] == 'error'
        check_command_with_error(body, item)
      else
        check_command_with_success(body['record'], item)
      end
    end

    def check_command_with_error(data, item)
      pr = ProcessResult.find_by(client_code: item.hv_client_code)

      if pr.present?
        pr.update(status: 3, status_description: merge_errors(data['message'], parse_errors(data['error_list'])))
      else
        puts "Error:: not find ProcessResult"
      end
    end

    def check_command_with_success(record, item)
      # params = OpenStruct.new(
      #   code_check: record['hv_code_check'],
      #   client_code: item.hv_client_code,
      #   hospcode: item.hospcode,
      #   status: record['ehhc_import_status'],
      #   status_description: record['import_status_desc'],
      #   sent_status: 2,
      #   sent_errors: '',
      #   assessment_id: item.ass_set_id
      # )
      pr = ProcessResult.find_by(client_code: item.hv_client_code)
      if pr.present?
        pr.update(
          code_check: record['hv_code_check'],
          status: record['ehhc_import_status'],
          status_description: record['import_status_desc'],
          sent_status: 2,
          sent_errors: ''
        )
      else
        puts "Error:: not find ProcessResult"
      end
    end

    def merge_errors(*errors)
      errors.join(',')
    end
  end
end
