require 'faraday'

module EHHC
  class Command
    attr_reader :api_url, :service_username, :service_password
    # API_URL = Rails.configuration.nhso_authentication[:url]

    def initialize
      config = Rails.configuration.nhso_authentication
      @api_url = config[:url]
      @service_username = config[:service_username]
      @service_password = config[:service_password]
    end

    def execute(*args, **kwargs)
      @before_call.call if @before_call.present?

      result = call(*args, **kwargs) if respond_to?(:call)

      @after_call.call if @after_call.present?

      result
    end

    def connection(url, token = nil)
      Faraday.new(url) do |conn|
        conn.adapter :net_http
        conn.headers['token-key'] = token unless token.nil?
        conn.headers['content-type'] = 'application/json'
      end
    end

    private

    def parse_errors(error_list)
      error_list&.map do |e|
        e['message']
      end
    end
  end
end
