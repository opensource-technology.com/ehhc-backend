module EHHC
  class ReferralError < StandardError
    def initialize(msg = 'Referral Error')
      super
    end
  end
end
