module EHHC
  module Extractor
    def self.included(klass)
      klass.extend(ClassMethods)
    end

    module ClassMethods
      def extract_patient_data(data)
        puts "extract patient data"
        data.key?('patient') ? data['patient'].reject(&Patient::IGNORE_ATTRS) : nil
      end

      def extract_contract_data(data)
        puts "extract contact"
        data.reject(&Contact::IGNORE_ATTRS)
      end

      def extract_contacts_data(data)
        puts "extract contact"
        puts data['patient']
        patient_data = data['patient']
        patient_data.key?('contacts') ? patient_data['contacts'] : nil
      end

      def extract_case_manager_data(data)
        puts "extract case manager"
        data.key?('case_manager') ? data['case_manager'].reject(&Provider::IGNORE_ATTRS) : nil
      end

      def extract_patient_allergy_data(data)
        data.reject(&PatientAllergy::IGNORE_ATTRS)
      end

      def extract_patient_allergies_data(data)
        puts "extract allergies"
        patient_data = data['patient']
        patient_data.key?('patient_allergies') ? patient_data['patient_allergies'] : []
      end

      def extract_equipment_data(data)
        data.reject(&Equipment::IGNORE_ATTRS)
      end

      def extract_visit_data(data)
        data.reject(&Visit::IGNORE_ATTRS)
      end

      def extract_referral_data(data)
        data.reject(&Referral::IGNORE_ATTRS)
      end

      def extract_provider(data, name)
        data.key?(name) && !data[name].nil? ? data[name].reject(&Provider::IGNORE_ATTRS) : nil
      end

      def extract_attending_doctor(data)
        data.key?('attending_doctor') && !data['attending_doctor'].nil? ? data['attending_doctor'].reject(&Provider::IGNORE_ATTRS) : nil
      end

      def extract_referring_doctor(data)
        data.key?('referring_doctor') && !data['referring_doctor'].nil? ? data['referring_doctor'].reject(&Provider::IGNORE_ATTRS) : nil
      end

      def extract_consulting_doctor(data)
        data.key?('consulting_doctor') && !data['consulting_doctor'].nil? ? data['consulting_doctor'].reject(&Provider::IGNORE_ATTRS) : nil
      end

      def extract_appoint_doctor(data)
        data.key?('appoint_doctor') && !data['appoint_doctor'].nil? ? data['appoint_doctor'].reject(&Provider::IGNORE_ATTRS) : nil
      end

      def extract_appointment(data)
        data.reject(&Appointment::IGNORE_ATTRS)
      end

      def extract_diagnoses(data)
        data.reject(&Diagnosis::IGNORE_ATTRS)
      end

      def extract_lab_results(data)
        data.reject(&LabResult::IGNORE_ATTRS)
      end

      def extract_procedures(data)
        data.reject(&Procedure::IGNORE_ATTRS)
      end

      def extract_treatment_orders(data)
        data.reject(&TreatmentOrder::IGNORE_ATTRS)
      end

      def extract_vital_signs(data)
        data.reject(&VitalSign::IGNORE_ATTRS)
      end
    end
  end
end
