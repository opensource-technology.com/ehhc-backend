module EHHC
  class Generator
    class << self
      def set_id
        time = Time.now.strftime('%Y%m%d%H%M%S%L')
        suffix = rand 100..999
        "#{hospcode}#{time}#{suffix}"
      end

      def dummy_patient_id
        num = (rand 1_000...1_000_000).to_s.rjust(6, '0')
        "ZZ#{hospcode}#{num}"
      end

      def hospcode
        Site.first.code
      end
    end
  end
end
