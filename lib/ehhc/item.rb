require 'faker'

module EHHC
  class Item
    attr_reader :hospcode, :ass_set_id, :hv_client_code, :pid, :type

    # Item.new
    # @param [Assessment]
    def initialize(instance)
      extract(instance)
    end

    def to_data
      [
        @adl_score,
        @age_mm,
        @age_year,
        @ass_set_id,
        @assessment_date,
        @assessment_create_date,
        @assessor_first_name,
        @assessor_id,
        @assessor_last_name,
        @assessor_position,
        @disease_code,
        @disease_text,
        @birth_date,
        @category,
        @catm,
        @first_name,
        @hcode_destination,
        @hospcode,
        @house,
        @hv_client_code,
        @last_name,
        @patient_type,
        @phonenumber,
        @pid,
        @postcode,
        @road,
        @sex,
        @soimain,
        @status,
        @villaname,
        @sbp,
        @dbp,
        @btemp,
        @pr,
        @rr,
        @weight,
        @height,
        @physical_dtx
      ]
    end

    private

    # Get age year and age month from birthdate
    # @param [date]
    def age(dob)
      now = Time.now.utc.to_date
      year = now.year - dob.year

      month = if now.month > dob.month
                now.month - dob.month
              else
                12 - (dob.month - now.month)
              end
      [year, month]
    end

    def date_format(date)
      return "-" if date.nil?

      date.strftime('%Y%m%d')
    end

    def datetime_format(date)
      return "-" if date.nil?

      date.strftime("%Y%m%d%H%M%S")
    end

    # Generate HC Clicnet Code for reference in the response
    def generate_hv_client_code(hospcode)
      timestamp = (Time.now.to_f * 100000).to_i
      "#{hospcode}#{timestamp}"
    end

    # Get SBP and DBP
    def sbp_and_dbp(blood_pressure)
      return [0, 0] if blood_pressure.nil?

      blood_pressure.split('/')
    end

    # Extract information from assessment
    # @param [Assesment]
    def extract(instance)
      if instance.instance_of?(ProcessResult)
        extract_on_process_result(instance)
      else
        extract_on_assessment(instance)
      end
    end

    def extract_on_assessment(instance)
      # assessment
      extract_assessment(instance)

      # referral
      extract_referral(instance.referral)

      # preventive_healht_measurement
      extract_preventive_health_measurement(instance.preventive_health_measure)
    end

    def extract_on_process_result(instance)
      assessment = Assessment.find_by(process_result_id: instance.id)
      @hospcode = instance.hospcode
      @ass_set_id = assessment&.set_id
      @hv_client_code = instance.client_code
      @hv_code_check = instance.code_check
    end

    def extract_assessment(assessment)
      @assessment_date = date_format(assessment.assessment_at)
      @assessment_create_date = datetime_format(assessment.updated_at)
      @ass_set_id = assessment.set_id

      assessor = Provider.find_by(set_id: assessment.assessor)
      extract_assessor(assessor)
    end

    def extract_assessor(assessor)
      return if assessor.nil?

      @assessor_id = assessor.provider_identifier
      @assessor_first_name = assessor.first_name
      @assessor_last_name = assessor.last_name
      @assessor_position = assessor.position
    end

    def extract_referral(referral)
      @hospcode = referral.hospital_source
      @hv_client_code = generate_hv_client_code(referral.hospital_source)
      @hcode_destination = referral.hospital_destination
      @category = referral.category
      @status = referral.status
      @patient_type = referral.patient_type
      @adl_score = referral.adl_score
      # visit
      extract_visit(referral.visits[0])
      # patient
      extract_patient(referral.patient)
    end

    # Extract information from patient
    # @param [Patient, patient cannot be null
    def extract_patient(patient)
      @pid = patient.patient_id_list
      @first_name = patient.first_name
      @last_name = patient.last_name
      if patient.birthdate
        @birth_date = date_format(patient.birthdate)
        @age_year, @age_mm = age(patient.birthdate)
      else
        @birth_date = ''
        @age_year = 0
        @age_mm = 0
      end
      @sex = patient.sex
      @villaname = patient.address_muban || '-'
      @house = patient.current_address_house || '-'
      @soimain = patient.current_address_soi || '-'
      @road = patient.current_address_road || '-'
      @catm = patient.current_address_tambon ? "#{patient.current_address_tambon}00" : '000000'
      @postcode = patient.current_address_postcode
      @phonenumber = patient.phone_number_mobile
    end

    def extract_preventive_health_measurement(ph_measurement)
      return if ph_measurement.nil?

      @sbp, @dbp = sbp_and_dbp(ph_measurement.blood_pressure)
      @btemp = ph_measurement.temperature
      @pr = ph_measurement.pulse
      @rr = ph_measurement.heart_rate
      @weight = ph_measurement.weight
      @height = ph_measurement.height
      @physical_dtx = ph_measurement.dtx
    end

    def extract_visit(visit)
      return if visit.nil? || visit.diagnoses[0].nil?

      diagnose = visit.diagnoses[0]
      @disease_code = diagnose.disease_code
      @disease_text = diagnose.disease_text
    end
  end
end
