require 'jwt'

# Handle JsonWebToken, encode and decode token
module EHHC
  class JsonWebToken
    class << self
      def encode(payload, password = nil, exp = 4.hours.from_now)
        payload[:exp] = exp.to_i
        token = JWT.encode payload, password
        [token, exp]
      end

      def decode(token, password = nil)
        payload = JWT.decode token, password
        payload
      end

      def get_payload(token)
        payload = JWT.decode token, nil, false
        payload[0]
      end
    end
  end
end
