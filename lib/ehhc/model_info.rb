module EHHC
  module ModelInfo
    ATTRS = {
      except: %i[created_at updated_at]
    }.freeze
  end
end
