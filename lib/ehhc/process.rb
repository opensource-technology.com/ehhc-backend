module EHHC
  module ProcessLoadData
    # Load assessment from yesterday
    def load_assessment
      Assessment.where(process_result_id: nil, ready: true).order(created_at: :asc).limit(2000)
    end

    def load_rest_assessment

    end

    # Load process_result from yesterday
    def load_process_result
      ProcessResult.where(status: 1, sent_status: 2).order(created_at: :asc).limit(2000)
    end
  end
end
