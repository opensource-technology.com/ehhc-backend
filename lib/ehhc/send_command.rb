module EHHC
  class SendCommand < Command
    def call(*_, **kwargs)
      if ENV.key?('FIXED_TOKEN')
        token = ENV['FIXED_TOKEN']
      else
        params = { username: service_username, password: service_password, appid: 11 }
        login_res = Faraday.post("#{api_url}/auth/login", params.to_json)
        data = JSON.parse(login_res.body)
        token = data['token_key']
        EventLog.info("AUTH", "Authenticate with #{service_username} and received token #{token}")
      end

      return if token.nil?

      connection = Faraday.new("#{api_url}/v1/HVService/send_data") do |conn|
        conn.adapter :net_http
        conn.headers['token-key'] = token
        conn.headers['content-type'] = 'application/json'
      end

      # TODO: Load all here
      item_list = kwargs[:list]
      item_list.each do |item|
        res = connection.post("#{api_url}/v1/HVService/send_data", item.to_json)
        process_response(res, item)
      end
    end

    def process_response(res, item)
      if res.status == 200
        body = JSON.parse(res.body)
        parse_body(body, item)
      else
        send_command_failed(item)
      end
    rescue JSON::ParserError
      puts $ERROR_INFO
      EventLog.info("ERROR", "Cannot parse json from #{item.surveytype}-#{item.hv_client_code}")
    end

    def parse_body(body, item)
      if body['status'] == 'error'
        send_command_error(body, item)
      else
        send_command_success(body['record'], item)
      end
    end

    def send_command_error(data, item)
      pr = ProcessResult.find_by(client_code: item.hv_client_code)
      # check_reference(data['record'], item) if data.key?('record') # create check mulitple time

      if pr.present?
        pr.update(sent_status: 3, sent_errors: merge_errors(data['message'], parse_errors(data['error_list']))) if pr.sent_status != 2
      else
        params = OpenStruct.new code_check: data['hv_code_check'],
                               client_code: item.hv_client_code, hospcode: item.hospcode, sent_status: 3,
                               assessment_id: item.ass_set_id,
                               sent_errors: merge_errors(data['message'], parse_errors(data['error_list']))
        create_process_result(params)
      end
      EventLog.info("SEND", "Send data to cloud success with error. [type: #{item.surveytype}, client_code: #{item.hv_client_code}, errors: #{merge_errors(data['message'], parse_errors(data['error_list']))}]")
    end

    def send_command_failed(item)
      # params = OpenStruct.new(
      #   code_check: record['hv_code_check'],
      #   client_code: item.hv_client_code,
      #   hospcode: item.hospcode,
      #   sent_status: 3,
      #   sent_errors: 'Cannot connect server',
      #   assessment_id: item.ass_set_id
      # )
      # create_process_result(params)
      EventLog.info("ERROR", "Cannot parse json from #{item.hv_client_code}")
    end

    def send_command_success(record, item)
      params = OpenStruct.new(
        code_check: record['hv_code_check'],
        client_code: item.hv_client_code,
        hospcode: item.hospcode,
        status: record['ehhc_import_status'],
        status_description: record['import_status_desc'],
        sent_status: 2,
        sent_errors: '',
        assessment_id: item.ass_set_id
      )
      pr = ProcessResult.find_by(client_code: item.hv_client_code)
      if pr.present?
        pr.update(
          code_check: record['hv_code_check'],
          status: record['ehhc_import_status'],
          status_description: record['import_status_desc'],
          sent_status: 2,
          sent_errors: ''
        )
      else
        create_process_result(params)
      end
      EventLog.info("SEND", "Send data to cloud sucess. [client_code: #{params.client_code}, status: #{record['ehhc_import_status']}]")
    end

    def create_process_result(params)
      # p params
      pr = ProcessResult.create(params.to_h.except(:assessment_id))
      # p pr.errors
      update_reference(pr, params.assessment_id)
    end

    def update_reference(process_result, set_id)
      instance = Assessment.find_by(set_id: set_id)
      return if instance.nil?

      instance.process_result = process_result
      instance.save(touch: false)
    end

    def check_reference(record, item)
      hv_code_check = record['hv_code_check']

      instance = Assessment.find_by(set_id: item.ass_set_id)
      # ค้นหา assessment
      if instance.process_result.present?
        # ถ้าพบ process_result ใน assessment ให้ทำการอัพเดตค่า ในกรณีที่ hv_code_check เป็น null
        pr.update(code_check: hv_code_check, sent_status: 2, sent_errors: '') if pr.code_check.nil?
      else
        # TODO: Create process result
        params = OpenStruct.new code_check: hv_code_check, assessment_id: item.ass_set_id,
                                client_code: item.hv_client_code, hospcode: item.hospcode, status: 1,
                                status_description: 'wait for process', sent_status: 2, sent_errors: ''
        create_process_result(params)
      end
    end

    def merge_errors(*errors)
      errors.join(',')
    end
  end
end
