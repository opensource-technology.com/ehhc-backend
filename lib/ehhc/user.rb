module EHHC
  class User
    attr_accessor :username, :first_name, :user_id, :hospcode, :role, :token, :grants

    def initialize(user_id, username, token, hospcode, role, grants)
      self.user_id = user_id
      self.username = username
      self.first_name = username
      self.token = token
      self.hospcode = hospcode
      self.role = role
      self.grants = grants
    end

    def role?(role)
      self.role.to_sym == role
    end

    def admin?
      role.to_sym == :admin
    end

    def advanced_user?
      role.to_sym == :advanced
    end

    def user?
      role.to_sym == :user
    end
  end
end
