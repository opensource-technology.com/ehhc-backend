def create_dummy_visit
  referrals = Referral.all
  referrals.each do |r|
    Visit.create(referral_id: r.id) if r.visits.count.zero?
  end
end

require 'base64'
require 'date'
require 'fileutils'
require 'securerandom'

module EHHC
  class Utils
    def self.generate_set_id(hospital)
      time = Time.now.strftime('%Y%m%d%H%M%S%L')
      suffix = rand 100..999
      hospital + time + suffix.to_s
    end

    def self.generate_dummy_patient_id(hospital)

      num = (rand 1_000...1_000_000).to_s.rjust(6, '0')
      "ZZ#{hospital}#{num}"
    end
  end
end
