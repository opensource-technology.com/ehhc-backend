class ObjResult
  attr_accessor :count_refers, :count_send_nhso, :count_waiting_sent, :total_count,
                :count_patients, :process_date, :count_assessment,
                :pid, :pt_name, :pt_type, :send_date, :record_date, :refer_out,
                :provider, :department, :count_pass, :count_fail, :assessment_date,
                :sent_status, :status_description, :inspection_date, :status, :sent_errors,
                :count_wait, :hos_code, :hos_name
  def initialize(result)
    @count_refers = result[:count_refers]
    @count_send_nhso = result[:count_send_nhso]
    @count_waiting_sent = result[:count_waiting_sent]
    @total_count = result[:total_count]
    @count_patients = result[:count_patients]
    @process_date = result[:process_date]
    @count_assessment = result[:count_assessment]
    @pid = result[:pid]
    @pt_name = result[:pt_name]
    @pt_type = result[:pt_type]
    @send_date = result[:send_date]
    @record_date = result[:record_date]
    @refer_out = result[:refer_out]
    @provider = result[:provider]
    @department = result[:department]
    @count_pass = result[:count_pass]
    @count_fail = result[:count_fail]
    @assessment_date = result[:assessment_date]
    @status = result[:status]
    @sent_status = result[:sent_status]
    @status_description = result[:status_description]
    @inspection_date = result[:inspection_date]
    @sent_errors = result[:sent_errors]
    @count_wait = result[:count_wait]
    @hos_code = result[:hos_code]
    @hos_name = result[:hos_name]
  end
end
