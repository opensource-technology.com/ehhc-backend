select 
    sum(q.total_count)::int as total_count
from (
select
    count(distinct assessments.id) as total_count
from assessments
    inner join referrals on referrals.id = assessments.referral_id
    inner join master.hospitals on hospitals.code = referrals.hospital_source
    left join process_results on process_results.id = assessments.process_result_id
where (assessments.{date_type} at time zone 'utc' at time zone 'Asia/bangkok')::date between '{from_date}' AND '{to_date}'
    and (case when '{htype}' = 0 then hospitals.htype not in (1,3,4) else hospitals.htype = '{htype}' end)
    and (case when '{hospital}' = '' or '{hospital}' is null  or '{hospital}' = 'all' then true else referrals.hospital_source = '{hospital}' end)
    and (case when '{destination}' = 'accept' then (case when '{hospital}' = '' or '{hospital}' is null  or '{hospital}' = 'all' then true else referrals.hospital_destination = '{hospital}' end)
              when '{destination}' = 'site' 
                        then (case when '{hospital_destination}' = '' or '{hospital_destination}' is null or '{hospital_destination}' = 'all'
                                        then referrals.hospital_destination <> '{hospital}'
                                    else referrals.hospital_destination = '{hospital_destination}' end)
                end)
    and (case when '{case_manager}' = '' or '{case_manager}' is null or '{case_manager}' = 'all' then true else assessments.assessor::text = '{case_manager}' end) 

union all 

select
    count(distinct assessments.id) as total_count
from assessments
    inner join referrals on referrals.id = assessments.referral_id
    inner join master.hospitals on hospitals.code = referrals.hospital_destination
    left join process_results on process_results.id = assessments.process_result_id
where (assessments.{date_type} at time zone 'utc' at time zone 'Asia/bangkok')::date between '{from_date}' AND '{to_date}'
    and (case when '{htype}' = 0 then hospitals.htype not in (1,3,4) else hospitals.htype = '{htype}' end)
    and (case when '{hospital}' = '' or '{hospital}' is null  or '{hospital}' = 'all' then true else referrals.hospital_destination = '{hospital}' end)
    and (case when '{destination}' = 'site' 
                        then (case when '{hospital_destination}' = '' or '{hospital_destination}' is null or '{hospital_destination}' = 'all'
                                        then referrals.hospital_source <> '{hospital}'
                                    else referrals.hospital_source = '{hospital_destination}' end)
                end)
    and (case when '{case_manager}' = '' or '{case_manager}' is null or '{case_manager}' = 'all' then true else assessments.assessor::text = '{case_manager}' end)
)q