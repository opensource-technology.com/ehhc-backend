select 
    q.*
    ,q.assessment_at
    ,q.updated_at
from(
select
    patients.patient_id_list as pid
    ,prenames.label || patients.first_name ||' '|| patients.last_name as pt_name
    ,r_patient_types.label as pt_type
    ,(referrals.effective_at at time zone 'utc' at time zone 'Asia/bangkok')::date as send_date
    ,(assessments.assessment_at at time zone 'utc' at time zone 'Asia/bangkok')::date as assessment_date
    ,(assessments.updated_at at time zone 'utc' at time zone 'Asia/bangkok')::date as record_date
    ,assessments.assessment_at
    ,assessments.updated_at
    ,hos_source.label as refer_source
    ,hos_destination.label as refer_out
    ,providers.provider_name as provider
    ,providers.department
    ,process_results.status
    ,process_results.sent_status
    ,process_results.sent_errors
    ,(process_results.updated_at at time zone 'utc' at time zone 'Asia/bangkok')::date as inspection_date 
    ,process_results.status_description
from assessments
    inner join referrals on referrals.id = assessments.referral_id
    left join process_results on process_results.id = assessments.process_result_id
    inner join patients on patients.id = referrals.patient_id
    left join master.prenames on prenames.code = patients.prename
    left join master.hospitals as hos_destination on hos_destination.code = referrals.hospital_destination
    left join master.hospitals as hos_source on hos_source.code = referrals.hospital_source
    left join (select providers.id
                    ,providers.set_id
                    ,providers.first_name ||' '|| providers.last_name as provider_name
                    ,providers.department
                from providers
                    inner join (select set_id
                                    ,max(updated_at) as max_update
                                from providers
                                group by set_id
                                ) max_providers on max_providers.set_id = providers.set_id
                                    and max_providers.max_update = providers.updated_at
                ) providers on providers.set_id = assessments.assessor
    left join r_patient_types on r_patient_types.id::text = assessments.patient_type
where (assessments.{date_type} at time zone 'utc' at time zone 'Asia/bangkok')::date between '{from_date}' AND '{to_date}'
    and (case when '{htype}' = 0 then hos_source.htype not in (1,3,4) else hos_source.htype = '{htype}' end)
    and (case when '{hospital}' = '' or '{hospital}' is null  or '{hospital}' = 'all' then true else referrals.hospital_source = '{hospital}' end)
    and (case when '{destination}' = 'accept' then (case when '{hospital}' = '' or '{hospital}' is null  or '{hospital}' = 'all' then true else referrals.hospital_destination = '{hospital}' end)
              when '{destination}' = 'site' 
                        then (case when '{hospital_destination}' = '' or '{hospital_destination}' is null or '{hospital_destination}' = 'all'
                                        then referrals.hospital_destination <> '{hospital}'
                                    else referrals.hospital_destination = '{hospital_destination}' end)
                end)
    and (case when '{case_manager}' = '' or '{case_manager}' is null or '{case_manager}' = 'all' then true else assessments.assessor::text = '{case_manager}' end) 
    and process_results.status = 3 
group by patients.patient_id_list
    ,prenames.label
    ,patients.first_name
    ,patients.last_name
    ,r_patient_types.label
    ,referrals.effective_at
    ,assessments.assessment_at
    ,assessments.updated_at
    ,hos_source.label
    ,hos_destination.label
    ,providers.provider_name
    ,providers.department
    ,process_results.status
    ,process_results.sent_status
    ,process_results.sent_errors
    ,process_results.updated_at
    ,process_results.status_description

union all

select
    patients.patient_id_list as pid
    ,prenames.label || patients.first_name ||' '|| patients.last_name as pt_name
    ,r_patient_types.label as pt_type
    ,(referrals.effective_at at time zone 'utc' at time zone 'Asia/bangkok')::date as send_date
    ,(assessments.assessment_at at time zone 'utc' at time zone 'Asia/bangkok')::date as assessment_date
    ,(assessments.updated_at at time zone 'utc' at time zone 'Asia/bangkok')::date as record_date
    ,assessments.assessment_at
    ,assessments.updated_at
    ,hos_source.label as refer_source
    ,hos_destination.label as refer_out
    ,providers.provider_name as provider
    ,providers.department
    ,process_results.status
    ,process_results.sent_status
    ,process_results.sent_errors
    ,(process_results.updated_at at time zone 'utc' at time zone 'Asia/bangkok')::date as inspection_date 
    ,process_results.status_description
from assessments
    inner join referrals on referrals.id = assessments.referral_id
    left join process_results on process_results.id = assessments.process_result_id
    inner join patients on patients.id = referrals.patient_id
    left join master.prenames on prenames.code = patients.prename
    left join master.hospitals as hos_destination on hos_destination.code = referrals.hospital_destination
    left join master.hospitals as hos_source on hos_source.code = referrals.hospital_source
    left join (select providers.id
                    ,providers.set_id
                    ,providers.first_name ||' '|| providers.last_name as provider_name
                    ,providers.department
                from providers
                    inner join (select set_id
                                    ,max(updated_at) as max_update
                                from providers
                                group by set_id
                                ) max_providers on max_providers.set_id = providers.set_id
                                    and max_providers.max_update = providers.updated_at
                ) providers on providers.set_id = assessments.assessor
    left join r_patient_types on r_patient_types.id::text = assessments.patient_type
where (assessments.{date_type} at time zone 'utc' at time zone 'Asia/bangkok')::date between '{from_date}' AND '{to_date}'
    and (case when '{htype}' = 0 then hos_destination.htype not in (1,3,4) else hos_destination.htype = '{htype}' end)
    and (case when '{hospital}' = '' or '{hospital}' is null  or '{hospital}' = 'all' then true else referrals.hospital_destination = '{hospital}' end)
    and (case when '{destination}' = 'site' 
                        then (case when '{hospital_destination}' = '' or '{hospital_destination}' is null or '{hospital_destination}' = 'all'
                                        then referrals.hospital_source <> '{hospital}'
                                    else referrals.hospital_source = '{hospital_destination}' end)
                end)
    and (case when '{case_manager}' = '' or '{case_manager}' is null or '{case_manager}' = 'all' then true else assessments.assessor::text = '{case_manager}' end) 
    and process_results.status = 3 
group by patients.patient_id_list
    ,prenames.label
    ,patients.first_name
    ,patients.last_name
    ,r_patient_types.label
    ,referrals.effective_at
    ,assessments.assessment_at
    ,assessments.updated_at
    ,hos_source.label
    ,hos_destination.label
    ,providers.provider_name
    ,providers.department
    ,process_results.status
    ,process_results.sent_status
    ,process_results.sent_errors
    ,process_results.updated_at
    ,process_results.status_description
)q
order by {date_type}