select
    count(q1.hos_code) as total_count
from (
    select
        q.hos_code
        ,q.hos_name
        ,sum(q.count_patients) as count_patients
        ,sum(q.count_assessment) as count_assessment
        ,sum(q.count_send_nhso) as count_send_nhso
        ,sum(q.count_waiting_sent) as count_waiting_sent
        ,sum(q.count_wait) as count_wait
        ,sum(q.count_pass) as count_pass
        ,sum(q.count_fail) as count_fail
    from (
    select
        hospitals.code as hos_code
        ,hospitals.label as hos_name
        ,count(distinct referrals.patient_id) as count_patients
        ,count(distinct assessments.id) as count_assessment
        ,count(distinct case when process_results.status in ('1','2','3') then assessments.id end) as count_send_nhso
        ,count(distinct case when process_results.status is null then assessments.id end) as count_waiting_sent 
        ,count(distinct case when process_results.status = 1 then assessments.id end) as count_wait
        ,count(distinct case when process_results.status = 2 then assessments.id end) as count_pass
        ,count(distinct case when process_results.status = 3 then assessments.id end) as count_fail

    from assessments
        inner join referrals on referrals.id = assessments.referral_id
        inner join master.hospitals on hospitals.code = referrals.hospital_source
        left join process_results on process_results.id = assessments.process_result_id

    where (assessments.{date_type} at time zone 'utc' at time zone 'Asia/bangkok')::date between '{from_date}' AND '{to_date}'
        and (case when '{htype}' = 0 then hospitals.htype not in (1,3,4) else hospitals.htype = '{htype}' end)
        and (case when '{hospital}' = '' or '{hospital}' is null or '{hospital}' = 'all' 
                            then referrals.hospital_source = hospitals.code  
                        else referrals.hospital_source = '{hospital}' end)
        and (case when '{destination}' = 'accept' 
                            then (case when '{hospital}' = '' or '{hospital}' is null or '{hospital}' = 'all' 
                                                then referrals.hospital_destination = hospitals.code 
                                        else referrals.hospital_destination = '{hospital}' end)
                  when '{destination}' = 'site' 
                            then (case when '{hospital_destination}' = '' or '{hospital_destination}' is null or '{hospital_destination}' = 'all' 
                                            then referrals.hospital_destination <> hospitals.code
                                        else referrals.hospital_destination = '{hospital_destination}' end)
                    end)
        and (case when '{case_manager}' = '' or '{case_manager}' is null or '{case_manager}' = 'all' then true else assessments.assessor::text = '{case_manager}' end)   

    group by hospitals.code
        ,hospitals.label

    union all

    select
        hospitals.code as hos_code
        ,hospitals.label as hos_name
        ,count(distinct referrals.patient_id) as count_patients
        ,count(distinct assessments.id) as count_assessment
        ,count(distinct case when process_results.status in ('1','2','3') then assessments.id end) as count_send_nhso
        ,count(distinct case when process_results.status is null then assessments.id end) as count_waiting_sent 
        ,count(distinct case when process_results.status = 1 then assessments.id end) as count_wait
        ,count(distinct case when process_results.status = 2 then assessments.id end) as count_pass
        ,count(distinct case when process_results.status = 3 then assessments.id end) as count_fail

    from assessments
        inner join referrals on referrals.id = assessments.referral_id
        inner join master.hospitals on hospitals.code = referrals.hospital_destination
        left join process_results on process_results.id = assessments.process_result_id

    where (assessments.{date_type} at time zone 'utc' at time zone 'Asia/bangkok')::date between '{from_date}' AND '{to_date}'
        and (case when '{htype}' = 0 then hospitals.htype not in (1,3,4) else hospitals.htype = '{htype}' end)
        and (case when '{hospital}' = '' or '{hospital}' is null or '{hospital}' = 'all' 
                            then referrals.hospital_destination = hospitals.code  
                        else referrals.hospital_destination = '{hospital}' end)
        and (case when '{destination}' = 'site' 
                            then (case when '{hospital_destination}' = '' or '{hospital_destination}' is null or '{hospital_destination}' = 'all' 
                                            then referrals.hospital_source <> hospitals.code 
                                        else referrals.hospital_source = '{hospital_destination}' end)
                    end)
        and (case when '{case_manager}' = '' or '{case_manager}' is null or '{case_manager}' = 'all' then true else assessments.assessor::text = '{case_manager}' end) 

    group by hospitals.code
        ,hospitals.label
    )q
    group by q.hos_code
        ,q.hos_name
)q1