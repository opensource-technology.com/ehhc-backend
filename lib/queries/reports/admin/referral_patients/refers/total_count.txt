select
    count(distinct referrals.id) as total_count
from referrals
    inner join master.hospitals on hospitals.code = referrals.hospital_source
where (referrals.{date_type} at time zone 'utc' at time zone 'Asia/bangkok')::date between '{from_date}' AND '{to_date}'
    and (case when '{htype}' = 0 then hospitals.htype not in (1,3,4) else hospitals.htype = '{htype}' end)
    and (case when '{hospital}' = '' or '{hospital}' is null or '{hospital}' = 'all' then true else referrals.hospital_source = '{hospital}' end)
    and (case when '{destination}' = 'accept' then (case when '{hospital}' = '' or '{hospital}' is null  or '{hospital}' = 'all' then true else referrals.hospital_destination = '{hospital}' end)
              when '{destination}' = 'site' 
                        then (case when '{hospital_destination}' = '' or '{hospital_destination}' is null or '{hospital_destination}' = 'all'
                                        then referrals.hospital_destination <> '{hospital}'
                                    else referrals.hospital_destination = '{hospital_destination}' end)
                end)
    and (case when '{case_manager}' = '' or '{case_manager}' is null or '{case_manager}' = 'all' then true else referrals.case_manager::text = '{case_manager}' end) 