select
    0 as total_count
from referrals   
where referrals.{date_type}::date between '{from_date}' AND '{to_date}'
    and referrals.hospital_source = '{hospital_code}'
    and (case when '{hospital}' = '' or '{hospital}' is null then referrals.hospital_destination = '{hospital_code}' else referrals.hospital_destination = '{hospital}' end)
    and (case when '{case_manager}' = '' or '{case_manager}' is null then true else referrals.case_manager = '{case_manager}' end)  