select
    '' as pid
    ,'' as pt_name
    ,'' as pt_type
    ,'' as send_date
    ,'' as record_date
    ,'' as refer_out
    ,'' as provider
    ,'' as department

from referrals
    inner join patients on patients.id = referrals.patient_id
    left join master.prenames on prenames.code = patients.prename
    left join master.hospitals on hospitals.code = referrals.hospital_destination
    left join providers on providers.set_id = referrals.case_manager
    left join r_patient_types on r_patient_types.id::text = referrals.patient_type

where referrals.{date_type}::date between '{from_date}' AND '{to_date}'
    and referrals.hospital_source = '{hospital}'
    and (case when '{hospital_destination}' = '' or '{hospital_destination}' is null
                then referrals.hospital_destination = '{hospital}' else referrals.hospital_destination = '{hospital_destination}' end)
    and (case when '{case_manager}' = '' or '{case_manager}' is null then true else referrals.case_manager = '{case_manager}' end) 
order by referrals.{date_type} 