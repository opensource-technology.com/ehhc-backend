require 'ehhc'

def seconds_to_hms(sec)
  [sec / 3600, sec / 60 % 60, sec % 60].map { |t| t.to_s.rjust(2, '0') }.join(':')
end

start_time = Time.now
# range_date = (18...22).map do |i|
#   "2019-09-#{i.to_s.rjust(2, '0')}"..."2019-09-#{(i + 1).to_s.rjust(2, '0')}"
# end
(1..100).each do |_|
  assessments = Assessment.where(process_result: nil).limit(100)

  # range_date.each do |range|
  # hlist = PersonHealthSurvey.where(datesurvey: range, process_result: nil)
  # hlist = HomeVisit.where(datesurvey: range, process_result: nil)

  items = assessments.map { |a| EHHC::Item.new a }
  c = EHHC::SendCommand.new
  EHHC::Commander.call c, list: items
end
end_time = Time.now

wait_time = end_time - start_time
puts "Wait: #{seconds_to_hms(wait_time.to_i)}"
