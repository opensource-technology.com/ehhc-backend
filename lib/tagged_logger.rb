# require 'active_support/tagged_logging'
class TaggedLogger
  class << self
    attr_accessor :logger, :tagged
    delegate :info, :warn, :debug, :error, to: :logger
  end

  def initialize(logger, tagged = nil)
    @logger = logger
    @tagged = tagged
    @logger.formatter = Logger::Formatter.new
    @logger = ActiveSupport::TaggedLogging.new @logger
  end

  # Define log
  %i[info warn debug error].each do |level|
    define_method(level) do |message, tagged = nil|
      @logger.tagged([*tagged, *@tagged].compact) { @logger.send(level, message) }
    end
  end
end
