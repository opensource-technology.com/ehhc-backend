require 'yaml'

namespace :app do
  desc "Auto increase build number"
  task auto_build_number: :environment do
    version = YAML.safe_load(File.read('config/version.yml'))
    version['patch'] = version['patch'].to_i + 1
    version['build'] = `git rev-list HEAD | wc -l`.strip.to_i
    version['hash'] = `git rev-parse --short HEAD`.strip.to_s
    File.open("config/version.yml", "w") { |file| file.write(version.to_yaml.gsub("---\n", '')) }
    puts "New build number is #{version['build']}"
  end

  desc "Get current build number"
  task build_number: :environment do
    version = YAML.safe_load(File.read('config/version.yml'))
  end
end
