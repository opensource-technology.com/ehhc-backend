namespace :deploy do
  desc "Create systemd config"
  task systemd: :environment do
    CONFIG = <<~PUMA.freeze
      [Unit]
      Description=Puma HTTP Server
      After=network.target

      # Uncomment for socket activation (see below)
      # Requires=puma.socket

      [Service]
      # Foreground process (do not use --daemon in ExecStart or config.rb)
      Type=simple

      # Preferably configure a non-privileged user
      User=#{`whoami`}

      # The path to the puma application root
      # Also replace the "<WD>" place holders below with this path.
      WorkingDirectory=#{Dir.pwd}

      # Helpful for debugging socket activation, etc.
      Environment=RAILS_ENV=production
      EnvironmentFile=#{Dir.pwd}/config/app.env

      # The command to start Puma. This variant uses a binstub generated via
      # `bundle binstubs puma --path ./sbin` in the WorkingDirectory
      # (replace "<WD>" below)
      ExecStart=#{`rbenv root`.chomp}/shims/bundle exec puma -C #{Dir.pwd}/config/puma.rb
      ExecStop=#{`rbenv root`.chomp}/shims/bundle exec pumactl -S #{Dir.pwd}/tmp/pids/puma.pid stop
      PIDFile=#{Dir.pwd}/tmp/pids/puma.pid

      # Variant: Use config file with `bind` directives instead:
      # ExecStart=<WD>/sbin/puma -C config.rb
      # Variant: Use `bundle exec --keep-file-descriptors puma` instead of binstub

      [Install]
      WantedBy=multi-user.target
    PUMA

    File.open("#{Dir.pwd}/tmp/puma.service", 'w', 0o655) do |f|
      f.write(CONFIG)
      f.flush
    end

    copy('puma')
  end

  desc "Create sidekiq config"
  task sidekiq: :environment do
    SIDEKIQ = <<~FILE.freeze
      #
      # systemd unit file for CentOS 7+, Ubuntu 15.04+
      #
      # Customize this file based on your bundler location, app directory, etc.
      # Put this in /usr/lib/systemd/system (CentOS) or /lib/systemd/system (Ubuntu).
      # Run:
      #   - systemctl enable sidekiq
      #   - systemctl {start,stop,restart,reload} sidekiq
      #
      # This file corresponds to a single Sidekiq process.  Add multiple copies
      # to run multiple processes (sidekiq-1, sidekiq-2, etc).
      #
      [Unit]
      Description=sidekiq
      # start us only once the network and logging subsystems are available,
      # consider adding redis-server.service if Redis is local and systemd-managed.
      After=syslog.target network.target

      # See these pages for lots of options:
      #
      #   https://www.freedesktop.org/software/systemd/man/systemd.service.html
      #   https://www.freedesktop.org/software/systemd/man/systemd.exec.html
      #
      # THOSE PAGES ARE CRITICAL FOR ANY LINUX DEVOPS WORK; read them multiple
      # times! systemd is a critical tool for all developers to know and understand.
      #
      [Service]
      Type=simple
      WorkingDirectory=#{Dir.pwd}
      # If you use rbenv:
      # ExecStart=#{`rbenv root`.chomp}/shims/bundle exec sidekiq -e production
      # If you use the system's ruby:
      ExecStart=#{`rbenv root`.chomp}/shims/bundle exec sidekiq -e production

      # use `systemctl reload sidekiq` to send the quiet signal to Sidekiq
      # at the start of your deploy process.
      # ExecReload=/usr/bin/kill -TSTP $MAINPID

      # Greatly reduce Ruby memory fragmentation and heap usage
      # https://www.mikeperham.com/2018/04/25/taming-rails-memory-bloat/
      Environment=MALLOC_ARENA_MAX=2

      # if we crash, restart
      RestartSec=1
      Restart=on-failure

      # output goes to /var/log/syslog
      StandardOutput=syslog
      StandardError=syslog

      # This will default to "bundler" if we don't specify it
      SyslogIdentifier=sidekiq

      [Install]
      WantedBy=multi-user.target
    FILE

    File.open("#{Dir.pwd}/tmp/sidekiq.service", 'w', 0o655) do |f|
      f.write(SIDEKIQ)
      f.flush
    end

    copy('sidekiq')
  end

  def copy(file_name)
    `sudo cp #{Dir.pwd}/tmp/#{file_name}.service /etc/systemd/system`
    `rm #{Dir.pwd}/tmp/#{file_name}.service`
    `sudo systemctl enable #{file_name}.service`
    puts "Copied #{file_name}.service to /etc/systemd/system success."
  end
end
