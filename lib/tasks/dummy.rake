require 'ehhc/api'

namespace :dummy do
  desc "Generate user token"
  task :token, [:username, :password] do |_, args|
    puts "Login with username #{args.username}"
    token = EHHC::Api.auth(args.username, args.password)
    puts "Token: #{token}"
  end
end
