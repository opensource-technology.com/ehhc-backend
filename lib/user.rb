class User
  attr_accessor :username, :password

  def initialize(username: nil, password: nil)
    @username = username
    @password = password
  end
end