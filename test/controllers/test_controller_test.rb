require 'test_helper'

class TestControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get test_url, params: { status: ['I'], q: 'abc' }
    assert_response :success
  end

  test "should get index 2" do
    get test_url, params: { status: ['I'], q: 'abc', start_date: '2019-08-01', end_date: '2019-08-30' }
    assert_response :success
  end
end
