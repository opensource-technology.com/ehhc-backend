require 'test_helper'
require 'ehhc'

class EHHC::EhhcLibTest < ActionDispatch::IntegrationTest
  def initialize(*args, &blk)
    super(*args, &blk)
  end

  test 'should send assessment success' do
    ActiveRecord::Base.transaction do
      assessment = send_data assessment_dup
      assert_equal assessment&.process_result&.status, 1, "รอตรวจสอบข้อมูล"
      assert_equal assessment&.process_result&.sent_status, 2, "ส่งสำเร็จ"
      raise ActiveRecord::Rollback
    end
  end

  test 'should check assessment success' do
    ActiveRecord::Base.transaction do
      assessment = send_data assessment_dup
      puts '-:- 1.process_result in return form SendCommand -:- ', assessment&.process_result.to_json
      EHHC::Commander.call(EHHC::CheckCommand.new, list: [EHHC::Item.new(assessment&.process_result)])
      assessment = Assessment.find(assessment.id)
      puts '-:- 2.process_result in return form CheckCommand -:- ', assessment&.process_result.to_json
      assert_equal assessment&.process_result&.sent_status, 2, "ส่งสำเร็จ"
      raise ActiveRecord::Rollback
    end
  end

  private

  def send_data(assessment)
    ehhc_comand EHHC::SendCommand.new, assessment
    Assessment.find(assessment.id) #update assessment
  end

  def ehhc_comand(cmd, assessment)
    item = EHHC::Item.new assessment
    EHHC::Commander.call cmd, list: [item]
  end

  def assessment_dup
    assessment_dup = Assessment.where(ready: true).first.dup
    assessment_dup.set_id = EHHC::Utils.generate_set_id('00000')
    assessment_dup.assessment_at = Time.now.getutc
    assessment_dup.process_result_id = nil
    assessment_dup.save
    assessment_dup
  end
end
