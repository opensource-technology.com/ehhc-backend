--master.address_tambons
INSERT INTO master.address_tambons (code ,label) VALUES ('100910' , 'พระโขนงใต้');
INSERT INTO master.address_tambons (code ,label) VALUES ('101406' , 'พญาไท');
INSERT INTO master.address_tambons (code ,label) VALUES ('102204' , 'บางแคเหนือ');
INSERT INTO master.address_tambons (code ,label) VALUES ('102602' , 'รัชดาภิเษก');
INSERT INTO master.address_tambons (code ,label) VALUES ('102704' , 'นวมินทร์');
INSERT INTO master.address_tambons (code ,label) VALUES ('102705' , 'นวลจันทร์');
INSERT INTO master.address_tambons (code ,label) VALUES ('102902' , 'วงศ์สว่าง');
INSERT INTO master.address_tambons (code ,label) VALUES ('103402' , 'อ่อนนุช');
INSERT INTO master.address_tambons (code ,label) VALUES ('103403' , 'พัฒนาการ');
INSERT INTO master.address_tambons (code ,label) VALUES ('103604' , 'ดอนเมือง');
INSERT INTO master.address_tambons (code ,label) VALUES ('103605' , 'สนามบิน');
INSERT INTO master.address_tambons (code ,label) VALUES ('104302' , 'รามอินทรา');
INSERT INTO master.address_tambons (code ,label) VALUES ('104402' , 'ราษฎร์พัฒนา');
INSERT INTO master.address_tambons (code ,label) VALUES ('104403' , 'ทับช้าง');
INSERT INTO master.address_tambons (code ,label) VALUES ('104502' , 'สะพานสอง');
INSERT INTO master.address_tambons (code ,label) VALUES ('104503' , 'คลองเจ้าคุณสิงห์');
INSERT INTO master.address_tambons (code ,label) VALUES ('104504' , 'พลับพลา');
INSERT INTO master.address_tambons (code ,label) VALUES ('104702' , 'บางนาเหนือ');
INSERT INTO master.address_tambons (code ,label) VALUES ('104703' , 'บางนาใต้');
INSERT INTO master.address_tambons (code ,label) VALUES ('105002' , 'บางบอนเหนือ');
INSERT INTO master.address_tambons (code ,label) VALUES ('105003' , 'บางบอนใต้');
INSERT INTO master.address_tambons (code ,label) VALUES ('105004' , 'คลองบางพราน');
INSERT INTO master.address_tambons (code ,label) VALUES ('105005' , 'คลองบางบอน');
INSERT INTO master.address_tambons (code ,label) VALUES ('360119' , 'ซับสีทอง');
INSERT INTO master.address_tambons (code ,label) VALUES ('402901' , 'ในเมือง');
INSERT INTO master.address_tambons (code ,label) VALUES ('402902' , 'เมืองเก่าพัฒนา');
INSERT INTO master.address_tambons (code ,label) VALUES ('402903' , 'เขาน้อย');
INSERT INTO master.address_tambons (code ,label) VALUES ('530407' , 'ท่าแฝก');
INSERT INTO master.address_tambons (code ,label) VALUES ('930806' , 'วังใหม่');

--master.address_amphurs
INSERT INTO master.address_amphurs (code ,label) VALUES ('4029' , 'เวียงเก่า');