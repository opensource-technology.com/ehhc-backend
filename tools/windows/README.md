# How to install EHHC-BKK on Windows 7

## Requirements
- ruby installer (2.4.3)
- git (2.16.2)
- nodejs (8.9.x)
- postgresql (10.2)
- nginx (1.12.2)
- redis (3.0.5)
- nssm (2.24)

## Ruby
- Install ruby with ruby installer (2.4.3) [Download](https://rubyinstaller.org/downloads/)
- After finish installing ruby, It will popup to install MSYS2, check the box, choose the choice number 3
- Install rails with command line


```
$ gem install rails
```

- Check rails version

```
$ rails -v
```

## Git
- [Download](https://git-scm.com/downloads/)
- Check version

```
$ git --version
```

## NodeJS
- [Download](https://nodejs.org/en/)
- Use version 8.x.x (LTS)

## PostgreSQL
- Choose default locale: thailand
- Configure environment path

## Redis
- [32 bit](https://github.com/cuiwenyuan/Redis-Windows-32bit)
- [64 bit](https://github.com/ServiceStack/redis-windows/tree/master/downloads)

- Install redis as Windows service
  - Double click service-install.bat
  - If not found service-install.bat, create a new one


  ```
  redis-server.exe --service-install redis.windows.conf --loglevel verbose
  redis-server --service-start
  ```

## Nginx

- [Download](http://nginx.org/en/download.html) and extract to drive c:
- Rename folder from `nginx-1.x.x` to `nginx`
- Install nginx as Windows service (Use CMD as administrator privilege)

  ```
  $ nginxservice.exe install
  ```

- Check nginx service in service manager
- Check redis service in Computer Manager -> Services and Applications -> Services

References
- [How to?](https://vexxhost.com/resources/tutorials/nginx-windows-how-to-install) 
- [Windows Service Wrapper](https://github.com/kohsuke/winsw)
- [Nginx XML](http://asysadmin.tumblr.com/post/32941224574/running-nginx-on-windows-as-a-service)


## EHHC-Backend
- Clone from repository to c:\

```
$ git clone http://gitlab.opensource-technology.com:9090/karn/ehhc-backend.git
```

- [Bcrypt](https://github.com/codahale/bcrypt-ruby/issues/142)
- Add bat to start automatically after windows started
- Run as windows-service
  - Download [NSSM](https://nssm.cc/download) and extract to c:\
  - Add path to environment variables
  - Run install script `autostart.bat`
  

## EHHC-Client
- Clone from repository to c:\

```
$ git clone http://gitlab.opensource-technology.com:9090/karn/ehhc-client.git
```

- Update nginx config

```
server {
  listen 4000;
  server_name localhost;
  root "c:/ehhc-client/build";

  location / {
    index index.html;
    try_files $uri $uri/ /index.html;
  }
}

```

## EHHC-Gateway
- Clone from repository to c:\

```
$ git clone http://gitlab.opensource-technology.com:9090/karn/ehhc-gateway.git
```

- Install gateway as windows service

```
$ node install.js
```

- Check service in Computer Manager -> Services and Applications -> Services
