@echo off
nssm install EHHC-Backend "C:\Ruby24\bin\rails.bat"
nssm set EHHC-Backend AppDirectory "C:\ehhc-backend"
nssm set EHHC-Backend AppParameters "s -e production -b 0.0.0.0 -p 5000"
nssm set EHHC-Backend Description "EHHC-Backend"
pause