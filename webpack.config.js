const path = require('path')
const webpack = require('webpack')

if(process.env.NODE_ENV === 'development') {
    console.log('Development mode')
}
module.exports = {
    entry: {
        app: path.join(__dirname, 'client/index.js')
    },
    output: {
        publicPath: '/',
        path: path.resolve(__dirname, 'dist'),
        filename: "[name].js"
    },
    devtool: "cheap-eval-source-map",
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)?$/,
                exclude: /node_modules/,
                enforce: 'pre',
                loader: 'eslint-loader'
            },
            {
                test: /\.(js|jsx)?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                      presets: ['es2015', 'react']
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                   'style-loader',
                   'css-loader'
                 ]
             }
        ]
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.EnvironmentPlugin({
            NODE_ENV: 'development', // use 'development' unless process.env.NODE_ENV is defined
            DEBUG: true
        })
    ]
}
